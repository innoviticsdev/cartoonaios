//
//  ChangePasswordViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var txtCurrentPass: RoundTextField!
    @IBOutlet weak var txtNewPass: RoundTextField!
    @IBOutlet weak var btnShowOld: UIButton!
    @IBOutlet weak var btnShowNew: UIButton!
    
    
    @IBOutlet weak var SaveChangesButton: UIButton!{didSet{
        SaveChangesButton.layer.cornerRadius = 10
        }}
    
    lazy var viewModel: UpdateProfileVM = {
        return UpdateProfileVM()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNewPass.isSecureTextEntry = true
        txtCurrentPass.isSecureTextEntry = true
        setupTextField(textField: txtNewPass)
        setupTextField(textField: txtCurrentPass)
        initVM()
        // Do any additional setup after loading the view.
    }

    @IBAction func dismissPage(_ sender: Any) {
        closePage()
    }
    
    func closePage(){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showOldPass(_ sender: Any) {
        self.showHideTxtField(txtField: txtCurrentPass, btn: btnShowOld)
    }
    
    @IBAction func showNewPass(_ sender: Any) {
        self.showHideTxtField(txtField: txtNewPass, btn: btnShowNew)
    }
    
    func initVM() {
            
            // Set binding init
            viewModel.showAlertClosure = { [weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.viewModel.alertMessage {
                        self?.showAlert( message )
                    }
                }
            }

            viewModel.updateLoadingStatus = { [weak self] () in
                guard let self = self else {
                    return
                }

                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    switch self.viewModel.state {
                    case .empty, .error:
                        indicatorSwitch(status: .off,view: self.view)
                        showToast(message: "ChangePasswordVC-error".autoLocalize(), view: self.view)
                    case .loading:
                        indicatorSwitch(status: .on,view: self.view)
                    case .populated:
                        indicatorSwitch(status: .off,view: self.view)
                        showToast(message: "ChangePasswordVC-success".autoLocalize(), view: self.view)
                        UIView.animate(withDuration: 0.2, animations: {
                            self.contentView.alpha = 1.0
                            //Dismiss VC
                            self.closePage()
                        })
                    }
                }
            }

            viewModel.reloadViewClosure = { [weak self] () in
                DispatchQueue.main.async {
                    //self?.reloadViews()
                }
            }

        }
    
    
    @IBAction func changePassword(_ sender: Any) {
        print("changePassword")
        viewModel.param1 = txtCurrentPass.text
        viewModel.param2 = txtNewPass.text
        
        if(viewModel.isValid(updateType: .password)){
                //btnSignUp.isEnabled = false
            viewModel.updateProfile(updateType: .password)
        }else{
            indicatorSwitch(status: .off, view: view)
            showAlert(viewModel.brokenRules.first!.message)
        }
        
    }
    
    func showHideTxtField(txtField: UITextField, btn: UIButton){
        if txtField.isSecureTextEntry{
            txtField.isSecureTextEntry = false
            btn.setTitle("HIDE", for: .normal)
        } else {
            txtField.isSecureTextEntry = true
            btn.setTitle("SHOW", for: .normal)
        }
    }
    
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
