//
//  HelpViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/23/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures

class HelpViewController: UIViewController {

    @IBOutlet weak var DetailsTextField: TRTextField!
    @IBOutlet weak var SubjectTextField: TRTextField!
    
    lazy var viewModel: HelpVM = {
           return HelpVM()
       }()
    
    @IBOutlet weak var SubmitView: UIView!{didSet{
        roundedViewWithShadow(view: SubmitView)
        
        }}
    @IBOutlet weak var SubmitButton: UIButton!{didSet{
    SubmitButton.layer.cornerRadius = 10
        SubmitButton.onTap{ _ in
            self.viewModel.helpParam = ["subject":self.SubjectTextField.text!,"details":self.DetailsTextField.text!]
            self.viewModel.validateTextFields()
        }
    }}
    @IBOutlet weak var CallUsButton: UIButton!{didSet{
    CallUsButton.layer.cornerRadius = 5
    }}
    override func viewDidLoad() {
        super.viewDidLoad()
        initVM()
        
        // Do any additional setup after loading the view.
    }

    @IBAction func closeScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                   
                    self.view.hideProgress()
                   
                case .loading:
                    self.view.showProgress(isDim: true)
              
                case .populated:
                    self.view.hideProgress()
                     self.present(HelpPopUp(), animated: true, completion: nil)
                    
                   
                }
            }
        }

      
       
      

    }
    
    func showAlert( _ message: String ) {
          let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
          alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
          self.present(alert, animated: true, completion: nil)
      }
    
    @IBAction func btnCallUs(_ sender: Any) {
        makePhoneCall(phoneNumber: "201201643419")
    }
    
    func makePhoneCall(phoneNumber: String) {

        if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {

            let alert = UIAlertController(title: ("Call " + phoneNumber + "?"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
                UIApplication.shared.open(phoneURL as URL, options: [:], completionHandler: nil)
            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    

}
