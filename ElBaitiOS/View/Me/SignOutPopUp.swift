//
//  SignOutPopUp.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/23/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures

class SignOutPopUp: UIViewController {

    @IBOutlet weak var CloseButton: UIButton!{didSet{
        CloseButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
        }}
    @IBOutlet weak var YesButton: UIButton!{didSet{
        YesButton.layer.cornerRadius = 10
        YesButton.onTap{ _ in
            self.LogoutAPI()
            self.view.window!.rootViewController?.dismiss(animated: true, completion: {
                self.tabBarController?.selectedIndex = 0
            })
        }
        }}
    @IBOutlet weak var NoButton: UIButton!{didSet{
    NoButton.layer.cornerRadius = 10
        NoButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
    }}
    @IBOutlet weak var MainView: UIView!{didSet{
        MainView.layer.cornerRadius = 10
        }}
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    func LogoutAPI()
    {
        let api = RestApiManager(request: Router.Logout)
        api.TestingEnabled = true
        api.Request(completionHandler: {
            (success,JSON,status) -> Void in
            
         
            if success {
                if (status == "200")
                {
                   
              let defaults: UserDefaults = UserDefaults.standard
                  defaults.set(false, forKey: "LoggedIn")
                    
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMe"), object: nil)
                    
                    
                }
                else
                {
                   
                }
                
            }
            else
            {
                print("faild")
            }
            
        })
    }
}
