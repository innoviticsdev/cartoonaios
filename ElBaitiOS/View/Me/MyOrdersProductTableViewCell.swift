//
//  MyOrdersProductTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/25/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class MyOrdersProductTableViewCell: UITableViewCell {

    @IBOutlet weak var ProductQuantity: UILabel!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var ProductImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func instanceFromNib() -> MyOrdersProductTableViewCell {
           return UINib(nibName: "MyOrdersProductTableViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MyOrdersProductTableViewCell
       }
    
}
