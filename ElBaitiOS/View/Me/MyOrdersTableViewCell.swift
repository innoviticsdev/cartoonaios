//
//  MyOrdersTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/25/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import FlexibleSteppedProgressBar
import GestureRecognizerClosures

class MyOrdersTableViewCell: UITableViewCell,FlexibleSteppedProgressBarDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var productsTableViewHeightConstraint: NSLayoutConstraint!
    var OrderObject: MyOrdersData!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var PlacedOnLabel: UILabel!
    @IBOutlet weak var OrderLabel: UILabel!
    @IBOutlet weak var CancelOrderLabel: UILabel!
    @IBOutlet weak var ProductTableview: UITableView!
    var OrderItems = [MyOrderItem]()
    @IBOutlet weak var ProgressBar: FlexibleSteppedProgressBar!{didSet{
        ProgressBar.numberOfPoints = 4
        ProgressBar.lineHeight = 3
        ProgressBar.radius = 5
        ProgressBar.progressRadius = 8
        ProgressBar.progressLineHeight = 3
        ProgressBar.selectedBackgoundColor = UIColor(hexString: "F4A03B")
        ProgressBar.selectedOuterCircleStrokeColor = UIColor(hexString: "F4A03B")
        ProgressBar.currentSelectedCenterColor = UIColor(hexString: "F4A03B")
        ProgressBar.stepTextColor = UIColor.black
        ProgressBar.currentSelectedTextColor = UIColor.black
        if(Localize.getLocal() == "en")
        {
        ProgressBar.stepTextFont = UIFont(name: "Poppins-Medium", size: 13)
        }
        else
        {
           ProgressBar.stepTextFont = UIFont(name: "Tajawal-Medium", size: 13)
        }
       
        
        }}
    @IBOutlet weak var MyOrdersCellMainVIew: UIView!{didSet{
        roundedViewWithShadow(view: MyOrdersCellMainVIew)
        }}
    
    @IBOutlet weak var btnRate: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ProgressBar.delegate = self
        
        ProductTableview.delegate = self
              ProductTableview.dataSource = self
              ProductTableview.registerCellNib(cellClass: MyOrdersProductTableViewCell.self)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        productsTableViewHeightConstraint.constant = (OrderItems.count >= 2) ? 120 : 60

        return OrderItems.count

          }

           func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersProductTableViewCell", for: indexPath) as! MyOrdersProductTableViewCell
            
            cell.ProductImage.kf.setImage(with: URL(string: (OrderItems[indexPath.row].product?.baseImage.originalImageURL)!))
            cell.ProductName.text = OrderItems[indexPath.row].product?.name
            cell.ProductQuantity.text = "MyOrdersVC-QTY".autoLocalize() + OrderItems[indexPath.row].qtyOrdered

              return cell
          }
          
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 60
          }
          
          func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
          }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar, textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
           if position == FlexibleSteppedProgressBarTextLocation.bottom {
               switch index {
                   
               case 0: return "MyOrdersVC-Ordered".autoLocalize()
               case 1: return OrderObject.statusLabel == "Canceled" ? "MyOrdersVC-Cancelled".autoLocalize() : "MyOrdersVC-Processing".autoLocalize()
               case 2: return "MyOrdersVC-Shipped".autoLocalize()
               case 3: return "MyOrdersVC-Delivered".autoLocalize()
               default: return "Date"
                   
               }
           }
           return ""
       }
    

    
}
