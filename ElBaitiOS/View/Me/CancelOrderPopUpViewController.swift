//
//  CancelOrderPopUpViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/25/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
class CancelOrderPopUpViewController: UIViewController {

    @IBOutlet weak var MainView: UIView!{didSet{
        roundedViewWithShadow(view: MainView)
        }}
    @IBOutlet weak var CancelButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        CancelButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
