//
//  MeVC.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Kingfisher

enum Section : String, CaseIterable{
    case accounts = "My Account"
    case settings = "Settings"
    case other = ""
}

let profileNotificationCtr = NotificationCenter.default
let profileBroadcast = "broadcast"

class MeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var UserNameLabel: UILabel!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    var sectionMyAccount = ["MeVC-MyOrders".autoLocalize(),"AddressesVC-Addresses".autoLocalize(),"MeVC-Profile".autoLocalize(),"Wishlist"]
    var sectionSettings = ["MeVC-ChangeLanguage".autoLocalize()]
    var sectionOthers = ["MeVC-Help".autoLocalize(),"MeVC-AboutDeveloper".autoLocalize(),"MeVC-Logout".autoLocalize()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confNavBar()
        setupDelegates()
        
       

        addShadowtoView(view: navBarView)
        makeCircleImgView(imgView: imgView)
        self.tableView.rowHeight = 10.0
        
        let footerView = MeFooter.instanceFromNib()
        tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(0))
        self.tableView.tableFooterView = footerView
        
        setupObserver()
        setupObserver2()
    }
    
    func setupObserver2(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshMe), name: NSNotification.Name(rawValue: "RefreshMe"), object: nil)
    }
    @objc func RefreshMe() {

        self.tableView.reloadData() // a refresh the tableView.
        imgView.image = #imageLiteral(resourceName: "avatar")
        UserNameLabel.text = "MeVC-Welcome".autoLocalize()
    }
    
     
    func setupObserver() {
        profileNotificationCtr.addObserver(self, selector: Selector(("updateObserver")), name: NSNotification.Name(rawValue: profileBroadcast), object: nil)
    }
        
    // MARK: Observer Selector functions
    @objc func updateObserver(){

        let defaults: UserDefaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "UserData") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(User.self, from: savedPerson) {
                UserNameLabel.text = loadedPerson.name
                //KingfisherManager.shared.cache.clearCache()
                imgView.kf.setImage(with: URL(string: loadedPerson.image),placeholder: UIImage(named: "product-placeholder"))
             
            }
        }
    }
    
    deinit {
        profileNotificationCtr.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
        //navigationController?.setNavigationBarHidden(true, animated: animated)
        
//        let searchButton = UIButton(type: .system)
//        searchButton.setImage(#imageLiteral(resourceName: "SearchIcon").withRenderingMode(.alwaysOriginal), for: .normal)
//        searchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
//        
//        navigationItem.leftBarButtonItems = [ UIBarButtonItem(customView: searchButton), UIBarButtonItem(customView: searchButton) ]
        
        
        tableView.reloadData()
        if(checkIfSignedIn())
               {
                   let defaults: UserDefaults = UserDefaults.standard

                   if let savedPerson = defaults.object(forKey: "UserData") as? Data {
                       let decoder = JSONDecoder()
                       if let loadedPerson = try? decoder.decode(User.self, from: savedPerson) {
                           UserNameLabel.text = loadedPerson.name
                           imgView.kf.setImage(with: URL(string: loadedPerson.image),placeholder: UIImage(named: "product-placeholder"))
                        
                       }
                   }

               }
               else{
            UserNameLabel.text = "MeVC-Welcome".autoLocalize()
               }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
     
    }

    func confNavBar(){
//        self.navigationController?.navigationBar.prefersLargeTitles = true
//        self.navigationController?.navigationBar.topItem!.title = "Hi, Ahmed"
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
    
    func setupDelegates(){
    tableView.delegate = self
    tableView.dataSource = self
    tableView.registerCellNib(cellClass: MeTableViewCell.self)
    tableView.registerCellNib(cellClass: MeHeaderTableViewCell.self)
    }
    
    @IBAction func goToPrivacy(_ sender: Any) {
        print("goToPrivacy")
        if let url = URL(string: "http://asmio.innsandbox.com/terms.html") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func goToTerms(_ sender: Any) {
        print("goToTerms")
        if let url = URL(string: "http://asmio.innsandbox.com/terms.html") {
            UIApplication.shared.open(url)
        }
    }
    
}

extension MeVC : UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return UIView(frame: CGRectMake(0, 0, tableView.bounds.size.width, 20))
//    }

//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 100
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return sectionMyAccount.count
        case 1:
            return sectionSettings.count
        case 2:
            return sectionOthers.count
        
        default:
          return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func setHeaderHeight(height: Double) {
        tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(height))

        tableView.tableHeaderView?.frame.size = CGSize(width:tableView.frame.width, height: CGFloat(height))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue() as MeTableViewCell
        switch (indexPath.section) {
        case 0:
            cell.lblTitle?.text = sectionMyAccount[indexPath.row].autoLocalize()
        case 1:
            cell.lblTitle?.text = sectionSettings[indexPath.row].autoLocalize()
          cell.lblAccessory.alpha = 1.0
        case 2:
          if(checkIfSignedIn())
          {
            cell.lblTitle?.text = sectionOthers[indexPath.row].autoLocalize()
            if(sectionOthers[indexPath.row] == "MeVC-Logout".autoLocalize()){
            cell.lblTitle.textColor = #colorLiteral(red: 0.9882352941, green: 0.3803921569, blue: 0.3803921569, alpha: 1)
            }
            }
          else{
            if(sectionOthers[indexPath.row] == "MeVC-Logout".autoLocalize()){
                cell.lblTitle?.text = "MeVC-Login".autoLocalize()
                cell.lblTitle.textColor = ColorPalette.ButtonColor
            }
            else{
                cell.lblTitle?.text = sectionOthers[indexPath.row].autoLocalize()
            }
            }
        default:
          cell.lblTitle?.text = "Other"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let headerCell = tableView.dequeue() as MeHeaderTableViewCell
        
        headerCell.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
    
          switch (section) {
          case 0:
            headerCell.lblName.text = "MeVC-MyAccount".autoLocalize()
          case 1:
            headerCell.lblName.text = "MeVC-Settings".autoLocalize()
          case 2:
            headerCell.lblName.text = Section.other.rawValue
          default:
            headerCell.lblName.text = "Other";
          }
    
          return headerCell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    //  Determine what to do when a cell in a particular section is selected.
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                if(checkIfSignedIn())
                {
                let MyOrdersVC = MyOrdersViewController()
                MyOrdersVC.modalPresentationStyle = .overFullScreen
                self.present(MyOrdersVC, animated: true, completion: nil)
                    
                }
                else{
                    //Show popup from bottom to up..
                    let vc = SignInVC()
                    presentBottomPopup(view: self, popupVC: vc)
                }
            case 1:
                if(checkIfSignedIn())
                {
                let AddressesPage = AddressesVC()
                AddressesPage.modalPresentationStyle = .overFullScreen
                self.present(AddressesPage, animated: true, completion: nil)
                }
                else{
                    let vc = SignInVC()
                    presentBottomPopup(view: self, popupVC: vc)
                }
                case 2:
                if(checkIfSignedIn())
                {
                let profileVC = ProfileVC()
                profileVC.modalPresentationStyle = .overFullScreen
                self.present(profileVC, animated: true, completion: nil)
                }
                else{
                    let vc = SignInVC()
                    presentBottomPopup(view: self, popupVC: vc)
                }
            case 3:
                self.present(WishlistVC(), animated: true, completion: nil)
            default:
                break
            }
            
        case 1:
             let defaults: UserDefaults = UserDefaults.standard
            if(Localize.getLocal() == "en")
            {
                defaults.set("ar", forKey: "Language")
                defaults.synchronize()
                self.setRTL(self)
                showToast(message: "Changed To Arabic", view: self.view)
            }
            else{
                defaults.set("en", forKey: "Language")
                defaults.synchronize()
                self.setLTR(self)
                showToast(message: "Changed To English", view: self.view)

                }
            break
        case 2:
        switch indexPath.row {
            case 0:
                let HelpVC = HelpViewController()
                HelpVC.modalPresentationStyle = .overFullScreen
                self.present(HelpVC, animated: true, completion: nil)
            case 1:
                if let url = URL(string: "https://www.innovitics.com") {
                    UIApplication.shared.open(url)
                }
            case 2:
                if(checkIfSignedIn())
                {
                    let vc = SignOutPopUp()
                    presentBottomPopup(view: self, popupVC: vc)
            }
                else{
                    let vc = SignInVC()
                    presentBottomPopup(view: self, popupVC: vc)
            }
            default:
                break
            }
       
        default:
            print("Out of index")
        }
    }
    
    func setRTL(_ sender: UIViewController) {
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
        goToRoot()
    }

    func setLTR(_ sender: UIViewController) {
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        goToRoot()
    }
}

