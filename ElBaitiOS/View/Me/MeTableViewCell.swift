//
//  MeTableViewCell.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/16/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class MeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAccessory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
