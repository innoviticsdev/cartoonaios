//
//  MeHeaderTableViewCell.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/16/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class MeHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
