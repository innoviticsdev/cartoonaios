//
//  HelpPopUp.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
class HelpPopUp: UIViewController {

    @IBOutlet weak var MainView: UIView!{didSet{
        roundedViewWithShadow(view: MainView)
        }}
    @IBOutlet weak var CloseButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        CloseButton.onTap{ _ in
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
