//
//  FooterView.swift
//  ElBaitiOS
//
//  Created by mac on 9/1/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class FooterView: UIView {
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> FooterView {
        return UINib(nibName: "FooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FooterView
    }

}
