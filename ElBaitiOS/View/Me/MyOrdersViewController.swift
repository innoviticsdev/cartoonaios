//
//  MyOrdersViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/25/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
import DZNEmptyDataSet

let cancelNotificationCtr = NotificationCenter.default
let cancelBroadcast = "cancelBroadcast"

let RatingNotificationCtr = NotificationCenter.default
let RatingBroadcast = "RatingBroadcast"

class MyOrdersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var CloseButton: UIButton!
    @IBOutlet weak var MyOrdersTableView: UITableView!
    var closeClearStack: Bool?
    
    lazy var viewModel: MyOrdersVM = {
           return MyOrdersVM()
       }()
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyOrdersTableView.delegate = self
        MyOrdersTableView.dataSource = self
        
        MyOrdersTableView.emptyDataSetDelegate = self
        MyOrdersTableView.emptyDataSetSource = self
        
        MyOrdersTableView.registerCellNib(cellClass: MyOrdersTableViewCell.self)
        
        CloseButton.onTap{ _ in
            
            if(self.closeClearStack ?? false){
                self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }

            self.dismiss(animated: true, completion: nil)
        }
         initVM()
         setupObserver()
        // Do any additional setup after loading the view.
    }
    
    func setupObserver() {
        cancelNotificationCtr.addObserver(self, selector: #selector(self.refreshOrders), name: NSNotification.Name(rawValue: cancelBroadcast), object: nil)
        
        RatingNotificationCtr.addObserver(self, selector: Selector(("notifyObservers")), name: NSNotification.Name(rawValue: RatingBroadcast), object: nil)
        }
        
    // MARK: Observer Selector functions
    @objc func notifyObservers(){
        refreshOrders()
    }
        
    // MARK: Observer Selector functions
    @objc func refreshOrders(){
        viewModel.initFetch()
    }
    
    deinit {
        notificationCtr.removeObserver(self)
        RatingNotificationCtr.removeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.myOrdersData.count

       }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersTableViewCell", for: indexPath) as! MyOrdersTableViewCell
            
//            if(!self.viewModel.myOrdersData[indexPath.row].canReview){
//                cell.btnRate.isHidden = true
//            }
            
            cell.btnRate.onTap{
                _ in
                let vc = RatingVC()
                vc.orderId = self.viewModel.myOrdersData[indexPath.row].id
                vc.arrOrderItems =  self.viewModel.myOrdersData[indexPath.row].items
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            
            cell.OrderLabel.text = "MyOrdersVC-Order#".autoLocalize() + viewModel.myOrdersData[indexPath.row].id.description
            cell.PlacedOnLabel.text = "MyOrdersVC-Placed".autoLocalize() + viewModel.myOrdersData[indexPath.row].createdAt
            cell.PriceLabel.text = viewModel.myOrdersData[indexPath.row].formatedGrandTotal
            cell.ProgressBar.currentIndex = Int(viewModel.myOrdersData[indexPath.row].statusLocation)!
            cell.CancelOrderLabel.onTap{ _ in
                let OrderDetailsVC = MyOrderDetailsVC()
                OrderDetailsVC.MyOrderObject = self.viewModel.myOrdersData[indexPath.row]
                self.present(OrderDetailsVC, animated: true, completion: nil)
               
            }
            cell.OrderItems = viewModel.myOrdersData[indexPath.row].items
            cell.OrderObject = viewModel.myOrdersData[indexPath.row]
            
            if(viewModel.myOrdersData[indexPath.row].statusLabel == "Canceled"){
                cell.ProgressBar.selectedBackgoundColor = ColorPalette.actionRed
                       cell.ProgressBar.selectedOuterCircleStrokeColor = ColorPalette.actionRed
                       cell.ProgressBar.currentSelectedCenterColor = ColorPalette.actionRed
                   }
                   else{
                       cell.ProgressBar.selectedBackgoundColor = ColorPalette.secondaryColor
                       cell.ProgressBar.selectedOuterCircleStrokeColor = ColorPalette.secondaryColor
                       cell.ProgressBar.currentSelectedCenterColor = ColorPalette.secondaryColor
                   }
            
            cell.ProductTableview.reloadData()

           return cell
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           
        let count = viewModel.myOrdersData[indexPath.row].items.count
        if(count >= 2){
            return 400
        }else{
          return 320
        }
       }
    
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              self.present(MyOrderDetailsVC(), animated: true, completion: nil)
       }
       

  func initVM() {
      
      // Set binding init
      viewModel.showAlertClosure = { [weak self] () in
          DispatchQueue.main.async {
              if let message = self?.viewModel.alertMessage {
                  self?.showAlert( message )
              }
          }
      }

      viewModel.updateLoadingStatus = { [weak self] () in
          guard let self = self else {
              return
          }
          self.MyOrdersTableView.reloadData()
          DispatchQueue.main.async { [weak self] in
              guard let self = self else {
                  return
              }
              switch self.viewModel.state {
              case .empty, .error:
                  indicatorSwitch(status: .off,view: self.view)
              case .loading:
                  indicatorSwitch(status: .on,view: self.view)
              case .populated:
                  indicatorSwitch(status: .off,view: self.view)
                  self.MyOrdersTableView.reloadData()

              }
          }
      }
      viewModel.initFetch()
  }
    
    
       func showAlert( _ message: String ) {
           let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
           alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }

}

extension MyOrdersViewController: DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        switch self.viewModel.state {
                       case .empty, .error:
                           return true
                       case .loading:
                            return false
                       case .populated:
                          return false
        }
            
        }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                           str = "DZNEmpty-Something-Wrong".autoLocalize()
                        break
                       case .empty:
                          str = "You haven't created any orders yet"
                        break
                        default:
                           str = ""
                        break
        }
         if(Localize.getLocal() == "en"){
               let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
               else{
                    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                         str = "DZNEmpty-Internet-Connection".autoLocalize()
                        break
                       case .empty:
                          str = "DZNEmpty-Check-Bestsellers".autoLocalize()
                        break
                        default:
                           str = ""
                        break
        }
     
        if(Localize.getLocal() == "en"){
               let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
               else{
                    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let str: String!
        switch self.viewModel.state {
                              case .error:
                                str = "DZNEmpty-Btn-Retry".autoLocalize()
                               break
                              case .empty:
                                 str = "DZNEmpty-Btn-Check-Bestsellers".autoLocalize()
                               break
                               default:
                                  str = ""
                               break
               }
           if(Localize.getLocal() == "en"){
            let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
                return NSAttributedString(string: str, attributes: myAttribute)
            }
            else{
                 let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
                return NSAttributedString(string: str, attributes: myAttribute)
            }
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let buttonFrame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 50)
        let buttonContainer = UIView(frame: buttonFrame)
        buttonContainer.backgroundColor = scrollView.backgroundColor

        let imageView = UIImageView(frame: buttonFrame)
        imageView.backgroundColor = ColorPalette.primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        buttonContainer.addSubview(imageView)
        return createImage(buttonContainer)

//        let capInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
//        var rectInsets: UIEdgeInsets = .zero
//        var imageName = ""
//
//        if state == .normal {
//            imageName = "dz-empty-button-bg"
//        }
//        if state == .highlighted {
//            imageName = "dz-empty-button-bg"
//        }
//
//        rectInsets = UIEdgeInsets(top: -19.0, left: -61.0, bottom: -19.0, right: -61.0)
//        let image = UIImage(named: imageName, in: Bundle(for: type(of: self)), compatibleWith: nil)
//
//        return image?.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
         
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        switch self.viewModel.state {
                       case .error:
                          viewModel.initFetch()
                        break
                       case .empty:
                          self.dismiss(animated: true, completion: nil)
                        break
                        default:
                        break
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        var image: UIImage?
        switch self.viewModel.state {
                              case .error:
                                  image = UIImage(named: "connection-error")
                               break
                              case .empty:
                                 image = UIImage(named: "no-available-products")
                               break
                               default:
                                  image = UIImage(named: "no-available-products")
                               break
               }
        return image
    }
    func createImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: view.frame.width, height: view.frame.height), true, 1)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    

}

