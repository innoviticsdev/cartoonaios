//
//  ChangePhoneVC.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class ChangePhoneVC: UIViewController {
    
    @IBOutlet weak var txtCode: RoundTextField!
    @IBOutlet weak var txtPhone: RoundTextField!
    @IBOutlet weak var btnChanges: UIButton!
    @IBOutlet weak var contentView: UIView!
    var  phoneNumber = ""
    lazy var viewModel: UpdateProfileVM = {
        return UpdateProfileVM()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField(textField: txtPhone)
        setUpRoundedBtn(btn: btnChanges, color: ColorPalette.ButtonColor)
        initVM()
    }

    @IBAction func dismissPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func closePage(){
        dismiss(animated: true, completion: nil)
    }

    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)

                    UIView.animate(withDuration: 0.2, animations: {
                        self.contentView.alpha = 1.0
                    })
                case .loading:
                    indicatorSwitch(status: .on,view: self.view)

                    UIView.animate(withDuration: 0.2, animations: {
                        self.contentView.alpha = 0.0
                    })
                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        self.contentView.alpha = 1.0
                        //Dismiss VC
                        //self.closePage()
                        self.verifyPhone()
                    })
                }
            }
        }

        viewModel.reloadViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                //self?.reloadViews()
            }
        }

    }
    
    @IBAction func changePhone(_ sender: Any) {
        print("changePhone")
        viewModel.param1 = txtCode.text
        viewModel.param2 = txtPhone.text
        
        if(viewModel.isValid(updateType: .number)){
            viewModel.updateProfile(updateType: .number)
        }else{
            indicatorSwitch(status: .off, view: view)
            showAlert(viewModel.brokenRules.first!.message)
        }
    }
    
    func verifyPhone() {
        print("Verify phone")
        guard let countryCode = txtCode.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
        guard let phone = txtPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
       
        phoneNumber = countryCode + phone
        
        viewModel.verifyPhone(phoneNumber: phoneNumber ){
            success, error in
            if error == nil{
                showToast(message: "OTP sent successfully", view: self.view)
                self.goVerify()
            }else{
                self.showAlert(error?.localizedDescription ?? "")
            }
        }
    }
    
    func goVerify(){
        print("go verify")
        let vc = VerifyNumberVC()
        vc.phoneNumber = phoneNumber
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
