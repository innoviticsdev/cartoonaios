//
//  CustomizedTabBarController.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseRemoteConfig

class CustomizedTabBarController: UITabBarController , UITabBarControllerDelegate{
    
    public static var DeepProductID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        
        setValue(CustomizedTabBar(frame: tabBar.frame), forKey: "tabBar")
        view.backgroundColor = .white
        NotificationCenter.default.addObserver(self, selector: #selector(GoToCart(_:)), name: NSNotification.Name(rawValue: "GoToCart"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GoToProductDetails), name: NSNotification.Name(rawValue: "GoToProductDetails"), object: nil)
        

        setupTabBar()
        
        
    }
    

    func setupTabBar(){
    
        let ExploreViewController = setupNavigationController(rootViewController: ExploreVC(nibName: "ExploreVC", bundle: nil))
        let CategoriesViewController = setupNavigationController(rootViewController: CategoriesTabViewController(nibName: "CategoriesTabViewController", bundle: nil))
        let DealsViewController = setupNavigationController(rootViewController:DealsVC(nibName: "DealsVC", bundle: nil))
        let CartViewController = MyCartViewController(nibName: "MyCartViewController", bundle: nil)

        let MeViewController = MeVC(nibName: "MeVC", bundle: nil)

        let tabIcon1 = UITabBarItem(title: "TabBar-Explore".autoLocalize(), image: UIImage(named: "explore"), tag: 0)
        let tabIcon2 = UITabBarItem(title: "TabBar-Categories".autoLocalize(), image: UIImage(named: "categories"), selectedImage: nil)
        let tabIcon3 = UITabBarItem(title: "TabBar-Deals".autoLocalize(), image: UIImage(named: "deals"), tag: 0)
        let tabIcon4 = UITabBarItem(title: "TabBar-Me".autoLocalize(), image: UIImage(named: "me"), selectedImage: nil)
        let tabIconDummy = UITabBarItem()
        tabIconDummy.isEnabled = false
        
        
        
        ExploreViewController.tabBarItem = tabIcon1
        CategoriesViewController.tabBarItem = tabIcon2
        DealsViewController.tabBarItem = tabIcon3
        MeViewController.tabBarItem = tabIcon4
        CartViewController.tabBarItem = tabIconDummy
     

//        if(Localize.getLocal() == "en")
//        {
            let tabControllers = [ExploreViewController, CategoriesViewController,CartViewController,DealsViewController, MeViewController]
            
            self.setViewControllers(tabControllers, animated: true)
            self.viewControllers = tabControllers
//        }
//        else{
//            let tabControllers = [ExploreViewController, CategoriesViewController,DealsViewController, MeViewController]
//
//            self.setViewControllers(tabControllers, animated: true)
//            self.viewControllers = tabControllers
//        }
        
        
        self.view.isUserInteractionEnabled = true
    }
    
    func setupNavigationController(rootViewController: UIViewController) -> UINavigationController
    {
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.navigationBar.backgroundColor = UIColor.white
        
        navigationController.navigationBar.isTranslucent = false
        
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.navigationItem.largeTitleDisplayMode = .always

        return navigationController
    }
    
     
    
    @objc func GoToCart(_ notification:Notification) {
        let vc = MyCartViewController()
        self.present(vc, animated: true, completion: nil)
        
    }
    @objc func GoToProductDetails(_ notification:Notification) {
        let ProductsDetailsVC = ProductDetailsViewController()
        ProductsDetailsVC.productId = CustomizedTabBarController.DeepProductID
        self.present(ProductsDetailsVC, animated: true, completion: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Delegate method
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
    
    
   
       
       
    
    
}
