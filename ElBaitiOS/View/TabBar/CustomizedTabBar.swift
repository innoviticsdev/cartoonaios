//
//  CustomizedTabBar.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
@IBDesignable
class CustomizedTabBar: UITabBar {
    private var shapeLayer: CALayer?
    let button: UIButton = UIButton()
    
    override func draw(_ rect: CGRect) {
        self.addShape()
        self.drawBtn()
    }
    
    func drawBtn(){
        
        let CartImage = UIImageView(image: UIImage(named: "cart"))
        CartImage.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        
        button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)

        
        button.setImage(CartImage.image, for: .normal)
        
      
        button.translatesAutoresizingMaskIntoConstraints = false
        self.selectedImageTintColor = ColorPalette.mainColor
        self.addSubview(button)
        self.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        self.topAnchor.constraint(equalTo: button.centerYAnchor).isActive = true
        
        
    }
    
    @objc func buttonClicked() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "GoToCart"), object: nil, userInfo: nil)
    }
    
    //To fix half floating button not working outside parent view.
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let translatedPoint = button.convert(point, from: self)
        if (button.bounds.contains(translatedPoint)) {
            return button.hitTest(translatedPoint, with: event)
        }
        return super.hitTest(point, with: event)
    }
    
    func addShape(){
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = createPath()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 1.0
        
        if let oldShape = self.shapeLayer {
            self.layer.replaceSublayer(oldShape, with: shapeLayer)
        } else{
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
        
        self.shapeLayer = shapeLayer
    }
    
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        let buttonRadius: CGFloat = 35
//        return abs(self.center.x - point.x) > buttonRadius || abs(point.y) > buttonRadius
//    }
    
    func createPath() -> CGPath{
        let radius: CGFloat = 34.0
        let path = UIBezierPath()
        let centerWidth = self.frame.width / 2
        
        path.move(to: CGPoint(x:0, y:0)) // Start top left
        
        path.addLine(to: CGPoint(x:(centerWidth - radius * 2), y: 0)) //the beginning of trough
        
        path.addArc(withCenter: CGPoint(x: centerWidth, y: 0), radius: radius, startAngle:
            CGFloat(180).degreesToRadians, endAngle: CGFloat(0).degreesToRadians, clockwise: false)
        

        //Complete the rect
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()
        
        return path.cgPath
    }

  
}

extension CGFloat {
    var degreesToRadians: CGFloat {return self * .pi / 180}
    var radiansToDegrees: CGFloat {return self * 180 / .pi}
}
