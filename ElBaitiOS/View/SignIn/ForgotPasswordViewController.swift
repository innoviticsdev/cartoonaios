//
//  ForgotPasswordViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/27/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
class ForgotPasswordViewController: UIViewController {

    var email = ""
    var apiService: RestApiManager!
    var forgotPasswordParam: [String : String]!
    @IBOutlet weak var CloseButton: UIButton!
    @IBOutlet weak var SentEmailLabel: UILabel!
    @IBOutlet weak var ResendEmailLabel: UILabel!
    @IBOutlet weak var MainView: UIView!{didSet{
    roundedViewWithShadow(view: MainView)
    }}
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SentEmailLabel.text = "ForgotPasswordVC-WeHave".autoLocalize() + email
        CloseButton.onTap { _ in
            self.dismiss(animated: true, completion: nil)
        }
        ResendEmailLabel.onTap { _ in
            self.forgotPasswordParam = ["email":self.email]
                       self.ForgotPasswordAPI()
        }
    }


    func ForgotPasswordAPI() {
        
        self.view.showProgress(isDim: true)
        
        apiService = RestApiManager(request: Router.ForgotPassoword(parameters: forgotPasswordParam))
            apiService.TestingEnabled = true

     

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let forgotPasswordResult = try JSONDecoder().decode(ForgotPasswordModel.self, from: JSONDATA.rawData())
                 if(forgotPasswordResult.status.code == 200)
                   {
                    self.view.hideProgress()
               
                            
                   
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                   self.view.hideProgress()
                   
                    self.showAlert(forgotPasswordResult.status.message)
                    return
                   }
             }
             catch{
                print("Fatal Error")
                self.view.hideProgress()
             }
            }
            // if connection failed
            else {
                self.view.hideProgress()
            }
        }
        )
    }
    
    func showAlert( _ message: String ) {
           let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
           alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }

}
