//
//  SignInPasswordVC.swift
//  ElBait
//
//  Created by Ahmed Ramzy on 7/29/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class SignInPasswordVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var ForgotPasswordButton: UIButton!
    @IBOutlet weak var txtPassword: RoundTextField!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    var forgotPasswordParam: [String : String]!
    var apiService: RestApiManager!
    
    // MARK: - Variables
    var userEmail: String?
    
    lazy var viewModel: LoginViewModel = {
        return LoginViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    func setupView() {
        setupTextField(textField: txtPassword)
        txtPassword.isSecureTextEntry = true
        setUpRoundedBtn(btn: btnLogin, color: ColorPalette.ButtonColor)
        ForgotPasswordButton.onTap { _ in
            self.forgotPasswordParam = ["email":self.userEmail!]
            self.ForgotPasswordAPI()
        }
    }
    
    @IBAction func goLogin(_ sender: Any) {
        viewModel.userEmail = userEmail
        viewModel.userPassword = txtPassword.text ?? ""
       
        if(viewModel.isPasswordValid){
            indicatorSwitch(status: .on, view: view)
            viewModel.authenticatePassword { [weak self] (loginModel,error) in
                indicatorSwitch(status: .off, view: (self?.view)!)
                if let error = error {
                    self?.showErrorMsg(msg: error)
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshCart"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMe"), object: nil)
                   self?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                }
            }
        }else{
            indicatorSwitch(status: .off, view: view)
            showErrorMsg(msg: viewModel.brokenRules.first!.message)
        }
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if txtPassword.isSecureTextEntry{
            txtPassword.isSecureTextEntry = false
            btnShow.setTitle("HIDE", for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            btnShow.setTitle("SHOW", for: .normal)
        }
    }
}






extension SignInPasswordVC{
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showErrorMsg(msg: String) {
        DispatchQueue.main.async { [weak self] in
        guard let self = self else{return}
        let alert = showErrorAlert(title: "Wrong!", errorMessage: msg)
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    func ForgotPasswordAPI() {
        
        self.view.showProgress(isDim: true)
        
        apiService = RestApiManager(request: Router.ForgotPassoword(parameters: forgotPasswordParam))
            apiService.TestingEnabled = true

     

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let forgotPasswordResult = try JSONDecoder().decode(ForgotPasswordModel.self, from: JSONDATA.rawData())
                 if(forgotPasswordResult.status.code == 200)
                   {
                    self.view.hideProgress()
                   let ForgotPasswordVC = ForgotPasswordViewController()
                   ForgotPasswordVC.email = self.userEmail!
                   self.present(ForgotPasswordVC, animated: true, completion: nil)
                            
                   
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                   self.view.hideProgress()
                   
                    self.showAlert(forgotPasswordResult.status.message)
                    return
                   }
             }
             catch{
                print("Fatal Error")
                self.view.hideProgress()
             }
            }
            // if connection failed
            else {
                self.view.hideProgress()
            }
        }
        )
    }
    
    func showAlert( _ message: String ) {
           let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
           alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
}
