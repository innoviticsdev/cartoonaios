//
//  EditProfileVC.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var txtLastName: RoundTextField!
    @IBOutlet weak var txtFirstName: RoundTextField!
    
    
    lazy var viewModel: UpdateProfileVM = {
        return UpdateProfileVM()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeCircleImgView(imgView: imgView)
        setUpRoundedBtn(btn: btnChange, color: ColorPalette.ButtonColor)
        setupTextField(textField: txtLastName)
        setupTextField(textField: txtFirstName)
        initVM()
        fillPlaceHolder()
        
    }
    
    func fillPlaceHolder(){
        let defaults: UserDefaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "UserData") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(User.self, from: savedPerson) {
                txtFirstName.text = loadedPerson.firstName
                txtLastName.text = loadedPerson.lastName
                imgView.kf.setImage(with: URL(string: loadedPerson.image),placeholder: UIImage(named: "product-placeholder"))
            }
        }
    }
    
    
    @IBAction func btnPickImg(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true){
            //after it's complete
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imgView.image = image
        }else{
            //Error msg
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnSave(_ sender: Any){
        print("btnSave")
        viewModel.param1 = txtFirstName.text
        viewModel.param2 = txtLastName.text
        viewModel.param3 = imgView.image?.jpegData(compressionQuality: 0.1)
        
        if(viewModel.isValid(updateType: .edit)){
                //btnSignUp.isEnabled = false
            viewModel.updateProfile(updateType: .edit)
        }else{
            indicatorSwitch(status: .off, view: view)
            showAlert(viewModel.brokenRules.first!.message)
        }
        
        
    }
    
    func initVM() {
            
            // Set binding init
            viewModel.showAlertClosure = { [weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.viewModel.alertMessage {
                        self?.showAlert( message )
                    }
                }
            }

            viewModel.updateLoadingStatus = { [weak self] () in
                guard let self = self else {
                    return
                }

                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    switch self.viewModel.state {
                    case .empty, .error:
                        indicatorSwitch(status: .off,view: self.view)
                        showToast(message: "EditProfileVC-Error".autoLocalize(), view: self.view)
                    case .loading:
                        indicatorSwitch(status: .on,view: self.view)
                    case .populated:
                        indicatorSwitch(status: .off,view: self.view)
                        UIView.animate(withDuration: 0.2, animations: {
                            self.contentView.alpha = 1.0
                            //Dismiss VC
                            self.closePage()
                        })
                    }
                }
            }

            viewModel.reloadViewClosure = { [weak self] () in
                DispatchQueue.main.async {
                    //self?.reloadViews()
                }
            }

        }
    
    @IBAction func dismissPage(_ sender: Any) {
        closePage()
    }
    
    func closePage(){
        dismiss(animated: true, completion: nil)
    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

