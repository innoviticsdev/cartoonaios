//
//  FiltersVC.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

protocol applyFilterDelegate {
    func didTapapply(filterParams: [String:String])
}

class FiltersVC: UIViewController,UITableViewDelegate,UITableViewDataSource, filterSelectionDelegate, rateSelectionDelegate  {

  
    var CategoryID = ""
    @IBOutlet weak var ResetButton: TRButton!
    var applyDelegate: applyFilterDelegate!
var CategoryAttributes = [CategoryAttribute]()
    @IBOutlet weak var CloseButton: UIButton!
 
    @IBOutlet weak var FiltersTableView: UITableView!
    
    lazy var viewModel: FiltersVM = {
              return FiltersVM()
          }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        CloseButton.onTap { _ in
            self.dismiss(animated: true, completion: nil)
        }
      

        FiltersTableView.delegate = self
        FiltersTableView.dataSource = self
        FiltersTableView.registerCellNib(cellClass: TagsTableViewCell.self)
        FiltersTableView.registerCellNib(cellClass: RateTableViewCell.self)
        FiltersTableView.registerCellNib(cellClass: ButtonTableViewCell.self)
        FiltersTableView.registerCellNib(cellClass: PriceRangeTableViewCell.self)
        
        ResetButton.onTap { _ in
            self.viewModel.filterParam.removeAll()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FiltersTableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CategoryAttributes.count + 2
            }
    
            

             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                if(indexPath.row < CategoryAttributes.count)
                {
                switch CategoryAttributes[indexPath.row].swatchType {
                case "text":
                     let cell = tableView.dequeue() as TagsTableViewCell
                     cell.configure(object: CategoryAttributes[indexPath.row], isTagFlag: true)
                     cell.selectionDelegate = self
                               return cell
                 case "dropdown":
                     let cell = tableView.dequeue() as TagsTableViewCell
                       cell.configure(object: CategoryAttributes[indexPath.row], isTagFlag: true)
                       cell.selectionDelegate = self
                    
                               return cell
                 case "color":
                     let cell = tableView.dequeue() as TagsTableViewCell
                       cell.configure(object: CategoryAttributes[indexPath.row], isTagFlag: false)
                       cell.selectionDelegate = self
                     
                               return cell
                 case "slider":
                     let cell = tableView.dequeue() as PriceRangeTableViewCell
                      cell.configure(object: CategoryAttributes[indexPath.row])
                
                  
                                        return cell
                 default:
                     break

                }
                }
                else{
                    switch indexPath.row {
                           
                    
                            case CategoryAttributes.count:
                                    let cell = tableView.dequeue() as RateTableViewCell
                                    cell.selectionDelegate = self
                                    cell.FilterCode = "rating"
                                               
                                        return cell
                            case CategoryAttributes.count + 1 :
                            let cell = tableView.dequeue () as ButtonTableViewCell
                            cell.ApplyButton.onTap{ _ in
                                
                                self.viewModel.filterParam["category_id"] = self.CategoryID
                                print(self.viewModel.filterParam.description)
                                self.applyDelegate.didTapapply(filterParams: self.viewModel.filterParam)
                                self.dismiss(animated: true, completion: nil)
                            }
                                           
                                    return cell
                            default:
                              break
                            }
                }
                let cell = tableView.dequeue() as ButtonTableViewCell
                                                       
                                                return cell
             
            }
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                if(indexPath.row < CategoryAttributes.count)
                               {
                switch CategoryAttributes[indexPath.row].swatchType {
                case "text":
                    let numberOfLines = Double(CategoryAttributes[indexPath.row].options.count) / 3.0
            
                    let finalLines = ceil(Double(numberOfLines))
                  
                    return CGFloat(50 * finalLines)
                case "dropdown":
                    let numberOfLines = Double(CategoryAttributes[indexPath.row].options.count) / 3.0
                  
                     let finalLines = ceil(Double(numberOfLines))
                    
                     return CGFloat(50 * finalLines)
                case "color":
                    return 150
                case "slider":
                    return 100
                default:
                    break
                }
                }

                switch indexPath.row {
                
                case CategoryAttributes.count:
                return 100
                case CategoryAttributes.count + 1:
                return 70
                default:
                    return 0
                }
                
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  
            }
    
    
    
      func didTapFilter(filterCode: String, filterValue: String) {
        viewModel.addRemoveFilter(filterCode: filterCode, filterValue: filterValue)
      }

  func didSelectRate(filterCode: String, rateValue: String) {
    viewModel.filterParam[filterCode] = rateValue
    print(viewModel.filterParam.description)
  }

}
