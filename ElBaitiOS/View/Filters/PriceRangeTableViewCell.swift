//
//  PriceRangeTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import TTRangeSlider

class PriceRangeTableViewCell: UITableViewCell {
     
    var CategoryOptions = [CategoryOption]()
    var FilterCode = ""
    var CategoryAttributes = [CategoryAttribute]()
    @IBOutlet weak var PriceRangeSlider: TTRangeSlider!{didSet{
        PriceRangeSlider.handleColor = UIColor.white
        PriceRangeSlider.handleBorderColor = UIColor(hexString: "F4A03B")
        PriceRangeSlider.handleBorderWidth = 2
        PriceRangeSlider.enableStep = false
        }}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(object: CategoryAttribute){
        
        PriceRangeSlider.minValue = (object.options[0].swatchValue! as NSString).floatValue
        PriceRangeSlider.maxValue = (object.options[1].swatchValue! as NSString).floatValue
        PriceRangeSlider.selectedMinimum = (object.options[0].swatchValue! as NSString).floatValue
        PriceRangeSlider.selectedMaximum = (object.options[1].swatchValue! as NSString).floatValue
          CategoryOptions = object.options
          FilterCode = object.code
        
    }
}
