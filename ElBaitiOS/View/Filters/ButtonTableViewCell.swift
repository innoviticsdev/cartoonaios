//
//  ButtonTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
 
    @IBOutlet weak var ApplyButton: TRButton!{didSet{
        ApplyButton.layer.cornerRadius = 10
        }}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
