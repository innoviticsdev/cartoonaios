//
//  RateTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Cosmos

protocol rateSelectionDelegate {
    func didSelectRate(filterCode: String,rateValue: String)
}

class RateTableViewCell: UITableViewCell {
    @IBOutlet weak var FilterName: TRLabelBold!
    @IBOutlet weak var CosmosView: CosmosView!{didSet{
        CosmosView.rating = 0.0
        }}
    
     var selectionDelegate: rateSelectionDelegate!
    var FilterCode = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        CosmosView.didTouchCosmos = { rating in
            
                self.selectionDelegate.didSelectRate(filterCode: self.FilterCode, rateValue: rating.description)
           
        }
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
