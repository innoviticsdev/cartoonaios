//
//  TagsCollectionViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

protocol filterSelectionDelegate {
    func didTapFilter(filterCode: String,filterValue: String)
}

class TagsTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    

    @IBOutlet weak var FilterName: TRLabelBold!
    var CategoryOptions = [CategoryOption]()
    var FilterCode = ""
    @IBOutlet weak var TagsCollectionView: UICollectionView!
    var selectionDelegate: filterSelectionDelegate!
    
    var isTag = true
    override func awakeFromNib() {
        super.awakeFromNib()
       TagsCollectionView.delegate = self
        TagsCollectionView.dataSource = self
        TagsCollectionView.register(UINib(nibName: "TagCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TagCollectionViewCell")
        TagsCollectionView.register(UINib(nibName: "ProductColorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductColorCollectionViewCell")
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CategoryOptions.count
       
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
                if(isTag)
                {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as! TagCollectionViewCell
                    cell.SizeLabel.text = CategoryOptions[indexPath.row].label
                    
                    cell.MainView.onTap { _ in
                        if(cell.MainView.layer.borderColor == UIColor(hexString: "#F4A03B").cgColor){
                            cell.MainView.layer.borderWidth = 0
                            cell.MainView.layer.borderColor  = UIColor.clear.cgColor
                            cell.SizeLabel.textColor = UIColor.black
                        }
                        else{
                            cell.MainView.layer.borderWidth = 1
                            cell.MainView.layer.borderColor  = UIColor(hexString: "#F4A03B").cgColor
                            cell.SizeLabel.textColor = UIColor(hexString: "#F4A03B")
                        }
                        self.selectionDelegate.didTapFilter(filterCode: self.FilterCode, filterValue: self.CategoryOptions[indexPath.row].id.description)
                    }
    
                    return cell
                    
        }
                else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductColorCollectionViewCell", for: indexPath) as! ProductColorCollectionViewCell
                    cell.ColorView.backgroundColor = UIColor(hexString: CategoryOptions[indexPath.row].swatchValue!)
                    
                    cell.ColorView.onTap { _ in
                        if(cell.ColorView.layer.borderColor == UIColor(hexString: "#F4A03B").cgColor){
                            cell.ColorView.layer.borderWidth = 0
                            cell.ColorView.layer.borderColor  = UIColor.clear.cgColor
                            cell.CorrectIcon.alpha = 0
                           
                        }
                        else{
                            cell.ColorView.layer.borderWidth = 1
                            cell.ColorView.layer.borderColor  = UIColor(hexString: "#F4A03B").cgColor
                             cell.CorrectIcon.alpha = 1
                          
                        }

                        self.selectionDelegate.didTapFilter(filterCode: self.FilterCode, filterValue: self.CategoryOptions[indexPath.row].id.description)
                    }
                    
                        return cell
        }


        
          
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if(isTag){
              return CGSize(width: CGFloat((collectionView.frame.size.width / 3)), height: CGFloat(50))
        }
        else{
             return CGSize(width: CGFloat(40), height: CGFloat(40))
        }
        
        

    }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
                

           
            
        }
    
    
    func configure(object: CategoryAttribute,isTagFlag: Bool){
    CategoryOptions = object.options
    FilterName.text = object.name
    FilterCode = object.code
    isTag = isTagFlag
    TagsCollectionView.reloadData()

    }
}


