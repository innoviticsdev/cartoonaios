//
//  MeFooter.swift
//  ElBaitiOS
//
//  Created by mac on 9/2/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class MeFooter: UIView {

    
    @IBAction func goPrivacyPolicy(_ sender: Any) {
        print("goToPrivacy")
        if let url = URL(string: "http://asmio.innsandbox.com/terms.html") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func goTermsOfUse(_ sender: Any) {
        print("goToPrivacy")
        if let url = URL(string: "http://asmio.innsandbox.com/terms.html") {
            UIApplication.shared.open(url)
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> MeFooter {
        return UINib(nibName: "MeFooter", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MeFooter
    }
}
