//
//  ActiveEnum.swift
//  ElBaitiOS
//
//  Created by mac on 8/30/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

enum ActivateState {
    case addCoupon
    case removeCoupon
}
