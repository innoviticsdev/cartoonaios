//
//  ShippingVC.swift
//  ElBaitiOS
//
//  Created by mac on 8/27/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import SimpleCheckbox
import GestureRecognizerClosures

class ShippingVC: UIViewController {

    @IBOutlet weak var DeliveryIcon: UIImageView!
    @IBOutlet weak var DeliveryLabel: TRLabel!
    @IBOutlet weak var checkboxOptions: Checkbox!
    @IBOutlet weak var btnPromocode: UIButton!
    @IBOutlet weak var BottomView: UIView!{didSet{
    BottomView.layer.shadowColor = UIColor.gray.cgColor
    BottomView.layer.shadowOpacity = 0.5
    BottomView.layer.shadowOffset = .zero
    BottomView.layer.shadowRadius = 5
    BottomView.layer.cornerRadius = 20
    }}
    @IBOutlet weak var btnCheckout: UIButton!{didSet{
    btnCheckout.layer.cornerRadius = 10
        btnCheckout.onTap{ _ in

            print(self.lastCheckedCell?.addressDetails?.id )
            
            guard let address = self.lastCheckedCell?.addressDetails?.address1?[0] else {
                self.showAddressAlert()
                return
            }
            guard let addressId = self.lastCheckedCell?.addressDetails?.id else {return}

            self.viewModel.prepareOrder(address: address, addressId: addressId)
        }
    }}
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var txtPromocode: RoundTextField!
    @IBOutlet weak var viewOfStack: UIView!
    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnActivate: UIButton!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var promocode: UILabel!

    @IBOutlet weak var lblTotal: UILabel!
    
    lazy var viewModel: ShippingVM = {
        return ShippingVM()
    }()

    var isChecked: Bool?
    var lastCheckedCell: ShippingAddressCell?
    var firstCheckedCell: ShippingAddressCell?
    var activateState : ActivateState = .addCoupon
    var myCartData: MyCartData!
    var coupon: String!
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getAddresses()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVM()
        initCouponVM()
        initRemoveCouponVM()
        setupViews()
        setUpLabels()
        initPrepareOrderVM()
        
        if(coupon != ""){
            txtPromocode.text = coupon
            setInActiveButton(msg: "ShippingVC-Coupon-msg".autoLocalize())
        }
    }
    
    func setUpLabels(){
        subtotal.text = myCartData.formatedSubTotal
        promocode.text = myCartData.formatedDiscount
        //shipping.text = myCartData.
        lblTotal.text = myCartData.formatedGrandTotal
        if(self.myCartData.freeShipping.status == 0){
            self.DeliveryIcon.alpha = 0
            self.DeliveryLabel.alpha = 0
        }
        else{
            self.DeliveryLabel.text = self.myCartData.freeShipping.message
        }

    }
    
    func setupViews(){
        setupTextField(textField: txtPromocode)
        setUpRoundedBtn(btn: btnPromocode, color: ColorPalette.ButtonColor)
        setupCheckbox(checkBox: checkboxOptions, isChecked: true, isEnabled: false)
            
    }
    
    
    @IBAction func btnVerifyCode(_ sender: Any) {
        
    }
    
    func initVM() {
        
        self.isChecked = true
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)

                case .loading:
                    indicatorSwitch(status: .on,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: false)

                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                    self.isChecked = true
                    self.setupAddresses(arr: self.viewModel.arrAddress)
                }
            }
        }
        
        viewModel.reloadViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                //self?.reloadViews()
            }
        }
    }
    
    
    func setupAddresses(arr: [AddressDetails]){
        
        let arrAddressess = viewModel.arrAddress
       
        //Empty stackView
        decreaseViewsHeight()
        //Fill stackView with arrAddressess
        addViewsHeight(cellsCount: arrAddressess.count)
       
        for item in  arrAddressess{
            let cell = ShippingAddressCell.instanceFromNib()
            
            cell.lblAddress.text = "\(item.locationName)"
            cell.lblDetails.text = "\(item.address1?[0] ?? "" )"
            cell.addressDetails = item

            stackView.addArrangedSubview(cell)
            //stackView.removeArrangedSubview(cell)
            
            setupCheckbox(checkBox: cell.checkbox, isChecked: isChecked ?? false, isEnabled: false)
            
            if(isChecked ?? false){
                firstCheckedCell = cell
                lastCheckedCell = firstCheckedCell
            }
            isChecked = false
            
            cell.onTap{ _ in
                self.changeCheckMark(cell: cell)
            }
            
        }
        
    }
    
    func addViewsHeight(cellsCount : Int) {
       let addedHeight = CGFloat(( 81 * cellsCount ))
        viewHeightConst.constant += addedHeight
        contentViewHeight.constant += addedHeight
    }
    
    func decreaseViewsHeight() {
        stackView.subviews.forEach({
            $0.removeFromSuperview()
            viewHeightConst.constant -= 81
            contentViewHeight.constant -= 81
        })
    }
    
    func changeCheckMark(cell : ShippingAddressCell){
        //if not checked
        if(!cell.checkbox.isChecked){
            //uncheck last checked cell
            if(lastCheckedCell != nil){
                lastCheckedCell?.checkbox.isChecked = false
            } //if no checked cell is cashed
            else{
                firstCheckedCell?.checkbox.isChecked = false
            }
            //check the new cell
            cell.checkbox.isChecked = true
            
            //cashing last checked cell
            lastCheckedCell = cell
        }
        
    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "ShippingVC-Alert-Title".autoLocalize(), message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "ShippingVC-Alert-OK".autoLocalize(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAddressAlert() {
        let alert = UIAlertController(title: "ShippingVC-Alert-Title".autoLocalize(),
                                      message: "ShippingVC-save-Address".autoLocalize(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ShippingVC-Add-New".autoLocalize(),
                                      style: UIAlertAction.Style.default,
        handler: {(alert: UIAlertAction!) in
            self.presentAddAddress()
        }))
        

        let cancelAlert = UIAlertAction(title: "ShippingVC-Alert-Cancel".autoLocalize(), style: UIAlertAction.Style.default, handler:nil)
        cancelAlert.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(cancelAlert)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goAddAddress(_ sender: Any) {
        presentAddAddress()
    }
    
    func presentAddAddress() {
        let vc = AddNewAddressViewController()
        //vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func activateButton(_ sender: Any) {
        useCouponToggle(promoCode: txtPromocode.text ?? "" )
    }
    
    func useCouponToggle(promoCode: String) {
        //print(txtPromocode.text!)
        if(promoCode != ""){
            if(activateState == .addCoupon){
                viewModel.getCoupon(promoCode: promoCode)
            }else if(activateState == .removeCoupon){
                viewModel.removeCoupon()
            }
        }else{
            lblError.text = "ShippingVC-Enter-Valid-Coupon" .autoLocalize()
            lblError.textColor = UIColor.red
        }
    }
    @IBAction func goContinue(_ sender: Any) {
        
    }
    
    func setActiveButton(){
        lblError.textColor = .red
        lblError.text = self.viewModel.message
        btnActivate.backgroundColor = ColorPalette.ButtonColor
        btnActivate.setTitle("ShippingVC-ActivateButton".autoLocalize(), for: .normal)
        activateState = .addCoupon
        self.txtPromocode.isEnabled = true
    }
    
    func setInActiveButton(msg: String){
        self.lblError.textColor = .blue
        self.lblError.text = msg
        self.btnActivate.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.6274509804, blue: 0.231372549, alpha: 1)
        self.btnActivate.setTitle("ShippingVC-ActivatedButton".autoLocalize(), for: .normal)
        self.activateState = .removeCoupon
        self.txtPromocode.isEnabled = false
    }
}








extension ShippingVC{
    
    func initCouponVM(){
        
           viewModel.updateCouponStatus = { [weak self] () in
               guard let self = self else {
                   return
               }

               DispatchQueue.main.async { [weak self] in
                   guard let self = self else {
                       return
                   }
                   switch self.viewModel.addCouponState {
                   case .empty, .error:
                       indicatorSwitch(status: .off,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: true)
                       print("Error")
                       self.setActiveButton()

                   case .loading:
                       indicatorSwitch(status: .on,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: false)
                   case .populated:
                       indicatorSwitch(status: .off,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: true)
                       print("Populated")
                       self.setInActiveButton(msg: self.viewModel.message)
                       self.myCartData = self.viewModel.myCartData
                       self.setUpLabels()

                   }
               }
           }
    }
    
}




extension ShippingVC{

    func initRemoveCouponVM(){

           viewModel.updateRemoveCouponStatus = { [weak self] () in
               guard let self = self else {
                   return
               }

               DispatchQueue.main.async { [weak self] in
                   guard let self = self else {
                       return
                   }
                   switch self.viewModel.removeCouponState {
                   case .empty, .error:
                       print("Error")
                       self.setInActiveButton(msg: self.viewModel.message)
                       toggleCardSwipe(viewController : self, allowSwipe: true)

                   case .loading:
                       indicatorSwitch(status: .on,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: false)

                   case .populated:
                       indicatorSwitch(status: .off,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: true)

                       print("Populated")
                       self.setActiveButton()
                       
                       self.myCartData = self.viewModel.myCartData
                       self.setUpLabels()
                   }
               }
           }
    }
}




extension ShippingVC{

    func initPrepareOrderVM(){

           viewModel.updatePrepareOrderStatus = { [weak self] () in
               guard let self = self else {
                   return
               }

               DispatchQueue.main.async { [weak self] in
                   guard let self = self else {
                       return
                   }
                   switch self.viewModel.prepareState {
                   case .empty, .error:
                       print("Error")
                       indicatorSwitch(status: .off,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: true)

                   case .loading:
                       indicatorSwitch(status: .on,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: false)

                   case .populated:
                       indicatorSwitch(status: .off,view: self.view)
                       toggleCardSwipe(viewController : self, allowSwipe: true)
                       print("Populated")
                       
                       let vc = OrderSummaryVC()
                       vc.perorderModel = self.viewModel.perorderModel
                       //vc.modalPresentationStyle = .fullScreen
                       self.present(vc, animated: true, completion: nil)
                       
                   }
               }
           }
    }
}



















//        let view1 = UIView()
//        view1.backgroundColor = .white
//        viewHeightConst.constant += 61
//
//
//        //create Checkbox
//        let checkbox1 = Checkbox()
//        setupCheckbox(checkBox: checkbox1, isChecked: true)
//        //add Checkbox to subview
//        view1.addSubview(checkbox1)
//        //constraints
//        checkbox1.snp.makeConstraints{
//            (make) -> Void in
//            make.leading.equalToSuperview().inset(21.5)
//            make.top.equalToSuperview().inset(16.5)
//            make.height.width.equalTo(16)
//        }
//
//        //create label
//        let label1 = UILabel()
//        label1.text = "HOME"
//        label1.textColor = .black
//        //add label to subview
//        view1.addSubview(label1)
//        //constraints
//        //make.top.equalTo(viewProgress.snp.bottom).offset(32)
//        label1.snp.makeConstraints{
//            (make) -> Void in
//            make.leading.equalTo(checkbox1.snp.trailing).offset(8)
//            make.top.equalToSuperview().inset(15)
//        }
//
//        //create label
//        let label2 = UILabel()
//        label2.text = "Loerm epism Loerm epism"
//        label2.textColor = .black
//        //add label to subview
//        view1.addSubview(label2)
//        //constraints
//        //make.top.equalTo(viewProgress.snp.bottom).offset(32)
//        label2.snp.makeConstraints{
//            (make) -> Void in
//            make.leading.equalTo(checkbox1.snp.trailing).offset(8)
//            make.top.equalTo(label1.snp.bottom).inset(4)
//        }
//
//        stackView.addArrangedSubview(view1)
//
        //let checkbox = Checkbox(frame: CGRect(x: 50, y: 50, width: 25, height: 25))
        
//        let view2 = UIView()
//        view2.backgroundColor = .green
//        viewHeightConst.constant += 61
//        stackView.addArrangedSubview(view2)
//
//        let view3 = UIView()
//        view3.backgroundColor = .red
//        viewHeightConst.constant += 61
//        stackView.addArrangedSubview(view3)
//
//        view1.snp.makeConstraints{
//            (make) -> Void in
//            make.height.width.equalTo(50)
//        }
//        view2.snp.makeConstraints{
//            (make) -> Void in
//            make.height.width.equalTo(50)
//        }
//        view3.snp.makeConstraints{
//            (make) -> Void in
//            make.height.width.equalTo(50)
//        }



//        let cell1 = ShippingAddressCell.instanceFromNib()
//        viewHeightConst.constant += 61
//        contentViewHeight.constant += 61
//        stackView.addArrangedSubview(cell1)
//        setupCheckbox(checkBox: cell1.checkbox, isChecked: true)
//
//        let cell2 = ShippingAddressCell.instanceFromNib()
//        viewHeightConst.constant += 61
//        contentViewHeight.constant += 61
//        stackView.addArrangedSubview(cell2)
//        setupCheckbox(checkBox: cell2.checkbox, isChecked: false)
//
//        let cell3 = ShippingAddressCell.instanceFromNib()
//        viewHeightConst.constant += 61
//        contentViewHeight.constant += 61
//        stackView.addArrangedSubview(cell3)
//        setupCheckbox(checkBox: cell3.checkbox, isChecked: false)
