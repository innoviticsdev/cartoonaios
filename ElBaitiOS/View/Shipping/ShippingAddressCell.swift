//
//  ShippingAddressCell.swift
//  ElBaitiOS
//
//  Created by Mohamed El Sawi on 8/29/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import SimpleCheckbox
class ShippingAddressCell: UIView {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var checkbox: Checkbox!
    
    var addressDetails : AddressDetails?
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> ShippingAddressCell {
        return UINib(nibName: "ShippingAddressCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ShippingAddressCell
    }
    
}

