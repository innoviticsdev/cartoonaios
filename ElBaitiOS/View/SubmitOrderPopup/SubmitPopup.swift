//
//  SubmitOrderPopup.swift
//  ElBaitiOS
//
//  Created by mac on 8/31/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class SubmitPopup: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var roundedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeRoundedCorners(view: contentView)
        setUpRoundedBtn(btn: roundedButton, color: ColorPalette.ButtonColor)
    }

    @IBAction func closePopup(_ sender: Any) {
        print("closePopup")
        
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func goTrackOrder(_ sender: Any) {
        print("goTrackOrder")
        
        let vc = MyOrdersViewController()
        vc.closeClearStack = true
        self.present(vc, animated: true, completion: nil)
    }
}
