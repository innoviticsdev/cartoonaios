//
//  SearchVC.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/20/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class SearchVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var SearchTextField: UITextField!
    @IBOutlet weak var CloseButton: CardView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchTextField.delegate = self
        CloseButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
    }

    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        print(textField.text)
    }

}
