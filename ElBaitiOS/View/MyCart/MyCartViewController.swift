//
//  MyCartViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/12/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
import DZNEmptyDataSet

class MyCartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var DeliveryLabel: TRLabel!
    @IBOutlet weak var DeliveryIcon: UIImageView!
    @IBOutlet weak var TotalLabel: UILabel!
    @IBOutlet weak var SubTotalLabel: UILabel!
    @IBOutlet weak var MyCartTableView: UITableView!
    @IBOutlet weak var CheckOutButton: UIButton!{didSet{
        CheckOutButton.layer.cornerRadius = 10
        }}
    @IBOutlet weak var CloseButtonView: UIButton!{didSet{
        CloseButtonView.layer.shadowColor = UIColor.gray.cgColor
        CloseButtonView.layer.shadowOpacity = 0.5
        CloseButtonView.layer.shadowOffset = .zero
        CloseButtonView.layer.shadowRadius = 3
        CloseButtonView.layer.cornerRadius = 10
        }}
    @IBOutlet weak var TopView: UIView!{didSet{
    TopView.layer.shadowColor = UIColor.gray.cgColor
    TopView.layer.shadowOpacity = 0.5
    TopView.layer.shadowOffset = .zero
    TopView.layer.shadowRadius = 3
    }}
    
    
    @IBOutlet weak var SubTotalView: UIView!
    @IBOutlet weak var BottomView: UIView!{didSet{
    BottomView.layer.shadowColor = UIColor.gray.cgColor
    BottomView.layer.shadowOpacity = 0.5
    BottomView.layer.shadowOffset = .zero
    BottomView.layer.shadowRadius = 5
    BottomView.layer.cornerRadius = 20
    }}
    
    //ramzy
    var myCartData: MyCartData?
    
    lazy var viewModel: MyCartVM = {
        return MyCartVM()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        MyCartTableView.delegate = self
        MyCartTableView.dataSource = self
        
        MyCartTableView.emptyDataSetSource = self
        MyCartTableView.emptyDataSetDelegate = self
        
        MyCartTableView.register(UINib(nibName: "MyCartCellTableViewCell", bundle: nil), forCellReuseIdentifier: "MyCartCellTableViewCell")
        
        CloseButtonView.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshCart), name: NSNotification.Name(rawValue: "RefreshCart"), object: nil)
        initVM()

        CheckOutButton.onTap{ _ in
            if(checkIfSignedIn())
            {
                let vc = ShippingVC()
                vc.myCartData = self.myCartData
                vc.coupon = self.viewModel.copoun
                //vc.modalPresentationStyle = .automatic
                self.present(vc, animated: true, completion: nil)
            }
            else{
            let Signin = SignInVC()
            Signin.modalPresentationStyle = .popover
            self.present(Signin, animated: true, completion: nil)
            }
            
        }

        // Do any additional setup after loading the view.
    }
    
     @objc func RefreshCart() {

     viewModel.initFetch()

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
         viewModel.initFetch()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.ItemsArray.count
       }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "MyCartCellTableViewCell", for: indexPath) as! MyCartCellTableViewCell
            
            var ItemCount = Int(viewModel.ItemsArray[indexPath.row].quantity)
            
            cell.ProductImage.kf.setImage(with: URL(string: viewModel.ItemsArray[indexPath.row].product.baseImage.originalImageURL))
            cell.ProductName.text = viewModel.ItemsArray[indexPath.row].product.name
            cell.ProductPrice.text = viewModel.ItemsArray[indexPath.row].product.finalPrice
            cell.ProductCountLabel.text = viewModel.ItemsArray[indexPath.row].quantity
            cell.DeleteButton.onTap { _ in
                self.viewModel.removeItemParam = ["item_id":self.viewModel.ItemsArray[indexPath.row].id.description]
                self.viewModel.fetchRemoveItem()
             
                
            }
            
            cell.PlusButton.onTap { _ in
                ItemCount = ItemCount! + 1
                self.viewModel.updateCartParam = ["qty[" + self.viewModel.ItemsArray[indexPath.row].id.description + "]":ItemCount!.description]
                self.viewModel.updateCartAPI()
                
            }
            
            cell.MinusButton.onTap { _ in
                ItemCount = ItemCount! - 1
                //If item count != 0
                if(ItemCount ?? 0 > 1){
                    self.viewModel.updateCartParam = ["qty[" + self.viewModel.ItemsArray[indexPath.row].id.description + "]":ItemCount!.description]
                    self.viewModel.updateCartAPI()
                }
            }
         
           return cell
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 105
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       }
    
    
    
    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            self.MyCartTableView.reloadData()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                case .loading:
                    indicatorSwitch(status: .on,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: false)
                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                    self.SubTotalLabel.text = self.viewModel.myCartData.formatedSubTotal
                    self.myCartData = self.viewModel.myCartData
                    self.TotalLabel.text = self.viewModel.myCartData.formatedGrandTotal
                    if(self.viewModel.myCartData.freeShipping.status == 0){
                        self.DeliveryIcon.alpha = 0
                        self.DeliveryLabel.alpha = 0
                    }
                    else{
                        self.DeliveryLabel.text = self.viewModel.myCartData.freeShipping.message
                    }
                }
            }
        }

      
       
        //viewModel.initFetch()

    }
    
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}










extension MyCartViewController: DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        switch self.viewModel.state {
                       case .empty, .error:
                        bottomViewVisibility(visible: false)
                           return true
                       case .loading:
                        bottomViewVisibility(visible: false)
                            return false
                       case .populated:
                        bottomViewVisibility(visible: true)
                          return false

        
        }
        }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                        str = "DZNEmpty-Something-Wrong".autoLocalize()
                        break
                       case .empty:
                        str = "DZNEmpty-Cart-Empty".autoLocalize()
                        break
                        default:
                           str = ""
                        break
        }
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                        str = "DZNEmpty-Internet-Connection".autoLocalize()
                        break
                       case .empty:
                        str = "DZNEmpty-Check-Bestsellers".autoLocalize()
                        break
                        default:
                           str = ""
                        break
        }
     
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let str: String!
        switch self.viewModel.state {
                              case .error:
                                str = "DZNEmpty-Btn-Retry".autoLocalize()
                               break
                              case .empty:
                                str = "DZNEmpty-Btn-Check-Bestsellers".autoLocalize()
                               break
                               default:
                                  str = ""
                               break
               }
        
    
        

        
       if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let buttonFrame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 50)
        let buttonContainer = UIView(frame: buttonFrame)
        buttonContainer.backgroundColor = scrollView.backgroundColor

        let imageView = UIImageView(frame: buttonFrame)
        imageView.backgroundColor = ColorPalette.primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        buttonContainer.addSubview(imageView)
        return createImage(buttonContainer)

//        let capInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
//        var rectInsets: UIEdgeInsets = .zero
//        var imageName = ""
//
//        if state == .normal {
//            imageName = "dz-empty-button-bg"
//        }
//        if state == .highlighted {
//            imageName = "dz-empty-button-bg"
//        }
//
//        rectInsets = UIEdgeInsets(top: -19.0, left: -61.0, bottom: -19.0, right: -61.0)
//        let image = UIImage(named: imageName, in: Bundle(for: type(of: self)), compatibleWith: nil)
//
//        return image?.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
         
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        switch self.viewModel.state {
                       case .error:
                          viewModel.initFetch()
                        break
                       case .empty:
                          self.dismiss(animated: true, completion: nil)
                        break
                        default:
                        break
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        var image: UIImage?
        switch self.viewModel.state {
                              case .error:
                                  image = UIImage(named: "connection-error")
                               break
                              case .empty:
                                 image = UIImage(named: "empty-cart")
                               break
                               default:
                                  image = UIImage(named: "empty-cart")
                               break
               }
        return image
    }
    func createImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: view.frame.width, height: view.frame.height), true, 1)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func bottomViewVisibility(visible: Bool)
    {
        if(visible)
        {
            SubTotalView.alpha = 1
            BottomView.alpha = 1
        }
        else
        {
            SubTotalView.alpha = 0
            BottomView.alpha = 0
        }
    }
}
