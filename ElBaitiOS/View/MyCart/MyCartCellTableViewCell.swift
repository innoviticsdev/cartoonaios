//
//  MyCartCellTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/12/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class MyCartCellTableViewCell: UITableViewCell {

    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var ProductPrice: UILabel!
    @IBOutlet weak var ProductColor: UILabel!
    @IBOutlet weak var ProductSize: UILabel!
    @IBOutlet weak var ProductCountLabel: UILabel!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var MinusButton: UIView!{didSet{
        MinusButton.layer.cornerRadius = 5
        }}
    @IBOutlet weak var PlusButton: UIView!{didSet{
        PlusButton.layer.cornerRadius = 5
        }}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
