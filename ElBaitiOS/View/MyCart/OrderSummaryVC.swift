//
//  ShippingVC.swift
//  ElBaitiOS
//
//  Created by mac on 8/27/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import SimpleCheckbox
import GestureRecognizerClosures

class OrderSummaryVC: UIViewController {

    var perorderModel: PerorderModel!
    var myCartData : CartPerorderModel?
    
    @IBOutlet weak var HomeAddressView: UIView!
    @IBOutlet weak var lblPaymenthMetod: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblPromocode: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblShippingStatus: TRLabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    
    @IBOutlet weak var CloseButton: UIButton!{didSet{
        CloseButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
        }}
    
    @IBOutlet weak var BottomView: UIView!{didSet{
    BottomView.layer.shadowColor = UIColor.gray.cgColor
    BottomView.layer.shadowOpacity = 0.5
    BottomView.layer.shadowOffset = .zero
    BottomView.layer.shadowRadius = 5
    BottomView.layer.cornerRadius = 20
    }}
    
    @IBOutlet weak var btnCheckout: UIButton!{didSet{
    btnCheckout.layer.cornerRadius = 10
        btnCheckout.onTap{ _ in
           print("btnCheckout")
           self.viewModel.submitOrder()
        }
    }}
    
    @IBOutlet weak var stackView: UIStackView!
  
    @IBOutlet weak var viewOfStack: UIView!
    
    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressDetails: UILabel!
    
    lazy var viewModel: SubmitOrderVM = {
        return SubmitOrderVM()
    }()

    override func viewWillAppear(_ animated: Bool) {
       if(self.myCartData?.freeShipping?.status == 0){
           self.imgIcon.alpha = 0
           self.lblShippingStatus.alpha = 0
       }
       else{
           self.lblShippingStatus.text = self.myCartData?.freeShipping?.message ?? "Found Nil!"
           //self.myCartData?.freeShipping?.status
           print("xXx: \(String(describing: self.myCartData?.freeShipping?.status))")
           print("xXx: \(String(describing: self.myCartData?.freeShipping?.message))")
       }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
                
        
        initVM()
        setupItems()

        myCartData = perorderModel.data?.cart
        
        print("Hello from sumary total is \(myCartData?.grandTotal ?? "undifiend")")
        setupLabels()
        
        HomeAddressView.onTap { _ in
            self.present(AddressesVC(), animated: true, completion: nil)
        }
        
    }
    
    func initVM() {
                
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                case .loading:
                    indicatorSwitch(status: .on,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: false)
                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                    //self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                    let vc = SubmitPopup()
                    presentBottomPopup(view: self, popupVC: vc)
                }
            }
        }
        
        /*func goLogin(email: String){
            let vc = SignInPasswordVC()
            vc.userEmail = email
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }*/
        
        viewModel.reloadViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                //self?.reloadViews()
            }
        }
    }
    
    func setupLabels(){
        lblSubTotal.text = myCartData?.formatedSubTotal
        lblPaymenthMetod.text = "OrderSummaryVC-CashOnDelivery".autoLocalize()
        lblPromocode.text = myCartData?.formatedDiscount
        lblShipping.text = (myCartData?.selectedShippingRate.price == "0") ? "MyOrdersVC-Free".autoLocalize() : myCartData?.selectedShippingRate.formatedPrice
        lblTotal.text = myCartData?.formatedGrandTotal
        
        lblAddress.text = perorderModel.data?.cart.shippingAddress.locationName
        lblAddressDetails.text = perorderModel.data?.cart.shippingAddress.address1[0]
    }
 
    func setupItems() {
        let arr = perorderModel.data?.cart.items
        changeViewsHeight(cellsCount: arr?.count ?? 0)
        arr!.forEach{
            let cell = OrderSummaryProduct.instanceFromNib()
            cell.title.text = "\($0.name)"
            cell.productQuantity.text = "OrderSummaryVC-QTY".autoLocalize() + " \($0.quantity)"
            cell.total.text = "\($0.formatedTotal)"
            cell.imgView.kf.setImage(with: URL(string: "\($0.product.baseImage.smallImageURL)"))
            
           
            stackView.addArrangedSubview(cell)
        }
    }

    func changeViewsHeight(cellsCount : Int) {
       let addedHeight = CGFloat(( 65 * cellsCount ))
        viewHeightConst.constant += addedHeight
        contentViewHeight.constant += addedHeight
    }
    
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "ShippingVC-Alert-Title".autoLocalize(), message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "ShippingVC-Alert-OK".autoLocalize(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
 
    
}
