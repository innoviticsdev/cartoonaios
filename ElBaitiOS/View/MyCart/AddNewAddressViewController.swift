//
//  AddNewAddressViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/27/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
import GooglePlaces
import GoogleMaps
import SnapKit

class AddNewAddressViewController: UIViewController {
    @IBOutlet weak var CloseButton: UIView!{didSet{
        CloseButton.onTap { _ in
            self.dismiss(animated: true, completion: nil)
        }
    }}
    
   
    @IBOutlet weak var btnAddLocation: TRButtonBold!
    @IBOutlet weak var GMapsView: GMSMapView!
    @IBOutlet weak var GoogleMapLogoView: UIView!
    @IBOutlet weak var SearchView: UIView!
    @IBOutlet weak var LocationServicePermissionDisabledWarning: UIView!
    @IBOutlet weak var TurnOnLocationButton: UIButton!
    @IBOutlet weak var GetCurrentLocationView: CardView!
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var setPickupButton: UIButton!{didSet{
        setPickupButton.layer.cornerRadius = 10
        }}
    @IBOutlet weak var searchLabel: UILabel!
    
    var locationManager = CLLocationManager()
    
    lazy var viewModel: AddNewAddressVM = {
        return AddNewAddressVM()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPickupButton.onTap { _ in
            let AddressDetailsVC = AddressDetailsViewController()
            AddressDetailsVC.locationAddrss = self.viewModel.locationAddrss
            AddressDetailsVC.locationCountryName = self.viewModel.locationCountryName
            AddressDetailsVC.locationCity = self.viewModel.locationCity
            self.present(AddressDetailsVC, animated: true, completion: nil)
        }
        
        initVM()
        
        //This button is responsible for presenting the search view for certain address using google maps autoComplete search
         SearchView.onTap { _ in
                    let filter = GMSAutocompleteFilter()
                    filter.country = "EG"
               
                    let autocompleteController = GMSAutocompleteViewController()
                    //autocompleteController.modalPresentationStyle = .fullScreen
                    autocompleteController.autocompleteFilter = filter
                    autocompleteController.delegate = self
                    
                    self.present(autocompleteController, animated: true, completion: nil)
                }
        
        GetCurrentLocationView.onTap { _ in
            self.checkLocationPermissionStatus()
            self.locationManager.startUpdatingLocation()
            
        }
            
        
      //This Button is responsible for allowing user to allow the app to use location service in order to optain current location on Map
        TurnOnLocationButton.onTap { _ in
                self.TurnOnLocationService()
            }
        intializeMaps()
        btnAddLocation.backgroundColor = #colorLiteral(red: 0.5254901961, green: 0.5254901961, blue: 0.5254901961, alpha: 1)
        btnAddLocation.isEnabled = false
        
    }
    
    func initVM() {
        
       

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    self.view.hideProgress()
                   
                case .loading:
                    self.view.showProgress(isDim: true)
              
                case .populated:
                    self.view.hideProgress()
                    self.pickUpLocationLabel.text = self.viewModel.locationAddrss
                    self.btnAddLocation.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
                    self.btnAddLocation.isEnabled = true
                    UIView.animate(withDuration: 0.2, animations: {
                     
                    })
                }
            }
        }

      
       
  

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(locationManager != nil)
        {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func intializeMaps()
    {
        self.GMapsView.delegate = self
        GMapsView.subviews.count > 0 ? (GMapsView.subviews[1].alpha = 0) : nil
       
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        self.locationManager.requestWhenInUseAuthorization()

        showLocationPin()
        checkLocationPermissionStatus()
    }
    
    func checkLocationPermissionStatus()
    {
        switch viewModel.locationServicePermissionStatus
        {
        case true:
            UIView.animate(withDuration: 0.3, animations: {
                    self.LocationServicePermissionDisabledWarning.alpha = 0
            })
        case false:
             UIView.animate(withDuration: 0.3, animations: {
                    self.LocationServicePermissionDisabledWarning.alpha = 1
             })
        }
    }
    
    func TurnOnLocationService()
          {
              guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                  return
              }
              if UIApplication.shared.canOpenURL(settingsUrl) {
                  if #available(iOS 10.0, *) {
                      UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                          print("Settings opened: \(success)") // Prints true
                      })
                  }
              }
          }

    func showLocationPin()
      {
          let apnImage = UIImage(named: "location-pin")
          
          let pinMarker = UIImageView(image: apnImage)
          pinMarker.translatesAutoresizingMaskIntoConstraints = false
          pinMarker.center = self.view.center;
    
          self.view.addSubview(pinMarker)
          
          pinMarker.snp.makeConstraints({ (make) in
              make.height.equalTo(60)
              make.centerX.equalTo(self.view)
              make.centerY.equalTo(self.view).offset(-15)
          })
        
          pinMarker.contentMode = .scaleAspectFit
      }
 

}

extension AddNewAddressViewController:GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        viewModel.mapView(mapView: mapView, idleAt: position)
        checkLocationPermissionStatus()
    }
}

extension AddNewAddressViewController:CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        GMapsView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
        //Finally stop updating location otherwise it will come again and again in this delegate
       self.locationManager.stopUpdatingLocation()
       
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
}

extension AddNewAddressViewController:GMSAutocompleteViewControllerDelegate
{
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    
       let notifiDict:[String: String] = ["lat":place.coordinate.latitude.description , "long": place.coordinate.longitude.description]
        searchLabel.text =  place.formattedAddress!
        dismiss(animated: true, completion: {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "goToLocation"), object: nil, userInfo: notifiDict)
        })
        
    }
  
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
     // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
     
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
