//
//  AddressDetailsViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/29/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures

class AddressDetailsViewController: UIViewController {
    
    @IBOutlet weak var FlatNumberTF: RoundTextField!
    @IBOutlet weak var DetailsTF: RoundTextField!
    @IBOutlet weak var FloorNumberTF: RoundTextField!
    @IBOutlet weak var BuildingNumberTF: RoundTextField!
    @IBOutlet weak var LocationNameTF: RoundTextField!
    @IBOutlet weak var AddressLabel: UILabel!
    var locationAddrss: String?
    var locationCity: String?
    var locationCountryName: String?
    
    @IBOutlet weak var lblErrorlocation: TRLabel!
    @IBOutlet weak var lblErrorBuilding: TRLabel!
    @IBOutlet weak var lblErrorFloor: TRLabel!
    
    lazy var viewModel: AddressDetailsVM = {
           return AddressDetailsVM()
       }()

    @IBOutlet weak var SaveAddressButton: UIButton!{didSet{
        SaveAddressButton.layer.cornerRadius = 10
        }}
    @IBOutlet weak var BottonView: UIView!{didSet{
        roundedViewWithShadow(view: BottonView)
        }}
    @IBOutlet weak var CloseButtonView: UIButton!{didSet{
        CloseButtonView.onTap { _ in
            self.dismiss(animated: true, completion: nil)
        }
        }}
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        initVM()
        SaveAddressButton.onTap { _ in
            self.clearErrors()
            if(self.LocationNameTF.text == ""){
                self.lblErrorlocation.alpha = 1
                return
            }else if(self.BuildingNumberTF.text == ""){
                self.lblErrorBuilding.alpha = 1
                return
            }else if(self.FloorNumberTF.text == ""){
                self.lblErrorFloor.alpha = 1
                return
            }else{
                self.viewModel.addressDetailsParam = ["address1[0]":self.locationAddrss!,"city":self.locationCity!,"country_name":self.locationCountryName!,"location_name":self.LocationNameTF.text!,"building_no":self.BuildingNumberTF.text!,"floor_no":self.FloorNumberTF.text!,"flat_no":self.FlatNumberTF.text!,"details":self.DetailsTF.text!]
                self.viewModel.saveAddressAPI()
            }
        }
        // Do any additional setup after loading the view.
    }
    
    func setupViews() {
        setupTextField(textField: FlatNumberTF)
        setupTextField(textField: DetailsTF)
        setupTextField(textField: FloorNumberTF)
        setupTextField(textField: BuildingNumberTF)
        setupTextField(textField: LocationNameTF)
        clearErrors()
    }
    
    func clearErrors(){
        lblErrorlocation.alpha = 0
        lblErrorBuilding.alpha = 0
        lblErrorFloor.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AddressLabel.text = locationAddrss
    }

    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    self.view.hideProgress()
                   
                case .loading:
                    self.view.showProgress(isDim: true)
              
                case .populated:
                    self.view.hideProgress()
                 
                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                }
            }
        }

      
       
   

    }

    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "ShippingVC-Alert-Title".autoLocalize(), message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "ShippingVC-Alert-OK".autoLocalize(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }


}
