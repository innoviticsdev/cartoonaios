//
//  OrderSummaryProduct.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/29/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class OrderSummaryProduct: UIView {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    
    override class func awakeFromNib() {
         super.awakeFromNib()
         //lblTest.text = "Ahmed"

     }
     
     class func instanceFromNib() -> OrderSummaryProduct {
         return UINib(nibName: "OrderSummaryProduct", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OrderSummaryProduct
     }

}
