//
//  CategoryCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Kingfisher

class CategoryCell: UITableViewCell {

    @IBOutlet weak var CategoryName: UILabel!
    
    @IBOutlet weak var GradientView: UIView!{didSet{
    GradientView.layer.cornerRadius = 10
       // GradientView.layerGradient()
        
    }}
    @IBOutlet weak var CategoryImage: UIImageView!{didSet{
    CategoryImage.layer.cornerRadius = 10

    }}
    @IBOutlet weak var MainView: UIView!{didSet{
        MainView.layer.cornerRadius = 10
        }}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print("HeyMainView \(self.MainView.frame.size.width)")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
 
    func configureCell(lblName: String,imgView: String){
        CategoryName.text = lblName
        CategoryImage.kf.setImage(with: URL(string: imgView))
        
    }
    
    //Flag for gridient layout 
    var flagLayout = true
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if(flagLayout){
            GradientView.layerGradient(size: self.frame.size)
            GradientView.clipsToBounds = true
            flagLayout = false
        }
    }
    
    
}

extension UIView {
    func layerGradient(size: CGSize) {
        let layer : CAGradientLayer = CAGradientLayer()
        //layer.frame.size = self.frame.size
       // self.layoutIfNeeded()
        print("Hey2 \(self.frame.size.width)")

        layer.frame.size.height = self.bounds.height
        layer.frame.size.width = size.width - 30
        //layer.frame.size = self.bounds.size

        layer.frame.origin = CGPoint(x: 0.0,y: 0.0)
        //layer.backgroundColor = UIColor.red.cgColor
        //layer.cornerRadius = CGFloat(frame.width / 20)

        let color1 = UIColor(red:0, green:0, blue: 0, alpha:0.6).cgColor
        let color2 = UIColor(red:0, green:0, blue: 0, alpha:0.5).cgColor
        let color3 = UIColor(red:0, green:0, blue: 0, alpha:0.4).cgColor
        let color4 = UIColor(red:0, green:0, blue: 0, alpha:0.3).cgColor
        let color5 = UIColor(red:0, green:0, blue: 0, alpha:0.2).cgColor
        let color6 = UIColor(red:0, green:0, blue: 0, alpha:0.1).cgColor
        let color7 = UIColor(red:0, green:0, blue: 0, alpha:0.0).cgColor

        layer.colors = [color1,color2,color3,color4,color5,color6,color7].reversed()
        self.layer.insertSublayer(layer, at: 0)
    }
}
