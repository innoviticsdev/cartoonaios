//
//  CategoriesTabViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import ESPullToRefresh

class CategoriesTabViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var CategoryTableView: UITableView!
    var window: UIWindow!
    lazy var viewModel: CategoriesVM = {
        return CategoriesVM()
    }()
    
    @IBOutlet weak var TopView: UIView!{didSet{
        TopView.layer.shadowColor = UIColor.black.cgColor
        TopView.layer.shadowOpacity = 0.3
        TopView.layer.shadowOffset = .zero
        TopView.layer.shadowRadius = 2
        }}
    
    var indicatorIsOn : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoryTableView.delegate = self
        CategoryTableView.dataSource = self
        CategoryTableView.registerCellNib(cellClass: CategoryCell.self)
        
        CategoryTableView.emptyDataSetSource = self
        CategoryTableView.emptyDataSetDelegate = self
        
        initVM()
        // Do any additional setup after loading the view.
        
//        self.CategoryTableView.es.addPullToRefresh {
//                          [weak self] in
//            self?.viewModel.currentPage = 1
//            self?.viewModel.initFetch(refreshType: .refresh)
//            }
            self.CategoryTableView.es.addInfiniteScrolling {
            [weak self] in
            self?.viewModel.initFetch(refreshType: .loadmore)
        }
    }
    
  
    func confNavBar()
     {
        navigationController?.navigationBar.topItem!.title = "CategoriesVC-Title-Categories".autoLocalize()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        //navigationController?.navigationBar.isTranslucent = false
     
        if #available(iOS 13.0, *) {
                 let navBarAppearance = UINavigationBarAppearance()
                 navBarAppearance.configureWithOpaqueBackground()
                 navBarAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                 navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                 navBarAppearance.backgroundColor = .white
                 navigationController?.navigationBar.standardAppearance = navBarAppearance
                 navigationController?.navigationBar.compactAppearance = navBarAppearance
                 navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
                 navigationController?.navigationBar.tintColor = .white
             } else {
                 navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                 navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                 navigationController?.navigationBar.prefersLargeTitles = true
                 navigationController?.navigationBar.barTintColor = .white
                 navigationController?.navigationBar.tintColor = .white
             }
       }
   
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arrCategories.count

    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.configureCell(lblName: viewModel.arrCategories[indexPath.row].name, imgView: viewModel.arrCategories[indexPath.row].imageURL)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let CategoriesAttachedVC = CategoriesAttached(categoryID: viewModel.arrCategories[indexPath.row].id.description,categoryName: viewModel.arrCategories[indexPath.row].name)
        CategoriesAttachedVC.CategoryAttributes = viewModel.arrCategories[indexPath.row].attributes
        self.navigationController?.pushViewController(CategoriesAttachedVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         confNavBar()
        
    }
    
    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            self.CategoryTableView.reloadData()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                
                case .noMore:
                    self.indicatorIsOn = false
                    indicatorSwitch(status: .off,view: self.view)
                    
                    self.CategoryTableView.es.noticeNoMoreData()
                    //self.CategoryTableView.es.stopLoadingMore()
                
                case .loading:
                    self.indicatorIsOn = true
                    indicatorSwitch(status: .on,view: self.view)
                
                case .populated:
                    self.indicatorIsOn = false
                    indicatorSwitch(status: .off,view: self.view)
                    
                    self.CategoryTableView.es.stopPullToRefresh()
                    self.CategoryTableView.es.stopLoadingMore()
                    
                    indicatorSwitch(status: .off,view: self.view)
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.CategoryTableView.reloadData()
            }
        }
        
        viewModel.initFetch(refreshType: .refresh)

    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



extension CategoriesTabViewController: DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        switch self.viewModel.state {
                       case .empty, .error:
                           return true
                       case .loading:
                            return false
                       case .noMore:
                        return false
                       case .populated:
                          return false
        }
            
        }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                            str = "DZNEmpty-Something-Wrong".autoLocalize()
                        break
                       case .empty:
                          str = "No Available Categories"
                        break
                        default:
                           str = ""
                        break
        }
        if(Localize.getLocal() == "en"){
               let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
               else{
                    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                          str = "DZNEmpty-Internet-Connection".autoLocalize()
                        break
                       case .empty:
                          str = "Check our bestsellers and find something for you"
                        break
                        default:
                           str = ""
                        break
        }
     
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: ColorPalette.FontCartDescription ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let str: String!
        switch self.viewModel.state {
                              case .error:
                                str = "DZNEmpty-Btn-Retry".autoLocalize()
                               break
                              case .empty:
                                 str = "Check Best Sellers"
                               break
                               default:
                                  str = ""
                               break
               }
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let buttonFrame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 50)
        let buttonContainer = UIView(frame: buttonFrame)
        buttonContainer.backgroundColor = scrollView.backgroundColor

        let imageView = UIImageView(frame: buttonFrame)
        imageView.backgroundColor = ColorPalette.primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        buttonContainer.addSubview(imageView)
        return createImage(buttonContainer)

//        let capInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
//        var rectInsets: UIEdgeInsets = .zero
//        var imageName = ""
//
//        if state == .normal {
//            imageName = "dz-empty-button-bg"
//        }
//        if state == .highlighted {
//            imageName = "dz-empty-button-bg"
//        }
//
//        rectInsets = UIEdgeInsets(top: -19.0, left: -61.0, bottom: -19.0, right: -61.0)
//        let image = UIImage(named: imageName, in: Bundle(for: type(of: self)), compatibleWith: nil)
//
//        return image?.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
         
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        switch self.viewModel.state {
                       case .error:
                        viewModel.initFetch(refreshType: .refresh)
                        break
                       case .empty:
                          self.dismiss(animated: true, completion: nil)
                        break
                        default:
                        break
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        var image: UIImage?
        switch self.viewModel.state {
                              case .error:
                                  image = UIImage(named: "connection-error")
                               break
                              case .empty:
                                 image = UIImage(named: "no-available-products")
                               break
                               default:
                                  image = UIImage(named: "no-available-products")
                               break
               }
        return image
    }
    func createImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: view.frame.width, height: view.frame.height), true, 1)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


