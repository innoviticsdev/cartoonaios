//
//  WishlistVC.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/10/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
class WishlistVC: UIViewController,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet weak var WishlistTableView: UITableView!
    @IBOutlet weak var CloseButton: UIButton!
      var indicatorIsOn : Bool = false
    lazy var viewModel: WishlistVM = {
           return WishlistVM()
       }()
    override func viewDidLoad() {
        super.viewDidLoad()

        CloseButton.onTap { _ in
            self.dismiss(animated: true, completion: nil)
        }
        
        WishlistTableView.delegate = self
        WishlistTableView.dataSource = self
        WishlistTableView.registerCellNib(cellClass: WishlistCell.self)
        
        initVM()
        // Do any additional setup after loading the view.
        
        self.WishlistTableView.es.addPullToRefresh {
                          [unowned self] in
                self.viewModel.currentPage = 1
                self.viewModel.initFetch(refreshType: .refresh)
            }
                self.WishlistTableView.es.addInfiniteScrolling {
                   [unowned self] in
                self.viewModel.initFetch(refreshType: .loadmore)
        }
    }
    
    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            self.WishlistTableView.reloadData()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                    case .noMore:
                    self.indicatorIsOn = false
                    indicatorSwitch(status: .off,view: self.view)
                    self.WishlistTableView.es.noticeNoMoreData()
                    self.WishlistTableView.es.stopLoadingMore()
                case .loading:
                    self.indicatorIsOn = true
                    indicatorSwitch(status: .on,view: self.view)
                case .populated:
                    self.indicatorIsOn = false
                    indicatorSwitch(status: .off,view: self.view)
                    self.WishlistTableView.es.stopLoadingMore()
                    self.WishlistTableView.es.stopPullToRefresh()
                    indicatorSwitch(status: .off,view: self.view)
                }
            }
        }
        
        viewModel.moveCartLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            self.WishlistTableView.reloadData()
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.Movestate {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                
                case .loading:
                    self.indicatorIsOn = true
                    indicatorSwitch(status: .on,view: self.view)
                case .populated:
                    self.indicatorIsOn = false
                    indicatorSwitch(status: .off,view: self.view)
                    self.viewModel.initFetch(refreshType: .refresh)
                    indicatorSwitch(status: .off,view: self.view)
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.WishlistTableView.reloadData()
            }
        }
        
        viewModel.initFetch(refreshType: .refresh)

    }


   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.viewModel.arrWishlist.count
         }

          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistCell", for: indexPath) as! WishlistCell
            
            let WishlistObject = self.viewModel.arrWishlist[indexPath.row]
            
            cell.ProductImage.kf.setImage(with: URL(string: (WishlistObject.product.baseImage.originalImageURL)))
            
            cell.ProductName.text = WishlistObject.product.name
            cell.ProductPrice.text = WishlistObject.product.finalPrice
            cell.OriginalPrice.text = WishlistObject.product.finalPrice
            
            cell.moveToCartButton.onTap {_ in
                self.viewModel.moveParam = ["id":WishlistObject.id.description]
                self.viewModel.moveToCartAPI()
            }
            
            cell.RemoveButton.onTap {_ in
                self.viewModel.removeParam = ["id":WishlistObject.product.id.description]
                self.viewModel.addRemoveToWishlistAPI()
            }
            
            
            let arrTags = WishlistObject.product.tags
             let countArrTags = arrTags.count
             
             
             switch countArrTags {
             case 1:
                  let color = UIColor(hexString: arrTags[0].color)
                 
                
                  cell.viewTag1.backgroundColor =  color.withAlphaComponent(0.2)

                  cell.lblTag1.text = arrTags[0].text
                  cell.lblTag1.textColor = color
                 
               
                 
             case 2:
                 let color = UIColor(hexString: arrTags[0].color)
                
                 
                 cell.viewTag2.backgroundColor =  color.withAlphaComponent(0.2)
                 
                 cell.lblTag2.text = arrTags[1].text
                 cell.lblTag2.textColor = color
                 break
                 
             default:
                cell.viewTag1.alpha = 0
                cell.viewTag2.alpha = 0
                 break
             }
              
            
           
             return cell
         }
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 200
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                 let ProductsDetailsVC = ProductDetailsViewController()
            ProductsDetailsVC.productId = viewModel.arrWishlist[indexPath.row].product.id.description
                        ProductsDetailsVC.modalPresentationStyle = .fullScreen
                        self.present(ProductsDetailsVC, animated: true, completion: nil)
         }
      func showAlert( _ message: String ) {
             let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
             alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
             self.present(alert, animated: true, completion: nil)
         }

}
