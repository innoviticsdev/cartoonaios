//
//  WishlistCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/10/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class WishlistCell: UITableViewCell {
    
    @IBOutlet weak var viewTag1: UIView!
       @IBOutlet weak var lblTag1: UILabel!
       
       @IBOutlet weak var viewTag2: UIView!
       @IBOutlet weak var lblTag2: UILabel!
       
    @IBOutlet weak var RemoveButton: TRButton!
    
    
    @IBOutlet weak var moveToCartButton: TRButton!
    
    @IBOutlet weak var OriginalPrice: UILabel!
    @IBOutlet weak var ProductPrice: TRLabelBold!
    @IBOutlet weak var ProductName: TRLabel!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var MainView: UIView!{didSet{
        MainView.layer.cornerRadius = 10
        }}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
