//
//  ProductColorCollectionViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/13/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class ProductColorCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ColorView: UIView!{didSet{
        ColorView.layer.cornerRadius = ColorView.frame.height / 2
        }}
    
    @IBOutlet weak var CorrectIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
