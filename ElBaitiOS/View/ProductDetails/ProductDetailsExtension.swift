//
//  ProductDetailsExtension.swift
//  ElBaitiOS
//
//  Created by mac on 9/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

//To handle product details empty states
extension ProductDetailsViewController: DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        if(scrollView == ProductDetailsScrollView)
        {
            switch self.viewModel.state {
                           case .empty, .error:
                               return true
                           case .loading:
                                return false
                           case .populated:
                              return false
                }
        }
        else{
            switch self.viewModel.similarItemsState {
                                      case .empty, .error:
                                          return true
                                      case .loading:
                                           return false
                                      case .populated:
                                         return false
                           }
        }
        }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        if(scrollView == ProductDetailsScrollView)
        {
            switch self.viewModel.state {
                           case .error:
                            str = "DZNEmpty-Something-Wrong".autoLocalize()
                            break
                           case .empty:
                              str = "This product is no longer available"
                            break
                            default:
                               str = ""
                            break
            }
        }
        else
        {
                switch self.viewModel.similarItemsState {
                case .error:
                   str = "DZNEmpty-Something-Wrong".autoLocalize()
                 break
                case .empty:
                   str = "Could't find similar items"
                 break
                 default:
                    str = ""
                 break
            }
        }
          if(Localize.getLocal() == "en"){
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                 return NSAttributedString(string: str, attributes: myAttribute)
             }
             else{
                  let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                 return NSAttributedString(string: str, attributes: myAttribute)
             }
             
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        if(scrollView == ProductDetailsScrollView)
        {
            switch self.viewModel.state {
                           case .error:
                                 str = "DZNEmpty-Internet-Connection".autoLocalize()
                            break
                           case .empty:
                             str = "DZNEmpty-Check-Bestsellers".autoLocalize()
                            break
                            default:
                               str = ""
                            break
            }
        }
        else
        {
            switch self.viewModel.similarItemsState {
                           case .error:
                               str = "DZNEmpty-Internet-Connection".autoLocalize()
                            break
                           case .empty:
                              str = "We always keep adding new items, Stay tuned."
                            break
                            default:
                               str = ""
                            break
            }
        }
     
        if(Localize.getLocal() == "en"){
               let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
               else{
                    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let str: String!
        if(scrollView == ProductDetailsScrollView)
        {
            switch self.viewModel.state {
                                  case .error:
                                     str = "DZNEmpty-Btn-Retry".autoLocalize()
                                   break
                                  case .empty:
                                    str = "DZNEmpty-Btn-Check-Bestsellers".autoLocalize()
                                   break
                                   default:
                                      str = ""
                                   break
                   }
        }
        else
        {
            switch self.viewModel.similarItemsState {
                           case .error:
                          str = "DZNEmpty-Btn-Retry".autoLocalize()
                            break
                           case .empty:
                              str = "DZNEmpty-Btn-Check-Bestsellers".autoLocalize()
                            break
                            default:
                               str = ""
                            break
            }
        }
        
         
        if(Localize.getLocal() == "en"){
         let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
             return NSAttributedString(string: str, attributes: myAttribute)
         }
         else{
              let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
             return NSAttributedString(string: str, attributes: myAttribute)
         }
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let buttonFrame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 50)
        let buttonContainer = UIView(frame: buttonFrame)
        buttonContainer.backgroundColor = scrollView.backgroundColor

        let imageView = UIImageView(frame: buttonFrame)
        imageView.backgroundColor = ColorPalette.primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        buttonContainer.addSubview(imageView)
        return createImage(buttonContainer)
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        if(scrollView == ProductDetailsScrollView)
        {
            switch self.viewModel.state {
                           case .error:
                              viewModel.initFetch()
                            break
                           case .empty:
                              self.dismiss(animated: true, completion: nil)
                            break
                            default:
                            break
            }
        }
        else
        {
            switch self.viewModel.similarItemsState {
                           case .error:
                            viewModel.fetchRelatedProducts()
                            break
                           case .empty:
                              self.dismiss(animated: true, completion: nil)
                            break
                            default:
                            break
            }
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        var image: UIImage?
        if(scrollView == ProductDetailsScrollView)
        {
            switch self.viewModel.state {
                                  case .error:
                                      image = UIImage(named: "connection-error")
                                   break
                                  case .empty:
                                     image = UIImage(named: "no-available-products")
                                   break
                                   default:
                                      image = UIImage(named: "no-available-products")
                                   break
                   }
        }
        else
        {
            switch self.viewModel.similarItemsState {
                           case .error:
                               image = UIImage(named: "connection-error")
                            break
                           case .empty:
                              image = UIImage(named: "no-available-products")
                            break
                            default:
                               image = UIImage(named: "no-available-products")
                            break
            }
        }
        return image
    }
    
    
    func createImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: view.frame.width, height: view.frame.height), true, 1)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func transformCollectionView(collectionView: UICollectionView){
        
    }

}
