//
//  ProductDetailsWebView.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import WebKit
import GestureRecognizerClosures

class ProductDetailsWebView: UIViewController {
    
    var ProductDescription = ""

    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var DescriptionWebView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        DescriptionWebView.loadHTMLString(ProductDescription, baseURL: nil)
        BackButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
