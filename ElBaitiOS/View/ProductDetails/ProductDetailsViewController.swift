//
//  ProductDetailsViewController.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/13/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures
import ImageSlideshow
import Kingfisher
import Alamofire
import FirebaseDynamicLinks
import DZNEmptyDataSet
import CarbonKit

class ProductDetailsViewController: UIViewController,CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var contentViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var tabViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var LikeImage: UIImageView!
    @IBOutlet weak var LikeView: UIView!
    @IBOutlet weak var LikeButton: UIView!{didSet{
    LikeButton.layer.shadowColor = UIColor.gray.cgColor
    LikeButton.layer.shadowOpacity = 0.5
    LikeButton.layer.shadowOffset = .zero
    LikeButton.layer.shadowRadius = 3
    LikeButton.layer.cornerRadius = 10
    }}
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var ProductDetailsScrollView: UIScrollView!    
    @IBOutlet weak var SimilarItemsStackView: UIStackView!
    @IBOutlet weak var ProductDetailsLabel: UILabel!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var ProductFinalPrice: UILabel!
    @IBOutlet weak var ProductOriginalPrice: UILabel!
    @IBOutlet weak var ProductDetailsView: UIView!
    @IBOutlet weak var ProductOrderLabel: UILabel!
    @IBOutlet weak var SimilarItemsCollectionView: UICollectionView!
    @IBOutlet weak var AddToCartButton: UIButton!{didSet{
        AddToCartButton.layer.cornerRadius = 10
    }}
    @IBOutlet weak var MinusButton: UIView!{didSet{
        MinusButton.layer.cornerRadius = 5
    }}
    @IBOutlet weak var PlusButton: UIView!{didSet{
        PlusButton.layer.cornerRadius = 5
    }}
    @IBOutlet weak var BottomView: UIView!{didSet{
        BottomView.layer.shadowColor = UIColor.gray.cgColor
        BottomView.layer.shadowOpacity = 0.5
        BottomView.layer.shadowOffset = .zero
        BottomView.layer.shadowRadius = 5
        BottomView.layer.cornerRadius = 20
    }}
    @IBOutlet weak var ProductsImageSlideShow: ImageSlideshow!
    @IBOutlet weak var ShareButtonView: UIView!{didSet{
        ShareButtonView.layer.shadowColor = UIColor.gray.cgColor
        ShareButtonView.layer.shadowOpacity = 0.5
        ShareButtonView.layer.shadowOffset = .zero
        ShareButtonView.layer.shadowRadius = 3
        ShareButtonView.layer.cornerRadius = 10
    }}
    @IBOutlet weak var CloseButtonView: UIView!{didSet{
        CloseButtonView.layer.shadowColor = UIColor.gray.cgColor
        CloseButtonView.layer.shadowOpacity = 0.5
        CloseButtonView.layer.shadowOffset = .zero
        CloseButtonView.layer.shadowRadius = 3
        CloseButtonView.layer.cornerRadius = 10
    }}
    
    var productOrderValue = 1
    var productId = ""
    var contentViewInitHeight: CGFloat?
    let overviewVC = OverviewVC()
    let specificationVC = SpecificationVC()
    let reviewsTabVC = ReviewsTabVC()
    lazy var viewModel: ProductDetailsVM = {
        return ProductDetailsVM()
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        setVCStartFromTop()
        setOverviewDynamicHeight()
    }
    
    func setVCStartFromTop(){
        if(ProductDetailsScrollView.contentOffset.y > 0)
        {
            ProductDetailsScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
        
        ShareButtonView.onTap{ _ in
            self.createShareLink()
        }
    }

    func reloadCollectionView(){
        self.SimilarItemsCollectionView.reloadData()
        setupCVStartCellDirection(myCollectionView: SimilarItemsCollectionView)
    }
    
    
    var isLiked = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initVM()
        setupDelegates()
        setupButtons()
        setupTabBar()
        
        contentViewInitHeight = contentViewHeightConstraints.constant
        Localize.allowTransformation(status: true)

        
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = ColorPalette.secondaryColor
        pageIndicator.pageIndicatorTintColor = ColorPalette.ViewLineGray
        ProductsImageSlideShow.pageIndicator = pageIndicator
        ProductsImageSlideShow.onTap({ _ in
            self.ProductsImageSlideShow.presentFullScreenController(from: self)
        })
        
        overviewVC.myText = "Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla bla Bla bla "
        
        }
    
    func setupButtons(){
        
        ProductDetailsView.onTap{ _ in
            let ProductDescriptionVC = ProductDetailsWebView()
            ProductDescriptionVC.modalPresentationStyle = .fullScreen
            ProductDescriptionVC.ProductDescription = self.viewModel.ProductDetailsObject.dataDescription!
            self.present(ProductDescriptionVC, animated: true, completion: nil)
        }
        
        CloseButtonView.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
        
        PlusButton.onTap{ _ in
            self.ProductOrderLabel.text = self.viewModel.changeProductQty(operators: .increment).description
        }
        
        MinusButton.onTap{ _ in
            self.ProductOrderLabel.text = self.viewModel.changeProductQty(operators: .decrement).description
        }

        AddToCartButton.onTap { _ in
            if(checkIfSignedIn()){
            self.viewModel.AddToCartAPI()
            }
            else{
                let vc = SignInVC()
                presentBottomPopup(view: self, popupVC: vc)
            }
        }
        
        LikeView.onTap{ _ in
        
            if(self.isLiked){
                self.LikeImage.image = UIImage(named:"LikeIcon")
                self.isLiked = false
                AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.ProductDetailsObject.id.description)
            
        }
                
        else{

                self.LikeImage.image = UIImage(named:"OrangeLikeIcon")
                self.isLiked = true
                AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.ProductDetailsObject.id.description)
        }
                  }
        
        
  
        // Do any additional setup after loading the view.
    }
        
        func setupTabBar(){
            let items = ["Overview", "Specifications", "  Reviews  "]
            let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
            //carbonTabSwipeNavigation.insert(intoRootViewController: self)
            carbonTabSwipeNavigation.setNormalColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            carbonTabSwipeNavigation.setSelectedColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            carbonTabSwipeNavigation.setIndicatorColor(#colorLiteral(red: 0.9568627451, green: 0.6274509804, blue: 0.231372549, alpha: 1))
            carbonTabSwipeNavigation.setTabExtraWidth(30)
            carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: tabView)
        }
        
        
        func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
             switch index {
                   //Did ViewController 1
                   case 0:
                    
                       UIView.animate(withDuration: 0.7, animations: {
                        self.setOverviewDynamicHeight()
                        })
                
                   //Did ViewController 2
                   case 1:
                       
                       UIView.animate(withDuration: 0.7, animations: {
                        self.setupTabViewHeight(height:  self.specificationVC.cellCount * 35)
                       })

                   //Did ViewController 3
                   case 2:
                       
                       UIView.animate(withDuration: 0.7, animations: {
                        if(self.reviewsTabVC.cellCount != 0){
                            self.setupTabViewHeight(height: self.reviewsTabVC.cellCount * 120 + 250)
                        }else{
                            self.setupTabViewHeight(height: 50)
                        }
                       })
                       

                   default:
                      print("OUT OFF INDEX")
                   }
        }

        func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {

            switch index {
            case 0:
                return overviewVC

            case 1:
                return specificationVC

            case 2:
                return reviewsTabVC

            default:
                return overviewVC
            }

        }
    
    func setupDelegates(){
    ProductDetailsScrollView.emptyDataSetDelegate = self
    ProductDetailsScrollView.emptyDataSetSource = self
    SimilarItemsCollectionView.emptyDataSetDelegate = self
    SimilarItemsCollectionView.emptyDataSetSource = self
    SimilarItemsCollectionView.delegate = self
    SimilarItemsCollectionView.dataSource = self
    SimilarItemsCollectionView.registerCellNib(cellClass: BestSellingCell.self)
    }

    func initVM() {
        viewModel.productId = self.productId
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
            
                self.ProductDetailsScrollView.reloadEmptyDataSet()

                switch self.viewModel.state {
                case .empty, .error:
                     indicatorSwitch(status: .off,view: self.view)
                     self.contentView.alpha = 0.0
                     self.BottomView.alpha = 0.0
                    
                case .loading:
                    indicatorSwitch(status: .on,view: self.view)
                    self.contentView.alpha = 0.0
                    self.BottomView.alpha = 0.0

                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    self.contentView.alpha = 1.0
                    self.BottomView.alpha = 1.0
                    self.setupTabs()
                    self.reviewsTabVC.objReviews = self.viewModel.ProductDetailsObject?.reviews
                    self.reviewsTabVC.id = self.viewModel.ProductDetailsObject?.id
                    UIView.animate(withDuration: 0.2, animations: {
                        self.FillProduct()
                       
                    })
                }
            }
        }
        
        viewModel.updateAddCartStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.addToCartState {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)

                case .loading:
                    indicatorSwitch(status: .on,view: self.view)

                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    print("Added item successfully")
                    showToast(message: "Item added successfully", view: self.view)
                }
            }
        }
        
        viewModel.updateSimilarItemsStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.reloadCollectionView()
                switch self.viewModel.similarItemsState {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.SimilarItemsCollectionView)
                    self.activeButtonToggle(isActive: false)
                    
                case .loading:
                    indicatorSwitch(status: .on,view: self.SimilarItemsCollectionView)
                    self.activeButtonToggle(isActive: false)

                case .populated:
                    indicatorSwitch(status: .off,view: self.SimilarItemsCollectionView)
                    self.activeButtonToggle(isActive: true)
                     if(self.viewModel.relatedProductsArray.count == 0){
                         self.SimilarItemsStackView.alpha = 0
                     }
                     else{
                        self.transformCollectionView(collectionView: self.SimilarItemsCollectionView)
                        self.SimilarItemsStackView.alpha = 1
                     }
                }
            }
        }
        viewModel.initFetch()
    }
    
    func activeButtonToggle(isActive: Bool){
        if(isActive){
            AddToCartButton.backgroundColor = ColorPalette.ButtonColor
            AddToCartButton.isEnabled = true
        }else{
            AddToCartButton.backgroundColor = #colorLiteral(red: 0.5254901961, green: 0.5254901961, blue: 0.5254901961, alpha: 1)
            AddToCartButton.isEnabled = false
        }
    }
    
    func FillProduct(){
        self.ProductName.text = self.viewModel.ProductDetailsObject.name
        self.ProductFinalPrice.text = self.viewModel.ProductDetailsObject.finalPrice
        self.ProductOriginalPrice.text = self.viewModel.ProductDetailsObject.originalPrice
        self.ProductDetailsLabel.attributedText = self.viewModel.ProductDetailsObject.shortDescription?.htmlToAttributedString
    
        var imageSource: [InputSource] = []
        for image in self.viewModel.ProductDetailsObject.images {
            let img = image
            imageSource.append( KingfisherSource(urlString: img.url)!)
            
        }
        self.ProductsImageSlideShow.setImageInputs(imageSource)
        
        if(self.viewModel.ProductDetailsObject.isSaved){
            LikeImage.image = UIImage(named:"OrangeLikeIcon")
            isLiked = true
        }
        else{
            LikeImage.image = UIImage(named:"LikeIcon")
            isLiked = false
        }
   
    }
    
    func setupTabs(){
        specificationVC.objReviews = viewModel.ProductDetailsObject.reviews
    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setOverviewDynamicHeight() {
        setupTabViewHeight(height: 90)
        let font = UIFont(name: "Poppins", size: 17.0)!
        let screenWidth = UIScreen.main.bounds.width - 36

        let height = heightForView(text: self.overviewVC.myText, font: font, width: screenWidth)

        setupTabViewHeight(height: Int(height))
    }

}


extension ProductDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                return viewModel.relatedProductsArray.count
    }
    
    func createShareLink(){
           var components = URLComponents()
           components.scheme = "https"
           components.host = "elbait.page.link"
           components.path = "/launch"
        let SessionIDQueryItem = URLQueryItem(name: "id",value: self.viewModel.ProductDetailsObject!.id.description)
           components.queryItems = [SessionIDQueryItem]
           
           guard let linkParameter = components.url else{ return }
           print("I'm sharing this link \(linkParameter.absoluteString)")
           
        let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://elbait.page.link")
           
           if let myBundleId = Bundle.main.bundleIdentifier{
            shareLink?.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleId)
           }
           
        shareLink?.iOSParameters?.appStoreID = "0"
           
        shareLink?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.innovitics.ElBait")
        shareLink?.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        shareLink?.socialMetaTagParameters?.title = self.viewModel.ProductDetailsObject.name
        shareLink?.socialMetaTagParameters?.descriptionText = self.viewModel.ProductDetailsObject.shortDescription?.htmlToString
        shareLink?.socialMetaTagParameters?.imageURL = NSURL(string: self.viewModel.ProductDetailsObject.baseImage.originalImageURL) as URL?
    
        guard let longURL = shareLink?.url else { return }
           
           print("I'm sharing this Long link \(longURL.absoluteString)")
           
        shareLink?.shorten { (url, warnings, error) in
               if let error = error{
                   print("The error is \(error)")
                   return
               }
               guard let url = url else { return }
               print("I'm sharing this short link \(url.absoluteString)")
               self.showShareSheet(url: url)
           }
       }
       
       func showShareSheet(url: URL)
       {
           let promoText = ""
           let activityVC = UIActivityViewController(activityItems: [promoText,url], applicationActivities: nil)
           present(activityVC,animated: true)
       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestSellingCell", for: indexPath) as! BestSellingCell
            
            let ProductObject = viewModel.relatedProductsArray[indexPath.row]
        if(ProductObject.inStock)
            {
                cell.ViewOutOfStock.alpha = 0
            }
            else{
                cell.ViewOutOfStock.alpha = 1
            }
            cell.ProductImage.kf.setImage(with: URL(string: ProductObject.baseImage.mediumImageURL))
            cell.ProductName.text = ProductObject.name
            cell.ProductPrice.text = ProductObject.finalPrice
            cell.ProductRating.rating = Double(ProductObject.reviews.averageRating)
            cell.ProcudtPreviousPrice.text = ProductObject.originalPrice
        
        cell.LikeView.onTap{ _ in
        if(cell.isLiked){
            cell.LikeImage.image = UIImage(named:"LikeIcon")
            cell.isLiked = false
            AddRemoveToWishlist.CallAPI(ProductID: ProductObject.id.description)
            
        }
        else{

            cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
            cell.isLiked = true
            AddRemoveToWishlist.CallAPI(ProductID: ProductObject.id.description)
        }
                  }

        
            let obj = ProductObject
            if(obj.inStock){
                cell.ViewOutOfStock.isHidden = true
            }else{
                cell.ViewOutOfStock.isHidden = false
            }
            
            cell.ProductImage.kf.setImage(with: URL(string: obj.baseImage.originalImageURL),placeholder: UIImage(named: "product-placeholder"))
            cell.ProductName.text = obj.name
            cell.ProductRating.rating = Double(obj.reviews.averageRating)
            
            let oldPrice = obj.originalPrice
            let newPrice = obj.finalPrice
            
            if(oldPrice == newPrice){
                cell.ProcudtPreviousPrice.isHidden = true
            }
            
            cell.ProcudtPreviousPrice.text = oldPrice
            cell.ProductPrice.text = newPrice
            
            
            
            let arrTags = obj.tags
            let countArrTags = arrTags.count
            cell.viewTagHolder1.isHidden = true
            cell.viewTagHolder2.isHidden = true
            
            switch countArrTags {
            case 1:
                 let color = UIColor(hexString: arrTags[0].color)
                 cell.viewTagHolder1.isHidden = false
               
                 cell.viewTag1.backgroundColor =  color.withAlphaComponent(0.2)

                 cell.lblTag1.text = arrTags[0].text
                 cell.lblTag1.textColor = color
                
              
                
            case 2:
                let color = UIColor(hexString: arrTags[0].color)
                cell.viewTagHolder2.isHidden = false
                
                cell.viewTag2.backgroundColor =  color.withAlphaComponent(0.2)
                
                cell.lblTag2.text = arrTags[1].text
                cell.lblTag2.textColor = color
                break
                
            default:
                break
            }
        
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ProductsDetailsVC = ProductDetailsViewController()
        ProductsDetailsVC.productId = viewModel.relatedProductsArray[indexPath.row].id.description
        ProductsDetailsVC.modalPresentationStyle = .fullScreen
        self.present(ProductsDetailsVC, animated: true, completion: nil)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        return CGSize(width: width/2.4, height: height/2.8)
        }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


// MARK: - Dynamic Height

extension ProductDetailsViewController{
    
    func setupTabViewHeight(height: Int){
        tabViewHeightConstraints.constant = CGFloat(height + 35)
        resetContentViewHeight()
        increaseContentViewHeight(height: height)
    }
    
    func resetContentViewHeight(){
        contentViewHeightConstraints.constant = contentViewInitHeight ?? 0.0
    }
    
    func increaseContentViewHeight(height: Int){
        contentViewHeightConstraints.constant += CGFloat(height)
    }
    
    func decreaseContentViewHeight(height: Int){
        contentViewHeightConstraints.constant -= CGFloat(height)
    }
    
}
