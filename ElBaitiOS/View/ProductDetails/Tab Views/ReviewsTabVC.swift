//
//  BlueViewController.swift
//  i am reach
//
//  Created by mac on 9/13/20.
//  Copyright © 2020 hussien. All rights reserved.
//

import UIKit

class ReviewsTabVC: UIViewController {
    
    var cellCount = 0
    var objReviews: Reviews?
    var id: Int?
    var count: Int?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var reviewsTV: UITableView!
    @IBOutlet weak var ratingTV: UITableView!
    @IBOutlet weak var btnLoadMore: UIButton!{didSet{
        btnLoadMore.onTap{_ in
           let vc = ReviewsVC()
            vc.productId = self.id
           self.present(vc, animated: true, completion: nil)
        }
        }}
    
    override func viewDidLoad() {
        super.viewDidLoad()
            setupTableView()
            checkEmpty()
        }
    
    func setupTableView(){
        reviewsTV.rowHeight = UITableView.automaticDimension
            reviewsTV.estimatedRowHeight = 600
        
            ratingTV.dataSource = self
            ratingTV.delegate = self
            
            reviewsTV.dataSource = self
            reviewsTV.delegate = self
            
            ratingTV.registerCellNib(cellClass: RatingCell.self)
            reviewsTV.registerCellNib(cellClass: ReviewCell.self)
    }
    
    func checkEmpty(){
        lblReviews.alpha = 0
        count = objReviews?.topReviews?.count
        
        if(count == 0){
            contentView.alpha = 0
            lblReviews.alpha = 1
        }
    }
    
}

extension ReviewsTabVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case ratingTV:
            return 5
        
        case reviewsTV:
            cellCount = count ?? 0
            return cellCount
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case ratingTV:
            let cell = ratingTV.dequeue() as RatingCell
            cell.configureCell(index: indexPath.row, percentage: objReviews?.percentage)
            return cell
        
        case reviewsTV:
            let cell = reviewsTV.dequeue() as ReviewCell
            cell.configureCell(objReview: (objReviews?.topReviews?[indexPath.row])!)
            return cell
            
        default:
            let cell = ratingTV.dequeue() as RatingCell
            return cell
        }
        
    }
    
}
