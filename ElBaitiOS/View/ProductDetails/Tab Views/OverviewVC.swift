//
//  RedViewController.swift
//  i am reach
//
//  Created by mac on 9/13/20.
//  Copyright © 2020 hussien. All rights reserved.
//

import UIKit

class OverviewVC: UIViewController {
    
    @IBOutlet weak var lblText: UILabel!

    var myText: String = "" {
        didSet {
            lblText.text = myText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
