//
//  GreenViewController.swift
//  i am reach
//
//  Created by mac on 9/13/20.
//  Copyright © 2020 hussien. All rights reserved.
//

import UIKit

class SpecificationVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var cellCount = 0
    var objReviews: Reviews?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerCellNib(cellClass: SpecificationCell.self)
    }
    
}

extension SpecificationVC: UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = 8
        cellCount = count

        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue() as SpecificationCell
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor =  #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
        } else{
            cell.backgroundColor =  #colorLiteral(red: 0.8705882353, green: 0.8705882353, blue: 0.8705882353, alpha: 1)
        }
        
        cell.configureCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
}
