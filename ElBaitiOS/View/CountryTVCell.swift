//
//  CountryTVCell.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/9/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class CountryTVCell: UITableViewCell {

    @IBOutlet weak var countryView: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
