//
//  CategoriesAttached.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/12/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import ESPullToRefresh

class CategoriesAttached: UIViewController, applyFilterDelegate, UITableViewDelegate, UITableViewDataSource, filterSelectionDelegate {
    
    
   
    var selectedIndex: IndexPath!
    @IBOutlet weak var FilterTableView: UITableView!
    @IBOutlet weak var AlphaView: UIView!
    @IBOutlet weak var ApplyButton: UIButton!{didSet{
        ApplyButton.layer.cornerRadius = 10
        }}
    @IBOutlet weak var ResetButton: UIButton!{didSet{
        ResetButton.layer.cornerRadius = 10
        ResetButton.layer.borderWidth = 2
        ResetButton.layer.borderColor = ColorPalette.ButtonColor.cgColor
        }}
    
    @IBOutlet weak var FilterView: UIView!
    var CategoryAttributes = [CategoryAttribute]()
    @IBOutlet weak var FilterCollectionView: UICollectionView!
    @IBOutlet weak var CategoriesAttachedCollectionView: UICollectionView!
    var categoryID :String!
    var categoryName: String!
    
    lazy var viewModel: CategoryAttachedVM = {
           return CategoryAttachedVM()
       }()
    
    lazy var filterViewModel: FiltersVM = {
              return FiltersVM()
          }()
    
    init(categoryID: String?,categoryName: String?) {
        self.categoryID = categoryID
        self.categoryName = categoryName
           super.init(nibName: nil, bundle: nil)
       }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var CloseView: CardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Localize.allowTransformation(status: false)
        CategoriesAttachedCollectionView.delegate = self
        CategoriesAttachedCollectionView.dataSource = self
        CategoriesAttachedCollectionView.register(UINib(nibName: "BestSellingCell", bundle: nil), forCellWithReuseIdentifier: "BestSellingCell")
        
        CategoriesAttachedCollectionView.emptyDataSetSource = self
        CategoriesAttachedCollectionView.emptyDataSetDelegate = self
        
        
        
        FilterCollectionView.delegate = self
        FilterCollectionView.dataSource = self
        FilterCollectionView.register(UINib(nibName: "TagCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TagCollectionViewCell")
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.viewModel.categoryId = self.categoryID
        self.viewModel.currentPage = 1
        
        self.CategoriesAttachedCollectionView.es.addPullToRefresh {
                   [weak self] in
            self?.viewModel.currentPage = 1
            self?.viewModel.initFetch(refreshType: .refresh, isFilter: false)
               }
        self.CategoriesAttachedCollectionView.es.addInfiniteScrolling {
            [weak self] in
            self?.viewModel.initFetch(refreshType: .loadmore, isFilter: false)
        }
        
        initVM()
        
        CloseView.onTap { _ in
            
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.navigationBar.isHidden = false
        }
        
        AlphaView.onTap { _ in
            UIView.animate(withDuration: 0.3) {
                self.FilterView.alpha = 0
            }
        }
        
        ApplyButton.onTap { _ in
            self.filterViewModel.filterParam["category_id"] = self.categoryID
            self.viewModel.filterParam = self.filterViewModel.filterParam
            self.viewModel.initFetch(refreshType: .refresh, isFilter: true)
            self.FilterView.alpha = 0
        }
        
        ResetButton.onTap { _ in
            self.viewModel.filterParam.removeAll()
            self.FilterView.alpha = 0
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        confNavBar()
    }
    
    func confNavBar(){
            navigationItem.largeTitleDisplayMode = .never
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.tintColor = UIColor.black
           // self.navigationController?.navigationBar.topItem!.title = categoryName
            self.title = categoryName
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
           
       }
   func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.CategoriesAttachedCollectionView.reloadData()
                switch self.viewModel.state {
                case .empty, .error:
                    self.CategoriesAttachedCollectionView.es.stopPullToRefresh()
                    indicatorSwitch(status: .off,view: self.view)
                case .noMore:
                    indicatorSwitch(status: .off,view: self.view)
                    self.CategoriesAttachedCollectionView.es.noticeNoMoreData()
                    self.CategoriesAttachedCollectionView.es.stopLoadingMore()
                case .loading:
                    indicatorSwitch(status: .on,view: self.view)
                case .populated:
                indicatorSwitch(status: .off,view: self.view)
                self.CategoriesAttachedCollectionView.es.stopLoadingMore()
                self.CategoriesAttachedCollectionView.es.stopPullToRefresh()
                }
                
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.CategoriesAttachedCollectionView.reloadData()
            }
        }
     
    

    viewModel.initFetch(refreshType: .refresh, isFilter: false)

    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension CategoriesAttached: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if(collectionView == FilterCollectionView){
        return CategoryAttributes.count + 1
    }
    else{
         return viewModel.arrCategoriesAttached.count
    }
   
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
    if(collectionView == FilterCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as! TagCollectionViewCell
        
        switch indexPath.row {
        case 0:
            cell.SizeLabel.text = "Filter"
            cell.FilterIcon.alpha = 1
        default:
            cell.SizeLabel.text = CategoryAttributes[indexPath.row - 1].name
            cell.FilterIcon.alpha = 0
            break
        }
        
        
                return cell
        
    }
                
 
    else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestSellingCell", for: indexPath) as! BestSellingCell
    
    if(viewModel.arrCategoriesAttached[indexPath.row].isSaved){
        cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
        cell.isLiked = true
    }
    else{
        cell.LikeImage.image = UIImage(named:"LikeIcon")
        cell.isLiked = false
    }
    
    let ProductObject = viewModel.arrCategoriesAttached[indexPath.row]
    if(ProductObject.inStock)
    {
        cell.ViewOutOfStock.alpha = 0
    }
    else{
        cell.ViewOutOfStock.alpha = 1
    }
    cell.ProductImage.kf.setImage(with: URL(string: ProductObject.baseImage.originalImageURL))
    cell.ProductName.text = ProductObject.name
    cell.ProductPrice.text = ProductObject.finalPrice
    cell.ProductRating.rating = Double(ProductObject.reviews.averageRating)
    cell.ProcudtPreviousPrice.text = ProductObject.originalPrice
    
    let arrTags = ProductObject.tags
    let countArrTags = arrTags.count
    cell.viewTagHolder1.isHidden = true
    cell.viewTagHolder2.isHidden = true
    
    switch countArrTags {
    case 1:
         let color = UIColor(hexString: arrTags[0].color)
         cell.viewTagHolder1.isHidden = false
       
         cell.viewTag1.backgroundColor =  color.withAlphaComponent(0.2)

         cell.lblTag1.text = arrTags[0].text
         cell.lblTag1.textColor = color
        
      
        
    case 2:
        let color = UIColor(hexString: arrTags[0].color)
        cell.viewTagHolder2.isHidden = false
        
        cell.viewTag2.backgroundColor =  color.withAlphaComponent(0.2)
        
        cell.lblTag2.text = arrTags[1].text
        cell.lblTag2.textColor = color
        break
        
    default:
        break
    }
    
    cell.LikeView.onTap{ _ in
    if(cell.isLiked){
        cell.LikeImage.image = UIImage(named:"LikeIcon")
        cell.isLiked = false
        AddRemoveToWishlist.CallAPI(ProductID: ProductObject.id.description)
    }
    else{

        cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
        cell.isLiked = true
        AddRemoveToWishlist.CallAPI(ProductID: ProductObject.id.description)
                  
    }
              }
    
    
            return cell
    }

    
      
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {

    

    let width = UIScreen.main.bounds.width

    

    if(collectionView == CategoriesAttachedCollectionView)

    {

        let myWidth = width/2.15

        return CGSize(width: myWidth, height: myWidth*1.58)

    }

    else{

        return CGSize(width: 120, height: 50)

    }

}
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == CategoriesAttachedCollectionView)
        {
        let ProductsDetailsVC = ProductDetailsViewController()
        ProductsDetailsVC.productId = viewModel.arrCategoriesAttached[indexPath.row].id.description
        ProductsDetailsVC.modalPresentationStyle = .fullScreen
        self.present(ProductsDetailsVC, animated: true, completion: nil)
        }
        else{
            switch indexPath.row {
            case 0:
                  let FiltersViewController = FiltersVC()
                          FiltersViewController.applyDelegate = self
                          FiltersViewController.CategoryAttributes = CategoryAttributes
                          FiltersViewController.CategoryID = categoryID
                          self.present(FiltersViewController, animated: true, completion: nil)
            default:
               
                selectedIndex = indexPath
                UIView.animate(withDuration: 0.3) {
                    self.FilterTableView.delegate = self
                    self.FilterTableView.dataSource = self
                    self.FilterTableView.registerCellNib(cellClass: TagsTableViewCell.self)
                    self.FilterTableView.registerCellNib(cellClass: RateTableViewCell.self)
                    self.FilterTableView.registerCellNib(cellClass: PriceRangeTableViewCell.self)
                    self.FilterTableView.reloadData()
                    self.FilterView.alpha = 1
                }
                
            }
          
        }
    }
}

extension CategoriesAttached: DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        switch self.viewModel.state {
                       case .empty, .error:
                           return true
                       case .loading:
                            return false
                       case .noMore:
                            return false
                       case .populated:
                          return false
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                           str = "DZNEmpty-Something-Wrong".autoLocalize()
                        break
                       case .empty:
                        str = "DZNEmpty-NoAvailableProducts".autoLocalize()
                        break
                        default:
                           str = ""
                        break
        }
        if(Localize.getLocal() == "en"){
               let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
               else{
                    let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
                   return NSAttributedString(string: str, attributes: myAttribute)
               }
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                           str = "DZNEmpty-Internet-Connection".autoLocalize()
                        break
                       case .empty:
                  str = "DZNEmpty-Check-Bestsellers".autoLocalize()
                        break
                        default:
                           str = ""
                        break
        }
     
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let str: String!
        switch self.viewModel.state {
                              case .error:
                                   str = "DZNEmpty-Btn-Retry".autoLocalize()
                               break
                              case .empty:
                                  str = "DZNEmpty-Btn-Check-Bestsellers".autoLocalize()
                               break
                               default:
                                  str = ""
                               break
               }
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let buttonFrame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 50)
        let buttonContainer = UIView(frame: buttonFrame)
        buttonContainer.backgroundColor = CategoriesAttachedCollectionView.backgroundColor

        let imageView = UIImageView(frame: buttonFrame)
        imageView.backgroundColor = ColorPalette.primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        buttonContainer.addSubview(imageView)
        return createImage(buttonContainer)

//        let capInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
//        var rectInsets: UIEdgeInsets = .zero
//        var imageName = ""
//
//        if state == .normal {
//            imageName = "dz-empty-button-bg"
//        }
//        if state == .highlighted {
//            imageName = "dz-empty-button-bg"
//        }
//
//        rectInsets = UIEdgeInsets(top: -19.0, left: -61.0, bottom: -19.0, right: -61.0)
//        let image = UIImage(named: imageName, in: Bundle(for: type(of: self)), compatibleWith: nil)
//
//        return image?.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
         
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        switch self.viewModel.state {
                       case .error:
                        viewModel.initFetch(refreshType: .refresh, isFilter: false)
                        break
                       case .empty:
                        self.navigationController?.popViewController(animated: true)
                        break
                        default:
                        break
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        var image: UIImage?
        switch self.viewModel.state {
                              case .error:
                                  image = UIImage(named: "connection-error")
                               break
                              case .empty:
                                 image = UIImage(named: "no-available-products")
                               break
                               default:
                                  image = UIImage(named: "no-available-products")
                               break
               }
        return image
    }
    func createImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: view.frame.width, height: view.frame.height), true, 1)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func didTapapply(filterParams: [String : String]) {
        viewModel.filterParam = filterParams
        viewModel.initFetch(refreshType: .refresh, isFilter: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
            }
    
            

             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
                switch CategoryAttributes[selectedIndex.row - 1].swatchType {
                case "text":
                    let cell = tableView.dequeue() as TagsTableViewCell
                    cell.configure(object: CategoryAttributes[selectedIndex.row - 1], isTagFlag: true)
                    cell.selectionDelegate = self
                              return cell
                case "dropdown":
                    let cell = tableView.dequeue() as TagsTableViewCell
                      cell.configure(object: CategoryAttributes[selectedIndex.row - 1], isTagFlag: true)
                      cell.selectionDelegate = self
                   
                              return cell
                case "color":
                    let cell = tableView.dequeue() as TagsTableViewCell
                      cell.configure(object: CategoryAttributes[selectedIndex.row - 1], isTagFlag: false)
                      cell.selectionDelegate = self
                    
                              return cell
                case "slider":
                    let cell = tableView.dequeue() as PriceRangeTableViewCell
                     cell.configure(object: CategoryAttributes[selectedIndex.row - 1])
               
                 
                                       return cell
                default:
                    break
                }
                
                
                let cell = tableView.dequeue() as ButtonTableViewCell
                                                       
                                                return cell
             
            }
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                if(indexPath.row < CategoryAttributes.count)
                               {
                switch CategoryAttributes[selectedIndex.row - 1].swatchType {
                case "text":
                    let numberOfLines = Double(CategoryAttributes[selectedIndex.row - 1].options.count) / 3.0
                    print(numberOfLines)
                    let finalLines = ceil(Double(numberOfLines))
                    print(finalLines)
                    return CGFloat(50 * finalLines)
                case "dropdown":
                    let numberOfLines = Double(CategoryAttributes[selectedIndex.row - 1].options.count) / 3.0
                     print(numberOfLines)
                     let finalLines = ceil(Double(numberOfLines))
                     print(finalLines)
                     return CGFloat(50 * finalLines)
                case "color":
                    return 150
                case "slider":
                    return 100
                default:
                    break
                }
                }

                switch indexPath.row {
                
                case CategoryAttributes.count:
                return 100
                case CategoryAttributes.count + 1:
                return 70
                default:
                    return 0
                }
                
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  
            }
    
    func didTapFilter(filterCode: String, filterValue: String) {
        filterViewModel.addRemoveFilter(filterCode: filterCode, filterValue: filterValue)
    }
       
    
}


