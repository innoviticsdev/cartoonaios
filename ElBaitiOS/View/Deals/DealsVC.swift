//
//  DealsVC.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class DealsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         confNavBar()
    }

    func confNavBar(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.topItem!.title = "DealsVC-Title-Deals".autoLocalize()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
