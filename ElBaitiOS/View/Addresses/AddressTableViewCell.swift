//
//  AddressTableViewCell.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/8/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import SimpleCheckbox

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var RemoveButton: UIButton!
    @IBOutlet weak var LocationDescription: UILabel!
    @IBOutlet weak var LocationName: UILabel!
    @IBOutlet weak var AddressCheckBox: Checkbox!
    override func awakeFromNib() {
        super.awakeFromNib()
        
         setupCheckbox(checkBox: AddressCheckBox, isChecked: true, isEnabled: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
