//
//  AddressesVC.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/8/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures

class AddressesVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var CloseButton: UIButton!{didSet{
        CloseButton.onTap{_ in
            self.dismiss(animated: true, completion: nil)
        }
    }
    }
    
    @IBOutlet weak var AddNewAddressButton: TRButton!{didSet{
    AddNewAddressButton.layer.cornerRadius = 10
     
    }}
    @IBOutlet weak var BottomView: UIView!{didSet{
    BottomView.layer.shadowColor = UIColor.gray.cgColor
    BottomView.layer.shadowOpacity = 0.5
    BottomView.layer.shadowOffset = .zero
    BottomView.layer.shadowRadius = 5
    BottomView.layer.cornerRadius = 20
    }}
    @IBOutlet weak var AddressesTableView: UITableView!
    
    lazy var viewModel: ShippingVM = {
        return ShippingVM()
    }()
    
    func initVM() {
        
 
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)

                case .loading:
                    indicatorSwitch(status: .on,view: self.view)

                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
     
                 
                    self.AddressesTableView.reloadData()
                }
            }
        }
        
        viewModel.removeAddressStatus = { [weak self] () in
               guard let self = self else {
                   return
               }

               DispatchQueue.main.async { [weak self] in
                   guard let self = self else {
                       return
                   }
                   switch self.viewModel.state {
                   case .empty, .error:
                       indicatorSwitch(status: .off,view: self.view)

                   case .loading:
                       indicatorSwitch(status: .on,view: self.view)

                   case .populated:
                       indicatorSwitch(status: .off,view: self.view)
        
                    
                       self.viewModel.getAddresses()
                   }
               }
           }
        
        viewModel.reloadViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                //self?.reloadViews()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        initVM()
       AddressesTableView.delegate = self
       AddressesTableView.dataSource = self
       AddressesTableView.registerCellNib(cellClass: AddressTableViewCell.self)
        
        self.AddNewAddressButton.onTap { _ in
               self.present(AddNewAddressViewController(), animated: true, completion: nil)
        }
        
        viewModel.getAddresses()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arrAddress.count

      }

       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell", for: indexPath) as! AddressTableViewCell
        
        cell.LocationName.text = viewModel.arrAddress[indexPath.row].locationName
        cell.LocationDescription.text = viewModel.arrAddress[indexPath.row].address1![0]
        cell.RemoveButton.onTap { _ in
            self.viewModel.removeAddress(addressID: self.viewModel.arrAddress[indexPath.row].id.description)
        }
      

          return cell
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 160
      }
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
      }
   

}
