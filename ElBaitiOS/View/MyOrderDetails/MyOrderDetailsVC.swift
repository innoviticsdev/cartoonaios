//
//  ShippingVC.swift
//  ElBaitiOS
//
//  Created by mac on 8/27/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

import GestureRecognizerClosures
import FlexibleSteppedProgressBar



class MyOrderDetailsVC: UIViewController,FlexibleSteppedProgressBarDelegate {

   

    @IBOutlet weak var OrderDetailsProgressBar: FlexibleSteppedProgressBar!{didSet{
     OrderDetailsProgressBar.numberOfPoints = 4
     OrderDetailsProgressBar.lineHeight = 3
     OrderDetailsProgressBar.radius = 5
     OrderDetailsProgressBar.progressRadius = 8
     OrderDetailsProgressBar.progressLineHeight = 3
     OrderDetailsProgressBar.selectedBackgoundColor = UIColor(hexString: "F4A03B")
     OrderDetailsProgressBar.selectedOuterCircleStrokeColor = UIColor(hexString: "F4A03B")
     OrderDetailsProgressBar.currentSelectedCenterColor = UIColor(hexString: "F4A03B")
     OrderDetailsProgressBar.stepTextColor = UIColor.black
     OrderDetailsProgressBar.currentSelectedTextColor = UIColor.black
        if(Localize.getLocal() == "en")
        {
        OrderDetailsProgressBar.stepTextFont = UIFont(name: "Poppins-Medium", size: 13)
        }
        else
        {
           OrderDetailsProgressBar.stepTextFont = UIFont(name: "Tajawal-Medium", size: 13)
        }
    
     
     }}
    var MyOrderObject: MyOrdersData!
    @IBOutlet weak var PlacedOnLabel: UILabel!
    @IBOutlet weak var OrderNumber: UILabel!
    @IBOutlet weak var CloseButton: UIButton!{didSet{
        CloseButton.onTap{ _ in
            self.dismiss(animated: true, completion: nil)
        }
        }}
    
    @IBOutlet weak var BottomView: UIView!{didSet{
    BottomView.layer.shadowColor = UIColor.gray.cgColor
    BottomView.layer.shadowOpacity = 0.5
    BottomView.layer.shadowOffset = .zero
    BottomView.layer.shadowRadius = 5
    BottomView.layer.cornerRadius = 20
    }}
    
    @IBOutlet weak var btnCancelOrder: UIButton!{didSet{
    btnCancelOrder.layer.cornerRadius = 10
      
    }}
    
    lazy var viewModel: CancelOrderVM = {
           return CancelOrderVM()
       }()
    
    @IBOutlet weak var TotalPrice: UILabel!
    @IBOutlet weak var DIscountLabel: UILabel!
    @IBOutlet weak var SubTotalLabel: UILabel!
    @IBOutlet weak var LocationAddress: UILabel!
    @IBOutlet weak var LocationName: UILabel!
    @IBOutlet weak var stackView: UIStackView!
  
    @IBOutlet weak var viewOfStack: UIView!
    
    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    
 

    override func viewWillAppear(_ animated: Bool) {
       
    }

    override func viewDidLoad() {
        super.viewDidLoad()
         setUpProducts()
        OrderNumber.text = "MyOrdersDetailsVC-Order#".autoLocalize() + MyOrderObject.id.description
        PlacedOnLabel.text = "MyOrdersDetailsVC-Placed".autoLocalize() + " " + MyOrderObject.createdAt
        OrderDetailsProgressBar.currentIndex = Int(MyOrderObject.statusLocation)!
        TotalPrice.text = MyOrderObject.formatedGrandTotal
        SubTotalLabel.text = MyOrderObject.formatedSubTotal
        DIscountLabel.text = MyOrderObject.formatedDiscountAmount
        OrderDetailsProgressBar.delegate = self
        LocationName.text = MyOrderObject.shippingAddress.locationName
        LocationAddress.text = MyOrderObject.shippingAddress.address1[0]
        
        initVM()
        applyOrderStatus()
        
        btnCancelOrder.onTap{ _ in
            self.viewModel.cancelParam = ["order_id":self.MyOrderObject.id.description]
            self.viewModel.initFetch()
        }
        
    }
    
    func applyOrderStatus()
    {
        if(MyOrderObject.statusLabel == "Canceled"){
            OrderDetailsProgressBar.selectedBackgoundColor = ColorPalette.actionRed
            OrderDetailsProgressBar.selectedOuterCircleStrokeColor = ColorPalette.actionRed
            OrderDetailsProgressBar.currentSelectedCenterColor = ColorPalette.actionRed
        }
        else{
            OrderDetailsProgressBar.selectedBackgoundColor = ColorPalette.secondaryColor
            OrderDetailsProgressBar.selectedOuterCircleStrokeColor = ColorPalette.secondaryColor
            OrderDetailsProgressBar.currentSelectedCenterColor = ColorPalette.secondaryColor
        }
        
        if(MyOrderObject.canCancel == false)
        {
            btnCancelOrder.backgroundColor = ColorPalette.ViewLineGray
            btnCancelOrder.isEnabled = false
        }
        else
        {
            btnCancelOrder.backgroundColor = ColorPalette.actionRed
            btnCancelOrder.isEnabled = true
        }
        
    }
    
 
    func setUpProducts(){

      
       
        changeViewsHeight(cellsCount: MyOrderObject.items.count)
        
        for item in MyOrderObject.items{
            
             let cell = OrderSummaryProduct.instanceFromNib()
            cell.imgView.kf.setImage(with: URL(string: (item.product?.baseImage.originalImageURL)!))
            cell.title.text = item.product?.name
            cell.productQuantity.text = "MyOrdersDetailsVC-QTY".autoLocalize() + item.qtyOrdered
            cell.total.text = item.product?.finalPrice
            
            stackView.addArrangedSubview(cell)
        }
    
        
        
    }
    
    func changeViewsHeight(cellsCount : Int) {
       let addedHeight = CGFloat(( 65 * cellsCount ))
        viewHeightConst.constant += addedHeight
        contentViewHeight.constant += addedHeight
    }
    

    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar, textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
              if position == FlexibleSteppedProgressBarTextLocation.bottom {
                  switch index {
                      
                 case 0: return "MyOrdersVC-Ordered".autoLocalize()
                  case 1: return MyOrderObject.statusLabel == "Canceled" ? "MyOrdersVC-Cancelled".autoLocalize() : "MyOrdersVC-Processing".autoLocalize()
                  case 2: return "MyOrdersVC-Shipped".autoLocalize()
                  case 3: return "MyOrdersVC-Delivered".autoLocalize()
                  default: return "Date"
                      
                  }
              }
              return ""
          }
    
    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                   
                    self.view.hideProgress()
                   
                case .loading:
                    self.view.showProgress(isDim: true)
              
                case .populated:
                    //ToDo: Here..
                    
                    self.view.hideProgress()
                     self.dismiss(animated: true, completion: nil)
                    
                   
                }
            }
        }

      
       
      

    }
    
}


































