//
//  BestSellingCell.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/12/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Cosmos

class BestSellingCell: UICollectionViewCell {
    
    @IBOutlet weak var LikeView: UIView!{didSet{
        LikeView.layer.shadowColor = UIColor.gray.cgColor
        LikeView.layer.shadowOpacity = 0.5
        LikeView.layer.shadowOffset = .zero
        LikeView.layer.shadowRadius = 2
        LikeView.layer.cornerRadius = LikeView.frame.height / 2
        }}
    // MARK: - Outlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var ProcudtPreviousPrice: UILabel!
    @IBOutlet weak var ProductPrice: UILabel!
    @IBOutlet weak var ProductRating: CosmosView!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var ViewOutOfStock: UIView!
    
        
    @IBOutlet weak var viewTag1: UIView!
    @IBOutlet weak var lblTag1: UILabel!
    
    @IBOutlet weak var viewTag2: UIView!
    @IBOutlet weak var lblTag2: UILabel!
    
    @IBOutlet weak var viewTagHolder1: UIView!
    @IBOutlet weak var viewTagHolder2: UIView!
    
    @IBOutlet weak var LikeImage: UIImageView!
    
    var isLiked = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundedViewWithShadow(view: cellView)
        ProductRating.settings.updateOnTouch = false
        
        
       
    }
    
    
    
    func configureCell(obj: BestSeller){
     
            
      
        
       
        
        if(obj.inStock ?? false){
            ViewOutOfStock.isHidden = true
        }else{
            ViewOutOfStock.isHidden = false
        }
        
        ProductImage.kf.setImage(with: URL(string: obj.baseImage.originalImageURL),placeholder: UIImage(named: "product-placeholder"))
        ProductName.text = obj.name
        ProductRating.rating = Double(obj.reviews.averageRating)
        
        let oldPrice = obj.originalPrice
        let newPrice = obj.finalPrice
        
        if(oldPrice == newPrice){
            ProcudtPreviousPrice.isHidden = true
        }
        
        ProcudtPreviousPrice.text = oldPrice
        ProductPrice.text = newPrice
        
        
        
        let arrTags = obj.tags
        let countArrTags = arrTags.count
        viewTagHolder1.isHidden = true
        viewTagHolder2.isHidden = true
        
        switch countArrTags {
        case 1:
             let color = UIColor(hexString: arrTags[0].color)
             viewTagHolder1.isHidden = false
           
             viewTag1.backgroundColor =  color.withAlphaComponent(0.2)

             lblTag1.text = arrTags[0].text
             lblTag1.textColor = color
            
          
            
        case 2:
            let color = UIColor(hexString: arrTags[0].color)
            viewTagHolder2.isHidden = false
            
            viewTag2.backgroundColor =  color.withAlphaComponent(0.2)
            
            lblTag2.text = arrTags[1].text
            lblTag2.textColor = color
            break
            
        default:
            break
        }
    }
}
