//
//  ViewController1.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ExploreVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var ExploreScrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var offersCV: UICollectionView!
    @IBOutlet weak var categoriesCV: UICollectionView!

    @IBOutlet weak var FashionCV: UICollectionView!
    @IBOutlet weak var ElictronicsCV: UICollectionView!
    
    @IBOutlet weak var bestSellingCV: UICollectionView!

    @IBOutlet weak var discoverView: UIView!
    var indicatorIsOn : Bool = false
    
/*
     test commit
     */
    
    lazy var viewModel: ExploreVM = {
        return ExploreVM()
    }()

    //ReachabilityNetwork
    override func viewWillAppear(_ animated: Bool) {
        confNavBar()
    }
    
     func confNavBar()
     {
        self.navigationController?.navigationBar.topItem!.title = "ExploreVC-Title-Discover".autoLocalize()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        //navigationController?.navigationBar.isTranslucent = false
        
    

        if #available(iOS 13.0, *) {
                 let navBarAppearance = UINavigationBarAppearance()
                 navBarAppearance.configureWithOpaqueBackground()
                 navBarAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                 navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                 navBarAppearance.backgroundColor = .white
                 navigationController?.navigationBar.standardAppearance = navBarAppearance
                 navigationController?.navigationBar.compactAppearance = navBarAppearance
                 navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
                 navigationController?.navigationBar.tintColor = .white
             } else {
                 navigationController?.navigationBar.prefersLargeTitles = true
                 navigationController?.navigationBar.barTintColor = .white
                 navigationController?.navigationBar.tintColor = .white

             }
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
            
         })
        
        
        setupViews()
        setupDelegates()
        initVM()
        setNavigationItem()
        if(!viewModel.isInitRequest){
            viewModel.initFetch()
        }
   
    }
    
    func setupViews(){
  
        
        self.contentView.alpha = 0.0
    
    }
    
    func setupDelegates(){
        Localize.allowTransformation(status: true)
        
        ExploreScrollView.emptyDataSetDelegate = self
        ExploreScrollView.emptyDataSetSource = self
       
        offersCV.delegate = self
        offersCV.dataSource = self
        offersCV.registerCellNib(cellClass: OffersCell.self)
        transformCollectionView(collectionView: offersCV)
        
        categoriesCV.delegate = self
        categoriesCV.dataSource = self
        categoriesCV.registerCellNib(cellClass: CategoriesCell.self)
        transformCollectionView(collectionView: categoriesCV)

        bestSellingCV.delegate = self
        bestSellingCV.dataSource = self
        bestSellingCV.registerCellNib(cellClass: BestSellingCell.self)
        transformCollectionView(collectionView: bestSellingCV)

        ElictronicsCV.delegate = self
        ElictronicsCV.dataSource = self
        ElictronicsCV.registerCellNib(cellClass: BestSellingCell.self)
        transformCollectionView(collectionView: ElictronicsCV)

        FashionCV.delegate = self
        FashionCV.dataSource = self
        FashionCV.registerCellNib(cellClass: BestSellingCell.self)
        transformCollectionView(collectionView: FashionCV)

    }
    
    func transformCollectionView(collectionView: UICollectionView){
        
    }
    
    func initVM() {
            
            // Set binding init
//            viewModel.showAlertClosure = { [weak self] () in
//                DispatchQueue.main.async {
//                    if let message = self?.viewModel.alertMessage {
//                        self?.showAlert( message )
//                    }
//                }
//            }

            viewModel.updateLoadingStatus = { [weak self] () in
                guard let self = self else {
                    return
                }

                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    switch self.viewModel.state {
                    case .empty:
                            self.indicatorIsOn = false
                            indicatorSwitch(status: .off,view: self.view)
                            self.contentView.alpha = 0.0
                    case .error:
                            self.indicatorIsOn = false
                            self.ExploreScrollView.reloadEmptyDataSet()
                            indicatorSwitch(status: .off, view: self.view)
                            self.contentView.alpha = 0.0
                    case .loading:
                            self.indicatorIsOn = true
                            self.ExploreScrollView.reloadEmptyDataSet()
                            indicatorSwitch(status: .on,view: self.view)
                            self.contentView.alpha = 0.0
                    case .populated:
                            self.indicatorIsOn = false
                            indicatorSwitch(status: .off,view: self.view)
                            self.reloadViews()
                            self.contentView.alpha = 1.0
                    }
                }
            }

            viewModel.reloadViewClosure = { [weak self] () in
                DispatchQueue.main.async {
                    self?.reloadViews()
                }
            }
        }
        
        func showAlert( _ message: String ) {
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    
    
    
    func reloadViews(){
        ExploreScrollView.reloadEmptyDataSet()
        offersCV.reloadData()
        categoriesCV.reloadData()
        bestSellingCV.reloadData()
        FashionCV.reloadData()
        ElictronicsCV.reloadData()
        
        setupCVStartCellDirection(myCollectionView: offersCV)
        setupCVStartCellDirection(myCollectionView: categoriesCV)
        setupCVStartCellDirection(myCollectionView: bestSellingCV)
        setupCVStartCellDirection(myCollectionView: FashionCV)
        setupCVStartCellDirection(myCollectionView: ElictronicsCV)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



extension ExploreVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {

        case offersCV:
                return viewModel.arrDeals.count
                    
        case categoriesCV:
                return viewModel.arrCategories.count
            
        case bestSellingCV:
                return viewModel.arrBestSellers.count
            
        case ElictronicsCV:
                return viewModel.arrElectronics.count
        
        case FashionCV:
                return viewModel.arrFashion.count
            
        default:
                return 0
                    
        }
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case offersCV:
            let cell = offersCV.dequeue(indexPath: indexPath) as OffersCell
            let obj = viewModel.arrDeals[indexPath.row]
            cell.configureCell(obj: obj)
                    return cell
            
        case categoriesCV:
                let cell = categoriesCV.dequeue(indexPath: indexPath) as CategoriesCell
                let obj = viewModel.arrCategories[indexPath.row]
                cell.configureCell(obj: obj)
             
                        return cell
        
        case bestSellingCV:
        let cell = bestSellingCV.dequeue(indexPath: indexPath) as BestSellingCell
        cell.configureCell(obj: viewModel.arrBestSellers[indexPath.row])
        if(viewModel.arrBestSellers[indexPath.row].isSaved){
            cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
            cell.isLiked = true
        }
        else{
            cell.LikeImage.image = UIImage(named:"LikeIcon")
            cell.isLiked = false
        }
        cell.LikeView.onTap{ _ in
        if(cell.isLiked){
            cell.LikeImage.image = UIImage(named:"LikeIcon")
            cell.isLiked = false
            AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.arrBestSellers[indexPath.row].id.description)
            
        }
        else{
            AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.arrBestSellers[indexPath.row].id.description)
            cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
            cell.isLiked = true
                      
        }
                  }
                return cell
            
        case ElictronicsCV:
        let cell = ElictronicsCV.dequeue(indexPath: indexPath) as BestSellingCell
        cell.configureCell(obj: viewModel.arrElectronics[indexPath.row])
        if(viewModel.arrElectronics[indexPath.row].isSaved){
            cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
            cell.isLiked = true
        }
        else{
            cell.LikeImage.image = UIImage(named:"LikeIcon")
            cell.isLiked = false
        }
        cell.LikeView.onTap{ _ in
        if(cell.isLiked){
            cell.LikeImage.image = UIImage(named:"LikeIcon")
            cell.isLiked = false
            AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.arrElectronics[indexPath.row].id.description)
            
        }
        else{
            AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.arrElectronics[indexPath.row].id.description)
            cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
            cell.isLiked = true
                      
        }
                  }
                return cell
            
        // For FashionCV
        default:
                let cell = collectionView.dequeue(indexPath: indexPath) as BestSellingCell
                cell.configureCell(obj: viewModel.arrFashion[indexPath.row])
                if(viewModel.arrFashion[indexPath.row].isSaved){
                    cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
                    cell.isLiked = true
                }
                else{
                    cell.LikeImage.image = UIImage(named:"LikeIcon")
                    cell.isLiked = false
                }
                cell.LikeView.onTap{ _ in
                if(cell.isLiked){
                    cell.LikeImage.image = UIImage(named:"LikeIcon")
                    cell.isLiked = false
                    AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.arrFashion[indexPath.row].id.description)
                }
                else{
                    AddRemoveToWishlist.CallAPI(ProductID: self.viewModel.arrBestSellers[indexPath.row].id.description)
                    cell.LikeImage.image = UIImage(named:"OrangeLikeIcon")
                    cell.isLiked = true
                              
                }
                          }
                        return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {

        case offersCV:
                print("")
            
        case categoriesCV:
                showCategories(id: viewModel.arrCategories[indexPath.row].id.description,name: viewModel.arrCategories[indexPath.row].name)
            
        case bestSellingCV:
                showDetails(id: viewModel.arrBestSellers[indexPath.row].id.description)
            
        case ElictronicsCV:
                showDetails(id: viewModel.arrElectronics[indexPath.row].id.description)
        
        case FashionCV:
                showDetails(id: viewModel.arrFashion[indexPath.row].id.description)
            
        default:
                print("")
                    
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        
        switch collectionView {
        
        case offersCV:
            let myWidth = width/1.4
            return CGSize(width: myWidth, height: myWidth*1.16)
            
        case categoriesCV:
            let myWidth = width/2.4
            return CGSize(width: myWidth, height: myWidth/3)
        
        // For BestSellingCV, ElctronicsCV and FashionCV
        default:
            let myWidth = width/2.4
            return CGSize(width: myWidth, height: myWidth * 1.7)
        }
    }
    
    func showDetails(id: String){
        let ProductsDetailsVC = ProductDetailsViewController()
        ProductsDetailsVC.productId = id
        ProductsDetailsVC.modalPresentationStyle = .fullScreen
        self.present(ProductsDetailsVC, animated: true, completion: nil)
    }
    
    func showCategories(id: String,name: String){
        self.navigationController?.pushViewController(CategoriesAttached(categoryID: id,categoryName: name), animated: true)
    }
}

//See All
extension ExploreVC{
    @IBAction func btnCategories(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 1
//        let presenter = Presentr(presentationType: .bottomHalf)
//        let controller = SignOutPopUp()
//        customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
        
    }
    
    @IBAction func btnBestSelling(_ sender: Any) {
        
    }
    
    @IBAction func btnElectronics(_ sender: Any) {
        self.navigationController?.pushViewController(CategoriesAttached(categoryID: "4",categoryName: "Electronics"), animated: true)
    }
    
    @IBAction func btnFashion(_ sender: Any) {
        self.navigationController?.pushViewController(CategoriesAttached(categoryID: "5",categoryName: "Fashion"), animated: true)
    }
}



extension ExploreVC: DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        switch self.viewModel.state {
                       case .empty, .error:
                           return true
                       case .loading:
                            return false
                       case .populated:
                          return false
        }
            
        }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                         str = "DZNEmpty-Something-Wrong".autoLocalize()
                        break
                       case .empty:
                          str = "You haven't created any orders yet"
                        break
                        default:
                           str = ""
                        break
        }
      if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str: String!
        switch self.viewModel.state {
                       case .error:
                        str = "DZNEmpty-Internet-Connection".autoLocalize()
                        break
                       case .empty:
                          str = "Check our bestsellers and find something for you"
                        break
                        default:
                           str = ""
                        break
        }
     
        if(Localize.getLocal() == "en"){
        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
        else{
             let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Regular", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.black ]
            return NSAttributedString(string: str, attributes: myAttribute)
        }
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let str: String!
        switch self.viewModel.state {
                              case .error:
                                  str = "DZNEmpty-Btn-Retry".autoLocalize()
                               break
                              case .empty:
                                 str = "Check Best Sellers"
                               break
                               default:
                                  str = ""
                               break
               }
          if(Localize.getLocal() == "en"){
              let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Poppins-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
                  return NSAttributedString(string: str, attributes: myAttribute)
              }
              else{
                   let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Tajawal-Bold", size: 18.0)!, NSAttributedString.Key.foregroundColor: UIColor.white ]
                  return NSAttributedString(string: str, attributes: myAttribute)
              }
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let buttonFrame = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: 50)
        let buttonContainer = UIView(frame: buttonFrame)
        buttonContainer.backgroundColor = scrollView.backgroundColor

        let imageView = UIImageView(frame: buttonFrame)
        imageView.backgroundColor = ColorPalette.primaryColor
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        buttonContainer.addSubview(imageView)
        return createImage(buttonContainer)

//        let capInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
//        var rectInsets: UIEdgeInsets = .zero
//        var imageName = ""
//
//        if state == .normal {
//            imageName = "dz-empty-button-bg"
//        }
//        if state == .highlighted {
//            imageName = "dz-empty-button-bg"
//        }
//
//        rectInsets = UIEdgeInsets(top: -19.0, left: -61.0, bottom: -19.0, right: -61.0)
//        let image = UIImage(named: imageName, in: Bundle(for: type(of: self)), compatibleWith: nil)
//
//        return image?.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
         
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        switch self.viewModel.state {
                       case .error:
                          viewModel.initFetch()
                        break
                       case .empty:
                          self.dismiss(animated: true, completion: nil)
                        break
                        default:
                        break
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        var image: UIImage?
        switch self.viewModel.state {
                              case .error:
                                  image = UIImage(named: "connection-error")
                               break
                              case .empty:
                                 image = UIImage(named: "no-available-products")
                               break
                               default:
                                  image = UIImage(named: "no-available-products")
                               break
               }
        return image
    }
    func createImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: view.frame.width, height: view.frame.height), true, 1)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    

}
