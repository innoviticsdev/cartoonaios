//
//  CategoriesCell.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/12/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewLbl: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()

    }
    
    func setupViews(){
        roundedViewWithShadow(view: cellView)
        imgView.roundCorners([.layerMinXMinYCorner, .layerMinXMaxYCorner], radius: 10,  borderWidth: 1)
        if(Localize.getLocal() == "en")
        {
        viewLbl.roundCorners([.layerMaxXMaxYCorner, .layerMaxXMinYCorner], radius: 10,  borderWidth: 1)
        }
        else{
            viewLbl.roundCorners([.layerMinXMinYCorner, .layerMinXMaxYCorner], radius: 10,  borderWidth: 1)
        }
    }
    
    func configureCell(obj: Category){
        lblName.text = obj.name
        imgView.kf.setImage(with: URL(string: obj.imageURL))
    }
    
}

