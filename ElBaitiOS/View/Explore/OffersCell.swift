//
//  OffersCell.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/11/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class OffersCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var offersView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundedViewWithShadow(view: offersView)
        roundedViewWithShadow(view: btnView)

    }

    func configureCell(obj: Deal){
        imgView.kf.setImage(with: URL(string: obj.image))
    }
    
}


