//
//  SelectCountryVC.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/9/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import GestureRecognizerClosures

enum SelectedCountry{
    case SelectEgypt
    case SelectEmirates
    case SelectGermany
}

class SelectCountryVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var selectViewEgypt: UIView!
    @IBOutlet weak var selectViewEmirates: UIView!
    @IBOutlet weak var selectViewGermany: UIView!
    @IBOutlet weak var checkMarkEgypt: UIImageView!
    @IBOutlet weak var checkMarkEmirates: UIImageView!
    @IBOutlet weak var checkMarkGermany: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRoundedBtn(btn:btnStart, color:#colorLiteral(red: 0.07375442237, green: 0.5983123779, blue: 0.6038841605, alpha: 1))
        setupCellsCheckMark()

        selectViewEgypt.onTap { _ in
            self.selectCountry(selectedCountry: .SelectEgypt)
        }
        
        selectViewEmirates.onTap { _ in
            self.selectCountry(selectedCountry: .SelectEmirates)
        }
        
        selectViewGermany.onTap { _ in
            self.selectCountry(selectedCountry: .SelectGermany)
        }
    }
    
    func setupCellsCheckMark() {
        checkMarkEgypt.isHidden = true
        checkMarkEmirates.isHidden = true
        checkMarkGermany.isHidden = true
    }
    
    func selectCountry(selectedCountry: SelectedCountry){
        switch selectedCountry{
        case .SelectEmirates:
            changeCheckMark(imgView: checkMarkEmirates)
            break
        case .SelectGermany:
            changeCheckMark(imgView: checkMarkGermany)
            break
        // Default case Egypt.
        default:
            changeCheckMark(imgView: checkMarkEgypt)
            break
        }
    }
    
    func changeCheckMark( imgView: UIImageView) {
        setupCellsCheckMark()
        imgView.isHidden = false
    }
}
