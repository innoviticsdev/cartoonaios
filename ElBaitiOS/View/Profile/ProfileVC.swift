//
//  ProfileVC.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Kingfisher

enum ProfileSection : String, CaseIterable{
    case accounts = "Security"
}

class ProfileVC: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    var sectionMyAccount = ["ProfileVC-ChangePassword".autoLocalize(),"ProfileVC-ChangePhoneNumber".autoLocalize()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confNavBar()
        setupDelegates()
        makeCircleImgView(imgView: imgView)
        makeRoundedButtonBorder(btn: btnEdit)
        setupObserver()
    }
    
    func setupObserver() {
        profileNotificationCtr.addObserver(self, selector: Selector(("updateObserver")), name: NSNotification.Name(rawValue: profileBroadcast), object: nil)
    }
        
    // MARK: Observer Selector functions
    @objc func updateObserver(){

        let defaults: UserDefaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "UserData") as? Data {
            let decoder = JSONDecoder()
            if let savedPerson = defaults.object(forKey: "UserData") as? Data {
                let decoder = JSONDecoder()
                if let loadedPerson = try? decoder.decode(User.self, from: savedPerson) {
                    userName.text = loadedPerson.name
                    userEmail.text = loadedPerson.email
                    imgView.kf.setImage(with: URL(string: loadedPerson.image),placeholder: UIImage(named: "product-placeholder"))
                
                }
            }

        }
    }
    
    deinit {
        profileNotificationCtr.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(checkIfSignedIn())
                      {
                          let defaults: UserDefaults = UserDefaults.standard

                          if let savedPerson = defaults.object(forKey: "UserData") as? Data {
                              let decoder = JSONDecoder()
                              if let loadedPerson = try? decoder.decode(User.self, from: savedPerson) {
                                  userName.text = loadedPerson.name
                                  userEmail.text = loadedPerson.email
                                  imgView.kf.setImage(with: URL(string: loadedPerson.image),placeholder: UIImage(named: "product-placeholder"))
                              
                              }
                          }

                      }
                     
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func confNavBar(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.topItem!.title = "Hi, Ahmed"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
    
    func setupDelegates(){
    tableView.delegate = self
    tableView.dataSource = self
    tableView.registerCellNib(cellClass: MeTableViewCell.self)
    tableView.registerCellNib(cellClass: MeHeaderTableViewCell.self)
    }
    
    @IBAction func goEditPage(_ sender: Any) {
        let editProfileVC = EditProfileVC()
            editProfileVC.modalPresentationStyle = .fullScreen
            self.present(editProfileVC, animated: true, completion: nil)
        
    }
    
    @IBAction func DismissPage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}

extension ProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ProfileSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return sectionMyAccount.count
            
        default:
          return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    func setHeaderHeight(height: Double) {
        tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(height))

        tableView.tableHeaderView?.frame.size = CGSize(width:tableView.frame.width, height: CGFloat(height))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue() as MeTableViewCell
        switch (indexPath.section) {
        case 0:
          cell.lblTitle?.text = sectionMyAccount[indexPath.row]
            
        default:
          cell.lblTitle?.text = "Other"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let headerCell = tableView.dequeue() as MeHeaderTableViewCell
        
        headerCell.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
    
          switch (section) {
          case 0:
            headerCell.lblName.text = "ProfileVC-MyAccount".autoLocalize()
          case 1:
            headerCell.lblName.text = Section.settings.rawValue
          case 2:
            headerCell.lblName.text = Section.other.rawValue
          default:
            headerCell.lblName.text = "Other";
          }
    
          return headerCell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    //  Determine what to do when a cell in a particular section is selected.
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                
                print("Change Password")
                let changePasswordVC = ChangePasswordViewController()
                changePasswordVC.modalPresentationStyle = .fullScreen
                self.present(changePasswordVC, animated: true, completion: nil)
            case 1:
                print("Change Phone number")
                let changePhoneVC = ChangePhoneVC()
                    changePhoneVC.modalPresentationStyle = .fullScreen
                    self.present(changePhoneVC, animated: true, completion: nil)
                
            default:
                break
            }
            
        default:
            print("Out of index")
        }
    }
}
