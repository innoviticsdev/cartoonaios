//
//  RatingVC.swift
//  ElBaitiOS
//
//  Created by mac on 9/15/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class RatingVC: UIViewController {

    var orderId: Int?
    var arrOrderItems:[MyOrderItem]?
    lazy var viewModel: RatingVM = {
        return RatingVM()
    }()
    
    @IBOutlet weak var btnSubmit: TRButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var AddNewAddressButton: TRButton!{didSet{
       AddNewAddressButton.layer.cornerRadius = 10
       }}
    
   @IBOutlet weak var BottomView: UIView!{didSet{
    BottomView.layer.shadowColor = UIColor.gray.cgColor
    BottomView.layer.shadowOpacity = 0.5
    BottomView.layer.shadowOffset = .zero
    BottomView.layer.shadowRadius = 5
    BottomView.layer.cornerRadius = 20
   }}
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        initVM()
    }
    
    func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerCellNib(cellClass: AddRatingCell.self)
    }
    
    func initVM() {
        
        // Set binding init
        viewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.viewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                case .empty, .error:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                case .loading:
                    indicatorSwitch(status: .on,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: false)
                case .populated:
                    indicatorSwitch(status: .off,view: self.view)
                    toggleCardSwipe(viewController : self, allowSwipe: true)
                    self.present(RatePopup(), animated: true, completion: nil)
                }
            }
        }

    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        var dic : [String : Any] = [:]
        var isChange = false
        
        //Loop tableView cells to check if product is already rated!
        for section in 0...self.tableView.numberOfSections - 1 {
            for row in 0...self.tableView.numberOfRows(inSection: section) - 1 {
                let cell = self.tableView.cellForRow(at: NSIndexPath(row: row, section: section) as IndexPath) as! AddRatingCell as AddRatingCell

                let rating = cell.cosmosRating.rating
                let comment = cell.txtComment.text
                let productID = cell.productID
                let isReviewd = cell.isReviewed
                
                if(rating >= 1.0 && isReviewd == false){
                    isChange = true
                    dic.updateValue(["rating":rating ?? 0 , "comment":comment ?? ""], forKey: "\(productID ?? 0)")
                }
            }
        }
        
        let myDict : [String : Any] = [
        "order_id": orderId ?? 0,
        "ratings": dic
        ]

        if(isChange){
            viewModel.submitOrder(dic: myDict)
        }else{
            showAlert("Please rate one product at least.")
        }
        
    }

    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}



extension RatingVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue() as AddRatingCell
        cell.configureCell(objItem: (arrOrderItems?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
}
