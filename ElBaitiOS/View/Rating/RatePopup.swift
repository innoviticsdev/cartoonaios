//
//  RatePopup.swift
//  ElBaitiOS
//
//  Created by mac on 9/15/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class RatePopup: UIViewController {

    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var btnThanks: TRButtonBold!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeRoundedCorners(view: viewPopup)
    }

    @IBAction func btnClose(_ sender: Any) {
       dismissRating()
    }
    
    @IBAction func btnThankYou(_ sender: Any) {
        dismissRating()
    }
    
    func dismissRating(){
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
