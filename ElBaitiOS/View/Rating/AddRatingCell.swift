//
//  AddRatingCell.swift
//  ElBaitiOS
//
//  Created by mac on 9/15/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Cosmos

class AddRatingCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblColor: UILabel!
    @IBOutlet weak var cosmosRating: CosmosView!
    @IBOutlet weak var txtComment: UITextField!
    
    var productID: Int?
    var isReviewed = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(objItem: MyOrderItem){
        
        if(objItem.isReviewed){
            disableEditingReview(objItem: objItem)
        }else{
            cosmosRating.rating = 0
            txtComment.text = ""
            }
        
        productID = objItem.product?.id
        imgView.kf.setImage(with: URL(string: objItem.product?.baseImage.smallImageURL ?? ""),placeholder: UIImage(named: "product-placeholder"))
        lblName.text = objItem.product?.name
        //lblSize.text = "Size: \(objItem.product.)"
        //lblColor.text = "Color: \(objItem.product.)"
    }
    
    func disableEditingReview(objItem: MyOrderItem){
        isReviewed = true
        cosmosRating.rating = Double(objItem.reviews?.rating ?? "0.0") ?? 0.0
        cosmosRating.settings.updateOnTouch = false
        txtComment.text = objItem.reviews?.comment
        txtComment.isUserInteractionEnabled = false
    }
    
}
