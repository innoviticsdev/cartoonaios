//
//  ReviewsVC.swift
//  ElBaitiOS
//
//  Created by mac on 9/14/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

class ReviewsVC: UIViewController {

    @IBOutlet weak var ratingTV: UITableView!
    @IBOutlet weak var reviewsTV: UITableView!

    var productId: Int?
    lazy var viewModel: ReviewsVM = {
        return ReviewsVM()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupPagination()
    }
    
    func setupPagination(){
        self.viewModel.currentPage = 1
        
        self.reviewsTV.es.addPullToRefresh {
                   [weak self] in
            self?.viewModel.currentPage = 1
            self?.viewModel.getReviews(productId: self?.productId ?? 0, refreshType: .refresh)
               }
        self.reviewsTV.es.addInfiniteScrolling {
            [weak self] in
            self?.viewModel.getReviews(productId: self?.productId ?? 0, refreshType: .loadmore)
        }
        initVM()
        viewModel.getReviews(productId: productId ?? 0, refreshType: .refresh)
    }
    
    func setupTableView(){
        reviewsTV.rowHeight = UITableView.automaticDimension
        reviewsTV.estimatedRowHeight = 600
        
        ratingTV.dataSource = self
        ratingTV.delegate = self
        
        reviewsTV.dataSource = self
        reviewsTV.delegate = self
        
        ratingTV.registerCellNib(cellClass: RatingCell.self)
        reviewsTV.registerCellNib(cellClass: ReviewCell.self)
    }
    
    func initVM() {

                viewModel.updateLoadingStatus = { [weak self] () in
                    guard let self = self else {
                        return
                    }

                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else {
                            return
                        }
                        switch self.viewModel.state{
                        case .empty, .error:
                            self.reviewsTV.es.stopPullToRefresh()
                            indicatorSwitch(status: .off,view: self.view)
                        case .noMore:
                            indicatorSwitch(status: .off,view: self.view)
                            self.reviewsTV.es.noticeNoMoreData()
                            self.reviewsTV.es.stopLoadingMore()
                        case .loading:
                            indicatorSwitch(status: .on,view: self.view)
                        case .populated:
                        indicatorSwitch(status: .off,view: self.view)
                        self.ratingTV.reloadData()
                        self.reviewsTV.reloadData()
                        self.reviewsTV.es.stopLoadingMore()
                        self.reviewsTV.es.stopPullToRefresh()
                        }
                    }
                }
            }
            
            func showAlert( _ message: String ) {
                let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
                alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ReviewsVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case ratingTV:
            return 5
        
        case reviewsTV:
            let cellsCount = viewModel.arrReviews.count
            return cellsCount
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
        case ratingTV:
            let cell = ratingTV.dequeue() as RatingCell
            
            
            cell.configureCell(index: indexPath.row, percentage: viewModel.percentage)
            return cell
        
        case reviewsTV:
            let cell = reviewsTV.dequeue() as ReviewCell
            cell.configureCell(objReview: viewModel.arrReviews[indexPath.row])
            return cell
            
        default:
            let cell = ratingTV.dequeue() as RatingCell
            return cell
        }
    }
}
