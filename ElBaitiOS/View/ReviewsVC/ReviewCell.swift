//
//  ReviewCell.swift
//  ElBaitiOS
//
//  Created by mac on 9/14/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class ReviewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var rateCosmos: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeCircleImgView(imgView: imgView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(objReview: ReviewsData){
        imgView.kf.setImage(with: URL(string: objReview.image),placeholder: UIImage(named: "avatar"))
        lblName.text = objReview.name
        let ratingDouble = Double(objReview.rating)
        rateCosmos.rating = ratingDouble ?? 0.0
        lblDate.text = objReview.createdAt
        lblDesc.text = objReview.comment
    }
    
}
