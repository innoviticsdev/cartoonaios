//
//  RatingCell.swift
//  ElBaitiOS
//
//  Created by mac on 9/14/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Cosmos

class RatingCell: UITableViewCell {

    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var ratinngCosmos: CosmosView!
    @IBOutlet weak var lblRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureCell(index: Int, percentage: Percentage?){

        switch index {
        case 0:
            fillCell(color: #colorLiteral(red: 0.2901960784, green: 0.6784313725, blue: 0.3215686275, alpha: 1),rating: 5,star: percentage?.fiveStar)
            break
        
        case 1:
            fillCell(color: #colorLiteral(red: 0.6470588235, green: 0.8392156863, blue: 0.2274509804, alpha: 1),rating: 4,star: percentage?.fourStar)
            break
            
        case 2:
            fillCell(color: #colorLiteral(red: 0.968627451, green: 0.9019607843, blue: 0.2588235294, alpha: 1),rating: 3,star: percentage?.threeStar)
            break
            
        case 3:
            fillCell(color: #colorLiteral(red: 0.9568627451, green: 0.6274509804, blue: 0.231372549, alpha: 1),rating: 2,star: percentage?.twoStar)
            break
          
        case 4:
            fillCell(color: #colorLiteral(red: 0.9176470588, green: 0.2235294118, blue: 0.2196078431, alpha: 1),rating: 2,star: percentage?.oneStar)
             break
        
        default:
            print("OUT OF INDEX")
        }
    }
    
    func fillCell(color: UIColor,rating: Double,star: Star?){
        progressBar.progressTintColor = color
        progressBar.progress = (Float(star?.percentage ?? 0) / 100)
        ratinngCosmos.rating = rating
        lblRate.text = "\(star?.count ?? 0 )"
    }
}
