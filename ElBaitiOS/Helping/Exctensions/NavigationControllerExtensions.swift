//
//  NavigationControllerExtensions.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 8/18/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationController {

    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }

}
