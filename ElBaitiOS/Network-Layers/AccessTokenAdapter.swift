//
//  AccessTokenAdapter.swift
//  Sportoya
//
//  Created by Mohamed Gamal on 9/23/17.
//  Copyright © 2017 Innovitics Inc. All rights reserved.
//

import Alamofire

class AccessTokenAdapter: RequestAdapter {
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        print("Adapt")
    }
    
    private let accessToken: String
    
    init(accessToken: String) {
        self.accessToken = accessToken
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(Router.baseURLString) {
            let Token = UserDefaults.standard.string(forKey: "Token") ?? ""
            urlRequest.setValue("Bearer " + Token, forHTTPHeaderField: "Authorization")
        
            //urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        }
        
        return urlRequest
    }
}
