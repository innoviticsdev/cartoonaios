//
//  MultiFormManager.swift
//  LeCasa
//
//  Created by Maged Aziz on 4/16/20.
//  Copyright © 2020 Alaamaher. All rights reserved.
//

import Foundation

class MultiFormManager: NSObject {
    
    public var fileName: String?
    public var name: String?
    public var mimeType = "image/png"
    public var data: Data?
    
    
    public init(fileName: String, name: String, mimeType: String, data: Data) {
        self.fileName = fileName
        self.name = name
        self.mimeType = mimeType
        self.data = data
    }
}
