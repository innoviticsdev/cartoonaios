//
//  interfaceConstants.swift
//  Sportoya
//
//  Created by Mohamed Gamal on 8/28/17.
//  Copyright © 2017 Innovitics Inc. All rights reserved.
//

import Foundation




let ImagesDirectory =  "Images/"
let APIappKey = "cf0780ac2d5a8b689e2b74e55f944dcd"
let APIappSecret = "6b706fdcc373ebe43d26fadc6091bada"
let APIRequest = "http://coldstone-beta.us-west-2.elasticbeanstalk.com/api/"


struct interfaceConstants {
    
    
    struct RESTRequest {
        
        static let login = APIRequest + "login"
        static let register = APIRequest + "register"
        
        static let listSessions = APIRequest + "listSession"
        static let listSportnavbar = APIRequest + "listSportnavbar"
        static let listSportIntrest = APIRequest + "listSportinterest"
        
        static let temToken = UserDefaults.standard.string(forKey: "Token") ?? ""
        
    }
    
    struct GetImagesDirectory {
        
        static let getMakerImage = ImagesDirectory + "Brands/"
        
        
    }
    struct GetAppAuth {
        
        static let appKey = APIappKey
        static let appSecret = APIappSecret
        static let appSecretBody = "appSecret=\(appSecret)"
    }
    
    
}


