//
//  Router.swift
//  Sportoya
//
//  Created by Mohamed Gamal on 9/23/17.
//  Copyright © 2017 Innovitics Inc. All rights reserved.
//


import Alamofire

enum Router: URLRequestConvertible {
    
    // Start Maged
    case createAccount(parameters: Parameters)
    case Login(parameters: Parameters)
    case getCategories(parameters: Parameters)
    case getProductsAttachedToCategory(parameters: Parameters)
    case getProductDetails(parameters: Parameters)
    case getMyCart
    case Logout
    case MyOrders
    case getRelatedProducts (parameters: Parameters)
    case AddToCart(parameters: Parameters)
    case ForgotPassoword(parameters: Parameters)
    case SaveAddress(parameters: Parameters)
    case cancelOrder(parameters: Parameters)
    case removeItem(parameters: Parameters)
    case updateItem(parameters: Parameters)
    case removeAddress(parameters: Parameters)
    case listWishlist
    case moveToCart(parameters: Parameters)
    case AddRemoveWishlist(parameters: Parameters)
    

    // End Maged
    
    
    //Start Ramzy
    case getExplore
    case checkEmail(parameters: Parameters)
    case loginPassword(parameters: Parameters)
    case register(parameters: Parameters)
    case updateProfiles(parameters: Parameters)
    case getAdressesList
    case getCoupon(parameters: Parameters)
    case removeCoupon
    case prepareOrder(parameters: Parameters)
    case submitOrder
    case getReviews(parameters: Parameters)
    case submitReview(parameters: Parameters)
    
    //End Ramzy
    
    case submitHelp(parameters: Parameters)

    //https://api.el-bait.com
    

    static let reachabilityURLString = "https://api.el-bait.com/api/" //"https://apisb.el-bait.com/api/"
    
    static let baseURLString = "https://api.el-bait.com/api/"
    //"https://apisb.el-bait.com/api/"

    
    var method: HTTPMethod {
        switch self {
        case .getCategories:
            return .get
        case .getProductsAttachedToCategory:
            return .get
        case .getMyCart:
            return .get
        case .getExplore:
            return .get
        case .getProductDetails:
            return .get
        case .Logout:
            return .get
        case .getRelatedProducts:
            return .get
        case .MyOrders:
            return .get
        case .getAdressesList:
            return .get
        case .getCoupon:
            return .post
        case .submitHelp:
            return .post
        case .updateItem:
            return .post
        case .cancelOrder:
            return .post
        case .removeCoupon:
            return .delete
        case .removeItem:
            return .get
        case .moveToCart:
        return .get
            case .removeAddress:
            return .delete
        case .submitOrder:
            return .post
        case .getReviews:
            return .get
        case .submitReview:
            return .post
        case .listWishlist:
        return .get
            
        case .AddRemoveWishlist:
        return .get
        default:
            return .post
            
        }
    }

    var params: ([String: Any]?) {
           switch self {

         
           default:
               break
           }
           return nil
       }
       
    
    var path: String {
        switch self {
  
        case .createAccount:
            return "users/createAccount"
        case .Login:
            return "users/login"
        case .getCategories:
            return "categories"
        case .getProductsAttachedToCategory:
            return "products"
        //Start Ramzy
        case .getExplore:
            return "explore"
        case .getProductDetails:
            return "products/get"
        case .getMyCart:
            return "checkout/cart"
        case .checkEmail:
            return "customer/check"
        case .Logout:
            return "customer/logout"
        case .getRelatedProducts:
            return "products/related"
        case .loginPassword:
            return "customer/login"
        case .register:
            return "customer/register"
        case .updateProfiles:
            return "customer/profile"
        case .MyOrders:
            return "orders"
        case .AddToCart:
            return "checkout/cart/add"
        case .ForgotPassoword:
            return "customer/forgot-password"
        case .getAdressesList:
            return "addresses"
        case .SaveAddress:
            return "addresses/create"
        case .getCoupon:
            return "checkout/cart/coupon"
        case .cancelOrder:
            return "orders/cancel"
        case .removeItem:
            return "checkout/cart/remove-item"
        case .updateItem:
            return "checkout/cart/update"
        case .removeAddress:
            return "addresses"
        case .submitOrder:
            return "checkout/save-order"
        case .submitHelp:
            return "help"
        case .removeCoupon:
            return "checkout/cart/coupon"
        case .prepareOrder:
            return "checkout/prepare-order"
        case .getReviews:
            return "reviews/get"
        case .submitReview:
            return "reviews/create"
        
        case .listWishlist:
        return "wishlist"
            
        case .moveToCart:
        return "move-to-cart"
            
        case .AddRemoveWishlist:
        return "wishlist/add"
        //End Ramzy
      
    
        
        }
    }
    
    var accessTokenEnabled: Bool
    {
        switch self {
        case.getCategories:
            return false
        //Start Ramzy
        case .getExplore:
            return true
        case .getProductDetails:
            return true
        case .getMyCart:
            return true
        case .checkEmail:
            return true
        case .getRelatedProducts:
            return false
        case .ForgotPassoword:
            return false
        case .loginPassword:
            return true
        case .register:
            return true
        case .updateProfiles:
            return true
        case .getAdressesList:
            return true
        case .getCoupon:
            return true
        case .cancelOrder:
            return true
        case .prepareOrder:
            return true
        case .removeCoupon:
            return true
        case .removeItem:
            return true
        case .updateItem:
            return true
        case .submitOrder:
            return true
        case .submitHelp:
            return true
        case .getReviews:
            return true
        case .submitReview:
            return true
        default:
                return true
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Router.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        urlRequest.timeoutInterval = 30
        
        switch self {
        case .createAccount(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .ForgotPassoword(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .Login(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .getProductsAttachedToCategory(let parameters):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            break
        case .getProductDetails(let parameters):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            break
        case .getRelatedProducts(let parameters):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            break
        case .checkEmail(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .loginPassword(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .register(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .updateProfiles(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .getCoupon(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .AddToCart(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .SaveAddress(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .submitHelp(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
            break
        case .cancelOrder(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
        break
        case .updateItem(let parameters):
                urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
        break
        case .prepareOrder(let parameters):
            urlRequest = try URLEncoding.httpBody.encode(urlRequest, with: parameters)
        break
        case .removeItem(let parameters):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            break
            
        case .moveToCart(let parameters):
                      urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
                  break
            
        case .removeAddress(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        break
            
        case .AddRemoveWishlist(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        break
        case .getCategories(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        break
        case .getReviews(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        break
        case .submitReview(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        break
        default:
            break
        }
        urlRequest = try URLEncoding.default.encode(urlRequest, with: ["locale":Localize.getLocal()])
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = method.rawValue
        if accessTokenEnabled {
            urlRequest = try AccessTokenAdapter(accessToken: interfaceConstants.RESTRequest.temToken).adapt(urlRequest)
        }
        urlRequest.cachePolicy = .reloadIgnoringLocalCacheData
        return urlRequest
    }

    // Still under development
    
    func Query(urlRequest: URLRequest,parameters: Parameters) throws -> URLRequest
    {
           return try URLEncoding.httpBody.encode(urlRequest, with: parameters)
    }
    
    func Field(urlRequest: URLRequest,parameters: Parameters) throws -> URLRequest
    {
         return try URLEncoding.queryString.encode(urlRequest, with: parameters)
    }
}

