//
//  RestApiManager.swift
//  Shift Inc.
//
//  Created by Mohamed Gamal on 2/7/18.
//  Copyright © 2018 Innovitics Inc. All rights reserved.
//

import Foundation

import UIKit
import Alamofire
import SwiftyJSON

import GestureRecognizerClosures

class RestApiManager
{
    typealias CompletionHandler = (_ success:Bool, _ JSON: JSON,_ status: String) -> Void
    
    
    var TestingEnabled = false
    var PrintStatusEnabled = false
    var AutoReachEnabled = false
    let sessionManager = Session()          // Create a session manager
   // Create a request retrier

    
    var request: URLRequestConvertible
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var pendingConnectivity = false
    var manager: NetworkReachabilityManager!
    
    

    
    public init(request: URLRequestConvertible)
    {
        self.request = request
    }
    
    func Request(completionHandler: @escaping CompletionHandler)
    {
        
       // ReachabilityManager(completionHandler: completionHandler)
        
            sessionManager.request(self.request).responseString{ (response) -> (Void) in
                // print("request body: \(self.request.urlRequest?.httpBody!)")
                //self.TestingEnabled ? self.DisplayRequestData(response: response) : nil
                
                
                print(response)
                print(response.request!)  // original URL request
                print(response.request?.httpBody ?? "Undefined httpBody")
                print(response.response)
                
                
                if response.data != nil {
                    
                    do {
                        let jsonArray = try JSON(data: response.data!)
                        
                        
                        self.TestingEnabled ? self.DisplayJSON(data: jsonArray) : nil
                        
                        if(jsonArray != JSON.null )
                        {
                            self.PrintStatusEnabled ? self.DisplayJSON(data: jsonArray) : nil
                            
                            completionHandler(true,jsonArray,jsonArray["status"]["code"].stringValue)
                            
                        }
                        else
                        {
                            completionHandler(false,JSON(),jsonArray["status"]["code"].stringValue)
                        }
                        
                    } catch {
                        completionHandler(false,JSON(),"9000")
                    }
                    
                    
                }
                else
                {
                    print("No Connection Error")
                    completionHandler(false,JSON(),"9000")
                    
                }
                
            }

        
        
        
    }
    
   
    func Request(with: MultiFormManager, completionHandler: @escaping CompletionHandler,param: [String:Any])
    {
        

        
      AF.upload(multipartFormData: { multipartEncoding in
       
        multipartEncoding.append(with.data!, withName: with.name!, fileName: with.fileName!, mimeType: with.mimeType)
        
    
        for (key, value) in param {
            multipartEncoding.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
        }
      
      }, with: self.request)
          .uploadProgress(queue: .main, closure: { progress in
              //Current upload progress of file
              print("Upload Progress: \(progress.fractionCompleted)")
          })
          .responseJSON(completionHandler: { data in
              do{
                 
                  print(data.request!)  // original URL request
                  print(data.request?.httpBody ?? "Undefined httpBody")
                
                  print(data.response)
                  
                  if data.data != nil {
                      
                      var jsonArray = try JSON(data: data.data!)
                      
                      self.TestingEnabled ? self.DisplayJSON(data: jsonArray) : nil
                      
                      if(jsonArray != JSON.null )
                      {
                          self.PrintStatusEnabled ? self.DisplayJSON(data: jsonArray) : nil
                          
                          if(jsonArray["status"]["code"].stringValue == "8000")
                          {
                              
                          }
                          completionHandler(true,jsonArray,jsonArray["status"]["code"].stringValue)
                          
                      }
                      else
                      {
                          completionHandler(false,JSON(),jsonArray["status"]["code"].stringValue)
                      }
                      
                      
                      
                      
                  }
                  else
                  {
                     
                      completionHandler(false,JSON(),"9000")
                  }
              }
              catch {
                
              }
          })
        
        
        
        
    }

    
   
 
    
    func DisplayJSON(data: JSON)
    {
        print("JSON: \(data.arrayValue.count)")
    }
    
    func DisplayStatus(data:JSON)
    {
        print(data["status"]["message"].stringValue)
    }
    
    
    
    
}




