//
//  AppDelegate.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/9/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import FirebaseDynamicLinks
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
var window: UIWindow?
var enableTransformation = false
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        
        
        
      if #available(iOS 13.0, *) {

            window?.overrideUserInterfaceStyle = .light

        }
        else
        {
            setUpWindow(window: window)
        }
        
        UITabBarItem.appearance()
        .setTitleTextAttributes(
            [NSAttributedString.Key.font: UIFont(name: Localize.getLocal() == "ar" ? "Tajawal-Regular" : "Poppins-Regular", size: 10)!],
        for: .normal)
        
        Majino(alertView: false).check()
        
        //Google Map and Google Places SDK Keys Init
        GMSServices.provideAPIKey("AIzaSyDQSs4yF-3GH7IR9EqwOYncMLvRRLLAMWs")
        GMSPlacesClient.provideAPIKey("AIzaSyAtUd5-WAVGSNJD7JHkstD2BNGmzsPIAZ8")
        
  

        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                        restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
               let url = dynamiclink?.url?.absoluteString
               
               if let range = url!.range(of: "id=") {
                   let ProductID = url![range.upperBound...]
                   CustomizedTabBarController.DeepProductID = ProductID.description
               }
               
               print(url!)
                   NotificationCenter.default.post(name: Notification.Name(rawValue: "GoToProductDetails"), object: nil, userInfo: nil)
           }
           
           return handled
       }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

