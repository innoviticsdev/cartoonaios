//
//  Utilis.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/9/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

func setUpRoundedBtn(btn:UIButton, color:UIColor)
{
    btn.backgroundColor = color
    btn.layer.cornerRadius = 7
    btn.clipsToBounds = true
}

func setUpWindow(window :UIWindow?)
{
        window?.rootViewController = CustomizedTabBarController()
                     
        if(Localize.getLocal() == "en")
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            goToRoot()
        }
        else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            goToRoot()
        }
}


