//
//  AddRemoveToWishlistClass.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/14/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation
import UIKit

class AddRemoveToWishlist {
   
    
    static func CallAPI (ProductID: String){
        let viewModel: WishlistVM = {
               return WishlistVM()
           }()
        viewModel.removeParam = ["id":ProductID]
        viewModel.addRemoveToWishlistAPI()
    }
}
