//
//  MajinoConfig.swift
//  Sportoya
//
//  Created by MOHAMED GAMAL on 5/13/19.
//  Copyright © 2019 Innovitics Inc. All rights reserved.
//

public struct MajinoConfig {
    
    public static let KEY_UPDATE_MAJOR: String = "current_major_version_IOS"
    public static let KEY_UPDATE_MINOR: String = "current_minor_version_IOS"
    public static let KEY_UPDATE_URL: String = "store_uri_ios"
    public static let KEY_ALERT_MINOR_TITLE: String = "alert_minor_title_IOS"
    public static let KEY_ALERT_MAJOR_TITLE: String = "alert_major_title_IOS"
    public static let KEY_ALERT_MINOR_BODY: String = "alert_minor_body_IOS"
    public static let KEY_ALERT_MAJOR_BODY: String = "alert_major_body_IOS"
    public static let KEY_ALERT_POSITIVE: String = "alert_positive_IOS"
    public static let KEY_ALERT_NEGATIVE: String = "alert_negative_IOS"

}

enum MajinoUpdateType {
    case major
    case minor
}
