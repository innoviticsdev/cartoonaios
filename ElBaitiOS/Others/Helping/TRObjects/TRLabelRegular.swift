//
//  TRLabel.swift
//  laverie
//
//  Created by Mohamed Gamal on 3/4/18.
//  Copyright © 2018 Innovitics Inc. All rights reserved.
//


import UIKit


 class TRLabelMedium: UILabel {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable
    public var ArabicFont: String = "Tajawal-Regular" {
        didSet{
            
            self.font = UIFont(name: ArabicFont, size: CGFloat(ArabicPointSize))

        }
    }
    
    @IBInspectable
    public var ArabicPointSize: CGFloat = 17 {
        didSet {
            
             self.font = self.font.withSize(ArabicPointSize)

            
        }
    }
    
    @IBInspectable
    public var EnglishFont: String = "Poppins-Regular" {
        didSet{
            
            self.font = UIFont(name: EnglishFont, size: CGFloat(EnglishPointSize))

        }
    }
    
    @IBInspectable
    public var EnglishPointSize: CGFloat = 15 {
        didSet {
            
            self.font = self.font.withSize(EnglishPointSize)

            
        }
    }
    
    
    @IBInspectable
    public var localizedString: String = "" {
        didSet {
            
            if(localizedString != "")
            {
                Localize.adjust(fonts: ["en":EnglishFont , "ar":ArabicFont , "en-size":EnglishPointSize.description,"ar-size":ArabicPointSize.description],type: "UILabel",item: self as AnyObject,localize: localizedString,upperCased: false)
            }

            // self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius

        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = self.borderWidth

        }
    }
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = self.borderColor.cgColor

        }
    }
    
    @IBInspectable
    public var textAlign: String = "auto" {
        didSet {
            switch textAlign {
            case "auto":
                Localize.getLocal() == "en" ? (self.textAlignment = .left) : (self.textAlignment = .right)
            case "right":
                
                self.textAlignment = .right

            case "left":
               
                self.textAlignment = .left
            
            case "center":
                
                self.textAlignment = .center

            default:
                Localize.getLocal() == "en" ? (self.textAlignment = .left) : (self.textAlignment = .right)
                
            }

        }
    }
    

}
