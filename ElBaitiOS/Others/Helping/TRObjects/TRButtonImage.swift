//
//  TRButton.swift
//  laverie
//
//  Created by Mohamed Gamal on 3/4/18.
//  Copyright © 2018 Innovitics Inc. All rights reserved.
//

import UIKit

 class TRButtonImage: UIButton {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    @IBInspectable
    public var IsFlip: Bool = false {
        didSet{
            if(IsFlip)
            {
            if(Localize.getLocal() == "ar")
            {
            var transform = CATransform3DIdentity
            transform = CATransform3DRotate(transform, CGFloat(Double(1) * .pi), 0, 1, 0)
            self.transform = CATransform3DGetAffineTransform(transform)
                }
            }
        }
    }
    
    @IBInspectable
    public var ArabicFont: String = "Tajawal-Regular"
    
    @IBInspectable
    public var ArabicPointSize: CGFloat = 17 {
        didSet {
            self.titleLabel?.font = self.titleLabel?.font.withSize(ArabicPointSize)
         

        }
    }
    
    @IBInspectable
    public var EnglishFont: String = "Poppins-Regular"
    
    @IBInspectable
    public var EnglishPointSize: CGFloat = 15 {
        didSet {
            
            self.titleLabel?.font = self.titleLabel?.font.withSize(EnglishPointSize)

        }
    }
    
    @IBInspectable
    public var localizedString: String = "" {
        didSet {
            
            Localize.adjust(fonts: ["en":EnglishFont , "ar":ArabicFont , "en-size":EnglishPointSize.description,"ar-size":ArabicPointSize.description],type: "UIButton",item: self as AnyObject,localize: localizedString,upperCased: false)

           // self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius

        }
    }
}
