//
//  LocalizeString.swift
//  laverie
//
//  Created by Mohamed Gamal on 2/7/18.
//  Copyright © 2018 Innovitics Inc. All rights reserved.
//


import Foundation
import UIKit

class LocalizeString: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        self.text!.autoLocalize()
    }
    
}

extension String {
    
    func autoLocalize() -> String {
        
        let language = UserDefaults.standard.string(forKey: "Language")
        if(language == "ar")
        {
            return self.localizedArabic()
        }
        else
        {
            return self.localizedEnglish()
        }
    }
    
   
    
    func localize(language: String) -> String {
        
        if(language == "ar")
        {
            return self.localizedArabic()
        }
        else
        {
            return self.localizedEnglish()
        }
    }
    
    func localizedArabic(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        let path = Bundle.main.path(forResource: "ar", ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: tableName,bundle: bundle!, value: "\(self)", comment: "")
    }
    
    func localizedEnglish(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        let path = Bundle.main.path(forResource: "en", ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: tableName,bundle: bundle!, value: "\(self)", comment: "")    }
    
}




