//
//  TRImage.swift
//  Shift
//
//  Created by Maged Aziz on 11/27/18.
//  Copyright © 2018 Innocitics Inc. All rights reserved.
//

import Foundation
import UIKit
class TRImage: UIImageView {
    
    
override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    @IBInspectable
    public var IsFlip: Bool = false {
        didSet{
            if(IsFlip)
            {
            if(Localize.getLocal() == "ar")
            {
            var transform = CATransform3DIdentity
            transform = CATransform3DRotate(transform, CGFloat(Double(1) * .pi), 0, 1, 0)
            self.transform = CATransform3DGetAffineTransform(transform)
                }
            }
        }
    }
    

}
