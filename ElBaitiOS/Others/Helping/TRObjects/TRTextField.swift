//
//  TRTextField.swift
//  laverie
//
//  Created by Mohamed Gamal on 3/4/18.
//  Copyright © 2018 Innovitics Inc. All rights reserved.
//


import UIKit

 class TRTextField: UITextField {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable
    public var ArabicFont: String = "Tajawal-Regular"
    
    @IBInspectable
    public var ArabicPointSize: CGFloat = 17 {
        didSet {
            
            self.font = self.font?.withSize(ArabicPointSize)

            
        }
    }
    
    @IBInspectable
    public var EnglishFont: String = "Poppins-Medium"
    
    @IBInspectable
    public var EnglishPointSize: CGFloat = 15 {
        didSet {
            
            self.font = self.font?.withSize(EnglishPointSize)

            
        }
    }
    
    @IBInspectable
    public var adjustFontOnly: String = "" {
        didSet {
            
            let fonts = ["en":EnglishFont , "ar":ArabicFont , "en-size":EnglishPointSize.description,"ar-size":ArabicPointSize.description]
            guard let fontSize = NumberFormatter().number(from: fonts[Localize.getLocal() + "-size"]!) else { return }
            
            self.font = UIFont(name: fonts[Localize.getLocal()]!, size: CGFloat(fontSize))
           
          
        }
    }
    
    @IBInspectable
    public var localizedPlaceholder: String = "" {
        didSet {
            
            Localize.adjust(fonts: ["en":EnglishFont , "ar":ArabicFont , "en-size":EnglishPointSize.description,"ar-size":ArabicPointSize.description],type: "UITextField",item: self as AnyObject,localize: localizedPlaceholder,upperCased: false)

        }
    }
    
    @IBInspectable
    public var WhitePlaceHolder: Bool = false {
        didSet {
            
            self.attributedPlaceholder = NSAttributedString(string:localizedPlaceholder.autoLocalize(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
        }
    }

    

    
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
            
        }
    }

   
}

