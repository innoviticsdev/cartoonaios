//
//  FontAdjuster.swift
//  laverie
//
//  Created by Mohamed Gamal on 2/7/18.
//  Copyright © 2018 Innovitics Inc. All rights reserved.
//

import Foundation
import UIKit


class Localize
{
    static let defaultLanguage = "en"
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate

    static func allowTransformation(status: Bool)
    {
        appDelegate.enableTransformation = status
    }
    
    static func getTransformationStatus() -> Bool
    {
        return appDelegate.enableTransformation
    }
    static func adjust(fonts: [String:String],type: String,item: AnyObject,localize: String,upperCased: Bool)
    {
        let numberFormatter = NumberFormatter()
        
        numberFormatter.locale = Locale(identifier: "en_US_POSIX")
        guard let fontSize = numberFormatter.number(from: fonts[getLocal() + "-size"]!) else { return }
        
        var locale = getLocal()
        switch type {
        case "UIButton":
            
            (item as! UIButton).titleLabel?.font = UIFont(name: fonts[locale]!, size: CGFloat(fontSize))
            (item as! UIButton).setTitle((upperCased ? localize.autoLocalize().uppercased() : localize.autoLocalize()), for: .normal)
            break
        case "UILabel":
            (item as! UILabel).font = UIFont(name: fonts[locale]!, size: CGFloat(fontSize))
            (item as! UILabel).text = (upperCased ? localize.autoLocalize().uppercased() : localize.autoLocalize())
               getLocal() == "en" ? ((item as! UILabel).textAlignment = .left) : ((item as! UILabel).textAlignment = .right)
            break
            
        case "UISearchBar":
            (item as! UISearchBar).placeholder = (upperCased ? localize.autoLocalize().uppercased() : localize.autoLocalize())
           
            break
            

        case "UITextField":
           
            (item as! UITextField).font = UIFont(name: fonts[locale]!, size: CGFloat(fontSize))
            (item as! UITextField).attributedPlaceholder = NSAttributedString(string:(upperCased ? localize.autoLocalize().uppercased() : localize.autoLocalize()), attributes: [NSAttributedString.Key.foregroundColor: ColorPalette.PlaceholderGrayColor])
            getLocal() == "ar" ? ((item as! UITextField).placeholder = (item as! UITextField).placeholder! ) : nil
            getLocal() == "en" ? ((item as! UITextField).textAlignment = .left) : ((item as! UITextField).textAlignment = .right)
            break
            
            case "UITextView":
            (item as! UITextView).font = UIFont(name: fonts[locale]!, size: CGFloat(fontSize))
            (item as! UITextView).text = (upperCased ? localize.autoLocalize().uppercased() : localize.autoLocalize())
            break
            
        case "UINavigationController":
            (item as! UINavigationController).navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: fonts[locale]!, size: CGFloat(truncating: fontSize))!]
            (item as! UINavigationController).title =  (upperCased ? localize.autoLocalize().uppercased() : localize.autoLocalize())
            break
        default:
            break
        }
    }
    
    static func autoAlignment() -> NSTextAlignment {
        
        let language = UserDefaults.standard.string(forKey: "Language")
        if(language == "ar")
        {
            return .right
        }
        else
        {
            return .left
        }
    }
    
    static func getLocal() -> String
    {
        var defaults = UserDefaults.standard
        
        let language = UserDefaults.standard.string(forKey: "Language")
        
        if(language == nil)
        {
            defaults.set(Localize.defaultLanguage, forKey: "Language")
            defaults.synchronize()
            return Localize.defaultLanguage
        }
        return language!
    }
    
    
    
    
}

extension NSMutableDictionary
{
    func autoLocalize()
    {
        if(self != nil)
        {
            
            self.setValue(Localize.getLocal(), forKey: "lang")
        }
        else
        {
            print("Unable to localize nil NSMutableDictionary")
        }
    }
}


