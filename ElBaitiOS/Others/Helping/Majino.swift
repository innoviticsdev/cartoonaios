//
//  Majino.swift
//  Sportoya
//
//  Created by MOHAMED GAMAL on 5/13/19.
//  Copyright © 2019 Innovitics Inc. All rights reserved.
//

import Firebase
import FirebaseRemoteConfig

public class Majino
{
    
    var majorVersion: Int!
    var minorVersion: Int!
    var appStoreUri: String!
    var alertView: Bool!
    let rc = RemoteConfig.remoteConfig()
    
    init(alertView: Bool) {
        self.alertView = alertView
    }
    
    public func check() {
        print("Checking Firebase Configurations")
        fetchFirebaseConfiguration()
        
    }
    
    func getCurrentVersion() -> Int
    {
        return (Bundle.main.infoDictionary!["CFBundleVersion"] as! NSString).integerValue
    }
    
    func fetchFirebaseConfiguration()
    {
        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
        RemoteConfig.remoteConfig().configSettings = debugSettings
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: 0){(status,error) in
            guard error == nil else
            {
                print("Error is \(error)")
                return
            }
            
            print("Retrieved Values")
            RemoteConfig.remoteConfig().activateFetched()
         
            self.majorVersion = (self.rc[MajinoConfig.KEY_UPDATE_MAJOR].stringValue! as NSString).integerValue
            self.minorVersion = (self.rc[MajinoConfig.KEY_UPDATE_MINOR].stringValue! as NSString).integerValue
            self.appStoreUri = self.rc[MajinoConfig.KEY_UPDATE_URL].stringValue!

            print("Major Version is \(self.majorVersion.description)")
            print("Minor Version is \(self.minorVersion.description)")
            
            self.validateVersion()
        }
    }
    
    func validateVersion()
    {
         if(getCurrentVersion() < majorVersion) {
            displayAlert(alertType: .major)
        }
        else if(getCurrentVersion() < minorVersion)
         {
            displayAlert(alertType: .minor)
        }
        else
         {
            print("MAJINO: NO UPDATE DETECTED")
        }
        
    }
    
    
    func openAppStore()
    {
        if let url = URL(string: self.rc[MajinoConfig.KEY_UPDATE_URL].stringValue!),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func displayAlert(alertType: MajinoUpdateType)
    {
        
        let alertView = UIAlertController(title: self.rc[MajinoConfig.KEY_ALERT_MAJOR_TITLE].stringValue!, message: self.rc[MajinoConfig.KEY_ALERT_MAJOR_BODY].stringValue!, preferredStyle: .alert)
        
        let positiveAlertTitle = self.rc[MajinoConfig.KEY_ALERT_POSITIVE].stringValue!
        let negativeAlertTitle = self.rc[MajinoConfig.KEY_ALERT_NEGATIVE].stringValue!
        
        alertView.addAction(UIAlertAction(title: positiveAlertTitle, style: .default, handler: { action in
            
                self.openAppStore()
            
                if(alertType == .major)
                {
                    self.presentView(alertView: alertView)
                }

            }))
        
        if(alertType == .minor)
        {
            alertView.addAction(UIAlertAction(title: negativeAlertTitle, style: .cancel, handler: nil))
        }
        
        presentView(alertView: alertView)
        
       
    }
    
    func presentView(alertView: UIAlertController)
    {
        DispatchQueue.main.async { [] in
            if let topViewController = self.topViewController() {
                topViewController.present(alertView, animated: true) {
                    
                }
            }
        }
    }
    
    func topViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    
}
