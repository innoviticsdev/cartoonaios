//
//  VCExtensions.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/15/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation
import UIKit
import GestureRecognizerClosures

extension UIViewController {
    func setNavigationItem() {
        let imageView = UIImageView(image: UIImage(named: "BlackSearchIcon"))
        imageView.onTap {_ in
            self.present(SearchVC(), animated: true, completion: nil)
        }
        let item = UIBarButtonItem(customView: imageView)
        self.navigationItem.rightBarButtonItem = item
    }
}
