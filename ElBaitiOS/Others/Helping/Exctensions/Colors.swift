//
//  Colors.swift
//  LeCasa
//
//  Created by Alaamaher on 2/18/19.
//  Copyright © 2019 Alaamaher. All rights reserved.
//

import Foundation
import UIKit


struct ColorPalette {
    
    static let mainColor = UIColor(hexString: "#F03C2E")
    static let ButtonColor = UIColor(hexString: "#57AB44")

    static let primaryColor = UIColor(hexString: "188888")
    static let secondaryColor = UIColor(hexString: "F4A03B")
    static let greyColor = UIColor(hexString: "999999")
    static let PlaceholderGrayColor = UIColor(hexString: "757575")
    static let BorderTFColor = UIColor(hexString: "cccccc")
    static let Dot = UIColor(hexString: "c9c9c9c9")
    static let Blue = UIColor(hexString: "3446bc")
    static let Bluegradient1 = UIColor(hexString: "262772") // 8ame2
    static let Bluegradient2 = UIColor(hexString: "3e5cf1") //fath
    static let HomeRedColor = UIColor(hexString: "d44646")
    static let HomeGrayColor = UIColor(hexString: "717171")
    static let GrayTextColor = UIColor(hexString: "383838") //SocialScreen Text
    static let ViewsColorSocial = UIColor(hexString: "f5f5f5")
    static let PlaceHolderColor = UIColor(hexString: "828282") //SocialScreen TextField
    static let CommentsColor = UIColor(hexString: "474747") //SocialScreen
    static let ViewLineGray = UIColor(hexString: "DEDEDE") //SocialScreen
    static let LabelgrayColor = UIColor(hexString: "7a7a7a") //Invitation Screen
    static let BackgroungViewColor = UIColor(hexString: "f7f7f7") //Invitation Screen
    static let FontCartDescription = UIColor(hexString: "A1A9C3")
    static let actionRed = UIColor(hexString: "FC6161")

    
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    
    
}
