//
//  UITextFieldClass.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/18/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

// -MARK: Views
class RoundTextField: TRTextField {

    override func layoutSubviews() {
        super.layoutSubviews()
        borderStyle = .none
        layer.cornerRadius = bounds.height / 7
        layer.borderWidth = 1.0
        layer.borderColor = #colorLiteral(red: 0.8705882353, green: 0.8705882353, blue: 0.8705882353, alpha: 1)
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 1
        layer.masksToBounds = false
        layer.shadowOpacity = 1.0
        layer.backgroundColor = UIColor.white.cgColor
    }
}

