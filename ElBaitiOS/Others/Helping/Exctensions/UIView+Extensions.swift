//
//  Extensions.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/12/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit

extension UIView {

func roundCorners(_ corners: CACornerMask, radius: CGFloat, borderWidth: CGFloat) {
    self.layer.maskedCorners = corners
    self.layer.cornerRadius = radius
    self.layer.borderWidth = borderWidth
    self.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func setUpViewShadow(myView:UIView)
    {
        myView.layer.shadowColor = UIColor(red: 36, green: 126, blue: 245, alpha: 0.36).cgColor
        myView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        myView.layer.shadowOpacity = 1.0
        myView.layer.shadowRadius = 0.0
        myView.layer.masksToBounds = false
    }
    
    func showProgress(isDim : Bool){

        let screenSize = UIScreen.main.bounds

        if(isDim)

        {
            var dimView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))

            dimView.backgroundColor = UIColor.black

            dimView.alpha = 0.30

            dimView.tag = 2

            self.addSubview(dimView)

        }

        

        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.color = .black
        indicator.center = self.center
        indicator.startAnimating()
        indicator.tag = 1
        self.addSubview(indicator)

//        indicator.tag = 1
//
//        indicator.color = .white
//
//        self.addSubview(indicator)
//
//
//
//        indicator.startAnimating()

        

    }

    

    func hideProgress()

    {

        if let viewWithTag = self.viewWithTag(1) {

            viewWithTag.removeFromSuperview()

        }else{

            print("view doesn't exist")

        }

        if let viewWithTag = self.viewWithTag(2) {

            viewWithTag.removeFromSuperview()

        }else{

            print("view doesn't exist")

        }

    }
}

