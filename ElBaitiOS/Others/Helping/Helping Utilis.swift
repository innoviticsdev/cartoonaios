//
//  Helping Utilis.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/13/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import UIKit
import SimpleCheckbox
import NVActivityIndicatorView
import Presentr

// MARK: - Dynamic view height with label
func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
    
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text

    label.sizeToFit()
    print("Your height is \(label.frame.height)")
    return label.frame.height
}

func goToRoot(){
     resetWindow(with: CustomizedTabBarController())
}

func resetWindow(with vc: UIViewController?) {
  if #available(iOS 13.0, *) {
      guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {
          fatalError("could not get scene delegate ")
      }
      sceneDelegate.window?.rootViewController = vc
  } else {
      // Fallback on earlier versions
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          fatalError("could not get app delegate ")
      }
      appDelegate.window?.rootViewController = vc
  }
}

func setupCVStartCellDirection(myCollectionView: UICollectionView){
    if (myCollectionView.numberOfItems(inSection: 0) > 0)  {
         let indexPath = NSIndexPath(item: 0 , section: 0)
        myCollectionView.scrollToItem(at: indexPath as IndexPath, at: .left, animated: false)
    }
}

let bottomHalfPresenter: Presentr = Presentr(presentationType: .bottomHalf)

func presentBottomPopup(view: UIViewController ,popupVC: UIViewController){
    if #available(iOS 13.0, *) {
        view.present(popupVC, animated: true, completion: nil)
    } else {
        view.customPresentViewController(bottomHalfPresenter, viewController: popupVC, animated: true, completion: nil)
         }
}

//To fix card swipe will loading in ios 13
func toggleCardSwipe(viewController : UIViewController, allowSwipe: Bool){
    if #available(iOS 13.0, *) {
        if(allowSwipe){
            viewController.isModalInPresentation = false
        }else{
            viewController.isModalInPresentation = true
        }
    }
}

func saveToken(token: String){
    UserDefaults.standard.set(token, forKey: "Token")
}

func setupCheckbox(checkBox: Checkbox, isChecked: Bool, isEnabled: Bool){
    checkBox.checkedBorderColor = #colorLiteral(red: 0.9568627451, green: 0.6274509804, blue: 0.231372549, alpha: 1)
    checkBox.uncheckedBorderColor = #colorLiteral(red: 0.631372549, green: 0.662745098, blue: 0.7647058824, alpha: 1)
    checkBox.borderStyle = .circle
    checkBox.checkmarkColor = #colorLiteral(red: 0.9568627451, green: 0.6274509804, blue: 0.231372549, alpha: 1)
    checkBox.checkmarkStyle = .circle
    checkBox.useHapticFeedback = true
    checkBox.isChecked = isChecked
    checkBox.isEnabled = isEnabled
}
func saveUserData(userObject: User){
    let defaults: UserDefaults = UserDefaults.standard
     
      let encoder = JSONEncoder()
      if let encoded = try? encoder.encode(userObject) {
          let defaults = UserDefaults.standard
          defaults.set(encoded, forKey: "UserData")
      }
      defaults.set(true, forKey: "LoggedIn")
      defaults.synchronize()
}



func checkIfSignedIn() -> Bool {
   let defaults: UserDefaults = UserDefaults.standard
   let LoginFlag = defaults.bool(forKey: "LoggedIn")
   return LoginFlag
}
func makeRoundedButtonBorder(btn: UIButton){
    btn.backgroundColor = UIColor.clear
    btn.layer.cornerRadius = 8
    btn.layer.borderWidth = 1
    btn.layer.borderColor = ColorPalette.ButtonColor.cgColor
}

func roundedViewWithShadow(view: UIView){
    view.layer.shadowColor = UIColor.gray.cgColor
    view.layer.shadowOpacity = 0.5
    view.layer.shadowOffset = .zero
    view.layer.shadowRadius = 2
    view.layer.cornerRadius = 10
}

func addShadowtoView(view: UIView){
    view.layer.shadowColor = UIColor.gray.cgColor
    view.layer.shadowOpacity = 0.5
    view.layer.shadowOffset = .zero
    view.layer.shadowRadius = 2
}

func makeCircleImgView(imgView: UIImageView){
    imgView.layer.borderWidth = 1.0
    imgView.layer.masksToBounds = false
    imgView.layer.borderColor = UIColor.white.cgColor
    imgView.layer.cornerRadius = imgView.frame.size.width / 2
    imgView.clipsToBounds = true
    
}

// MARK: - Indicator
enum IndicatorSwitch {
    case on
    case off
}

var activityIndicators = Dictionary<UIView, NVActivityIndicatorView>()

func indicatorSwitch(status: IndicatorSwitch,view: UIView) {
    switch status {
    //Show Indicator
    case .on:
        //Add Indicator
        activityIndicators[view] = NVActivityIndicatorView(frame: CGRect(x: 50, y: 0, width: 70, height: 70),type: .ballScaleMultiple)
        activityIndicators[view] = showActivityIndicator(view: view)
        view.isUserInteractionEnabled = false
        break
        
    default:
        //Hide Indicator
        //remove from indicator array
        removeActivityIndicator(view: view)
        view.isUserInteractionEnabled = true
        break
    }
}

func showActivityIndicator(view: UIView) -> NVActivityIndicatorView{
    
    if let spinnerExisted = view.viewWithTag(0xAAAAAAAA) {
        return spinnerExisted as! NVActivityIndicatorView
    }

    let spinner = NVActivityIndicatorView(frame: CGRect(x: 50, y: 0, width: 70, height: 70),type: .ballScaleMultiple)

    spinner.color = ColorPalette.mainColor
    spinner.translatesAutoresizingMaskIntoConstraints = false
    spinner.startAnimating()
    view.addSubview(spinner)
    
    spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    spinner.tag = 0xAAAAAAAA
    return spinner
}

func removeActivityIndicator(view: UIView){
    if(activityIndicators[view] != nil)
    {
        activityIndicators[view]!.stopAnimating()
        activityIndicators[view]!.removeFromSuperview()
        activityIndicators.removeValue(forKey: view)
    }
    
}



func transformCollectionView(collectionView: UICollectionView)
{
    if (Localize.getLocal() == "ar")
    { collectionView.transform = CGAffineTransform(scaleX: -1, y: 1) }
}

// -MARK: Views

func setupTextField(textField: RoundTextField) {
    if(Localize.getLocal() == "en"){
        textField.leftView = UIView(frame: CGRect(x: 0,y: 0,width: 16,height: 0))
        textField.leftViewMode = .always
    }else{
        textField.rightView = UIView(frame: CGRect(x: 0,y: 0,width: 16,height: 0))
        textField.rightViewMode = .always
    }
}

func makeRoundedCorners(view: UIView){
    view.layer.cornerRadius = 20.0
    view.clipsToBounds = true
    view.frame = view.frame.inset(by: UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0))
}

func showErrorAlert(title: String, errorMessage: String) -> UIAlertController {
    let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
    let action = UIAlertAction(title: "ok", style: .default)
    alert.addAction(action)
    return alert
}

func showToast(message : String, view: UIView) {
    
    let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 100, y: view.frame.size.height-100, width: 200, height: 35))
    toastLabel.backgroundColor = #colorLiteral(red: 0.2168715205, green: 0.2174777687, blue: 0.2421023348, alpha: 1).withAlphaComponent(1.0)
    toastLabel.textColor = UIColor.white
    toastLabel.textAlignment = .center;
    toastLabel.numberOfLines = 0
    toastLabel.font = UIFont(name: "Optima", size: 16.0)
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.adjustsFontSizeToFitWidth = true
    view.addSubview(toastLabel)
    UIView.animate(withDuration: 6.0, delay: 0.1, options: .curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
}

    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

let emailRegEx = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

func validateRegex(inputRegix: String, regixFormat: String) -> Bool{
    let regixPred =  NSPredicate(format:"SELF MATCHES %@", regixFormat)
    return regixPred.evaluate(with: inputRegix)
}

