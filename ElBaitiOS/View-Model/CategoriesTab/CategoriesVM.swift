//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class CategoriesVM {
    
    var apiService: RestApiManager!
    
    var arrCategories: [CategoryData] = [CategoryData]()
    var categoryParam = [String:String]()
    
    var currentPage = 1
    var lastPage = 1

    // callback for interfaces
    var state: PaginatedState = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?

   
    
    func initFetch(refreshType : RefreshType) {
        
        self.categoryParam = ["page":self.currentPage.description]
        apiService = RestApiManager(request: Router.getCategories(parameters: categoryParam))
        apiService.TestingEnabled = true

        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
            
            if(refreshType == .refresh)
                   {
                       self.arrCategories.removeAll()
                       self.currentPage = 1
                   }
                   else
                   {
                       if(self.currentPage > (self.lastPage))
                       {
                        self.state = .noMore
                           return;
                       }
                   }
                
            if success{
            do
             {
                 let categoryResult = try JSONDecoder().decode(CategoryModel.self, from: JSONDATA.rawData())
                 if(categoryResult.status.code == 200)
                   {
                     if(categoryResult.data != nil && categoryResult.data!.count > 0)
                     {
                        self.lastPage = (categoryResult.meta.lastPage)
                         if(refreshType == .refresh)
                         {
                             self.arrCategories = categoryResult.data!
                             self.currentPage += 1
                         }
                         else
                         {
                            if(self.currentPage <= (categoryResult.meta.lastPage))
                             {
                                 self.arrCategories += categoryResult.data!
                                 self.currentPage += 1
                             }
                             else
                             {
                                 self.state = .noMore
                             }
                         }
                         self.state = .populated
                     }
                        else
                        {
                            self.state = .empty
                        }
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = categoryResult.status.message
                    return
                   }
             }
             catch{
                //fatal error
                self.state = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.state = .error
            }
        }
        )
    }
}
