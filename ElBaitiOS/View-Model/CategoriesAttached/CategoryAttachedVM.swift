//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class CategoryAttachedVM {
    
    var categoryParam = [String:String]()
    var filterParam = [String:String]()
    var apiService: RestApiManager!
    var currentPage = 1, categoryId = "0"
    var lastPage = 1
    var arrCategoriesAttached: [CategoriesAttachedData] = [CategoriesAttachedData]()
    

    // callback for interfaces
    var state: PaginatedState = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
   
    
    func initFetch(refreshType : RefreshType,isFilter : Bool) {
        
        if(isFilter){
            self.categoryParam = filterParam
        }
        else{
             self.categoryParam = ["category_id":self.categoryId.description,"page":self.currentPage.description]
        }
       
        apiService = RestApiManager(request: Router.getProductsAttachedToCategory(parameters: categoryParam))
        apiService.TestingEnabled = true
        
        state = .loading
        
        
        
        if(refreshType == .refresh)
        {
            self.arrCategoriesAttached.removeAll()
            self.currentPage = 1
        }
        else
        {
            if(self.currentPage > (self.lastPage))
            {
                state = .noMore
                return;
            }
        }
        
        
        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
                 let categoryResult = try JSONDecoder().decode(CategoryAttachedModel.self, from: JSONDATA.rawData())
                if(categoryResult.status.code == 200)
                   {
                        if(categoryResult.data != nil && categoryResult.data!.count > 0)
                        {
                            self.lastPage = (categoryResult.meta?.lastPage)!
                            if(refreshType == .refresh)
                            {
                                self.arrCategoriesAttached = categoryResult.data!
                                self.currentPage += 1
                            }
                            else
                            {
                                if(self.currentPage <= (categoryResult.meta?.lastPage)!)
                                {
                                    self.arrCategoriesAttached += categoryResult.data!
                                    self.currentPage += 1
                                }
                                else
                                {
                                    self.state = .noMore
                                }
                            }
                            self.state = .populated
                        }
                    else
                        {
                            self.state = .empty
                    }
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = categoryResult.status.message
                    return
                   }
             }
             catch{
                print(error)
                self.state = .error
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.state = .error
            }
        }
        )
    }
}
