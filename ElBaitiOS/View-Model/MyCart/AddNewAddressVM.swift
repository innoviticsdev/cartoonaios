//
//  AddNewAddressVM.swift
//  ElBaitiOS
//
//  Created by Mohamed El Sawi on 8/28/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

class AddNewAddressVM {
    
    var longitude: CLLocationDegrees?
    var latitude: CLLocationDegrees?
    var locationAddrss: String?
    var locationCity: String?
    var locationCountryName: String?
    var locationServicePermissionStatus: Bool {
        get{
            return self.getLocationManagerStatus()
        }
    }
    
    // callback for interfaces
      var state: State = .empty {
          didSet {
              self.updateLoadingStatus?()
          }
      }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var reloadTableViewClosure: (()->())?
     var showAlertClosure: (()->())?
     var updateLoadingStatus: (()->())?
    
    func mapView(mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
            longitude = position.target.longitude
            latitude = position.target.latitude
            
            //Responsible for converting coordinates to a stree/location address using GooglePlaces API
            GMSGeocoder().reverseGeocodeCoordinate(CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude).coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    print(address.description)
                    self.locationAddrss = lines[0]
                    self.locationCity = address.administrativeArea
                    self.locationCountryName = address.country
                    self.state = .populated
                }
            }
    }
    
    private func getLocationManagerStatus() -> Bool
      {
         if(CLLocationManager.locationServicesEnabled())
         {
             let status = CLLocationManager.authorizationStatus()
             
             switch status
             {
               case .restricted ,.denied:
                    print("No access")
                    return false
               case .notDetermined:
                    print("asking for access...")
                    return false
             case .authorizedAlways , .authorizedWhenInUse:
                    print("Access")
                    return true
             @unknown default:
                    return false
            }
         }
         else
         {
             print("Location services are not enabled")
             return false
         }
    }
   
}
