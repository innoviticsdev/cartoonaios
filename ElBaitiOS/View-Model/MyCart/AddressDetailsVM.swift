
//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class AddressDetailsVM {
    
    
    var apiService: RestApiManager!
    
    var addressDetailsParam = [String:String]()
    

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    func ValidateInputs() {
        
    }
    
    func saveAddressAPI() {

        apiService = RestApiManager(request: Router.SaveAddress(parameters: addressDetailsParam))
        apiService.TestingEnabled = true
        
        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let myCartResult = try JSONDecoder().decode(AddressDetailsModel.self, from: JSONDATA.rawData())
                 if(myCartResult.status.code == 200)
                   {
                       print("Reload")
                  
                      self.state = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = myCartResult.status.message
                    return
                   }
             }
             catch{
                self.state = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
            }
        }
        )
    }
}

