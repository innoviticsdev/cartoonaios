//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class MyCartVM {
    
    
    var apiService: RestApiManager!
    
    var ItemsArray: [Item] = [Item]()
    var myCartData: MyCartData!
    var removeItemParam = [String:String] ()
    var updateCartParam = [String:String] ()
    var copoun: String?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
 
    
    func initFetch() {
            if(!checkIfSignedIn())
            {
                return
            }
        
            apiService = RestApiManager(request: Router.getMyCart)
            apiService.TestingEnabled = true
            
            state = .loading

            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in
                
                guard let self = self else {
                                return
                            }
                    
                if success{
                do
                 {
                  
                     let myCartResult = try JSONDecoder().decode(MyCartProductModel.self, from: JSONDATA.rawData())
                     if(myCartResult.status.code == 200)
                       {
                        self.myCartData = myCartResult.data
                        if(self.myCartData != nil)
                        {
                           print("Reload")
                            self.ItemsArray = myCartResult.data!.items
                            self.ItemsArray.count > 0 ? (self.state = .populated) : (self.state = .empty)
                            print("couponCode is \(myCartResult.data?.couponCode)")
                            self.copoun = myCartResult.data?.couponCode
                        }
                        else
                        {
                            self.ItemsArray.removeAll()
                            self.state = .empty
                        }
                       }
                
                        // if codeStatus != 200
                       else{
                        //show error msg
                        
                        self.switchState(to: .error)
                        self.alertMessage = myCartResult.status.message
                        return
                       }
                 }
                 catch{
                    self.switchState(to: .error)
                    print(error)
                 }
                }
                // if connection failed
                else {
                    // show connection failed
                    self.switchState(to: .error)
                }
            }
            )
    }
    
    func updateCartAPI() {
            if(!checkIfSignedIn())
            {
                return
            }
        
            apiService = RestApiManager(request: Router.updateItem(parameters: updateCartParam))
            apiService.TestingEnabled = true
            
            state = .loading

            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in
                
                guard let self = self else {
                                return
                            }
                    
                if success{
                do
                 {
                  
                     let myCartResult = try JSONDecoder().decode(MyCartProductModel.self, from: JSONDATA.rawData())
                     if(myCartResult.status.code == 200)
                       {
                        if(myCartResult.data != nil)
                        {
                           print("Reload")
                            self.ItemsArray = myCartResult.data!.items
                            self.myCartData = myCartResult.data
                            self.ItemsArray.count > 0 ? (self.state = .populated) : (self.state = .empty)
                        }
                        
                        else
                        {
                            self.state = .empty
                        }
                       }
                    
                        else if(myCartResult.status.code == 410){
                        self.state = .populated
                        self.alertMessage = myCartResult.status.message
                        }
                        // if codeStatus != 200
                       else{
                        //show error msg
                        
                        self.switchState(to: .error)
                        self.alertMessage = myCartResult.status.message
                        return
                       }
                 }
                 catch{
                    self.switchState(to: .error)
                    print(error)
                 }
                }
                // if connection failed
                else {
                    // show connection failed
                    self.switchState(to: .error)
                }
            }
            )
    }
    
    func fetchRemoveItem() {
           
            apiService = RestApiManager(request: Router.removeItem(parameters: removeItemParam))
            apiService.TestingEnabled = true
            
            state = .loading

            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in
                
                guard let self = self else {
                                return
                            }
                    
                if success{
                do
                 {
                  
                     let myCartResult = try JSONDecoder().decode(RemoveItemModel.self, from: JSONDATA.rawData())
                     if(myCartResult.status.code == 200)
                       {
                        self.initFetch()
//                        self.ItemsArray = myCartResult.data!.items
//                        self.myCartData = myCartResult.data
//                        self.ItemsArray.count > 0 ? (self.state = .populated) : (self.state = .empty)
                         
                       }
                        // if codeStatus != 200
                       else{
                        //show error msg
                        
                        self.switchState(to: .error)
                        self.alertMessage = myCartResult.status.message
                        return
                       }
                 }
                 catch{
                    self.switchState(to: .error)
                    print(error)
                 }
                }
                // if connection failed
                else {
                    // show connection failed
                    self.switchState(to: .error)
                }
            }
            )
    }
    
    func switchState(to: State)
    {
        if(to == .error)
        {
            ItemsArray.removeAll()
            self.state = .error

        }
    }
}

