//
//  Rating.swift
//  ElBaitiOS
//
//  Created by mac on 9/21/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class RatingVM {
    
    
    var apiService: RestApiManager!
   
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    init(){
        
    }

    
    func submitOrder(dic : [String : Any]) {

            state = .loading
          
        apiService = RestApiManager(request: Router.submitReview(parameters: dic ))
            
            apiService.TestingEnabled = true

            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let submitReviewModel = try JSONDecoder().decode(SubmitReviewModel.self, from: JSONDATA.rawData())

                    if(submitReviewModel.status.code == 200)
                       {
                        print("Reload")
                        // logic success here..
                        print("populated message is  : \(submitReviewModel.status.message)")
                        self.state = .populated
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RatingBroadcast), object: nil)
                        
                       }

                        // if codeStatus != 200
                       else{
                        print("error message is  : \(submitReviewModel.status.message)")
                        self.alertMessage = "\(submitReviewModel.status.message)"
                        self.state = .error
                        
                        return
                       }
                 }catch{
                    print(error)
                    self.alertMessage = "\(error)"
                    self.state = .error
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.state = .error
                }
            }
          )
    }
}

