//
//  UpdateProfileVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation
import Firebase

enum UpdateMyProfile {
    case edit
    case password
    case number
}

class UpdateProfileVM {
    
    var param1 : String!
    var param2 : String!
    var param3 : Data!
    var apiService: RestApiManager!
    var brokenRules: [SignUpBrokenRule] = [SignUpBrokenRule]()
    
    func isValid(updateType : UpdateMyProfile) -> Bool{
        self.brokenRules = [SignUpBrokenRule]()
        self.validate(updateType: updateType)
        return self.brokenRules.count == 0 ? true : false
    }
    
//    var isValid: Bool{
//        get{
//            self.brokenRules = [SignUpBrokenRule]()
//            self.validate()
//            return self.brokenRules.count == 0 ? true : false
//        }
//    }

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?

    init(){
        
    }

    func updateProfile(updateType : UpdateMyProfile) {

        state = .loading
        
        switch updateType {
        case .edit:
            apiService = RestApiManager(request: Router.updateProfiles(parameters:
                ["first_name": param1!,
                 "last_name": param2!,"image": param3!]))
            break
            
        case .password:
            apiService = RestApiManager(request: Router.updateProfiles(parameters:
            ["old_password": param1!,
             "password": param2!]))
            break
            
        case .number:
            apiService = RestApiManager(request: Router.updateProfiles(parameters:
            ["country_code": param1!,
             "phone": param2!]))
            break
        }
        
        apiService.TestingEnabled = true
        
        if(updateType == .edit){
            var mFormConfig : MultiFormManager!
            mFormConfig  = MultiFormManager(fileName:  "image.png", name: "image", mimeType: "image/png", data: param3!)
            
            apiService.Request(with: mFormConfig,completionHandler: {
                
                (success,JSONDATA,status) -> Void in
                
                
                
                
                if success {
                    
                   do{
                        let exploreResult = try JSONDecoder().decode(UpdateProfileModel.self, from: JSONDATA.rawData())

                       if(exploreResult.status.code == 200)
                          {
                              print("Reload")
                           // logic success here..
                           let token = exploreResult.data!.token
                           saveToken(token: token)

                           if let myData = exploreResult.data{
                               saveUserData(userObject: myData.user)
                           }
                            
                            // lllllll
                            profileNotificationCtr.post(name: NSNotification.Name(rawValue: profileBroadcast), object: self)
                           
                           //saveUserData(userObject: exploreResult.data!.user)
                             self.state = .populated
                          }

                           // if codeStatus != 200
                          else{
                           print("codeStatus != 200")
                           self.state = .error
                           self.alertMessage = exploreResult.status.message
                           return
                          }
                    }catch{
                       print(error)
                       //fatalError("Failed to serialize JSON")
                    }
                    
                }
                    
                else
                    
                {
                    
                    print("Faild")
                    
                    
                    
                    
                    
                }
                
                
                
            }, param: ["first_name": param1!,
            "last_name": param2!,"image": param3!])
        }
        
        
        else{
       // if(ReachabilityNetwork.getReachabilityInfo()){
         //   isInitRequest = true
                
        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in

            guard let self = self else { return }

            if success{
            do{
                 let exploreResult = try JSONDecoder().decode(UpdateProfileModel.self, from: JSONDATA.rawData())

                if(exploreResult.status.code == 200)
                   {
                       print("Reload")
                    // logic success here..
                    let token = exploreResult.data!.token
                    saveToken(token: token)

                    if let myData = exploreResult.data{
                        saveUserData(userObject: myData.user)
                    }
                    
                    //saveUserData(userObject: exploreResult.data!.user)
                      self.state = .populated
                   }

                    // if codeStatus != 200
                   else{
                    print("codeStatus != 200")
                    self.state = .error
                    self.alertMessage = exploreResult.status.message
                    return
                   }
             }catch{
                print(error)
                //fatalError("Failed to serialize JSON")
             }
            }
            // if connection failed
            else {
                self.state = .error
                self.alertMessage = "connection failed"
            }
        }
      )
        }
//}
}
    deinit {
        profileNotificationCtr.removeObserver(self)
    }
}


extension UpdateProfileVM{

    private func validate(updateType : UpdateMyProfile)
    {
        switch updateType {
        case .edit:
            self.validateUsername(name: param1, type: "First name")
            self.validateUsername(name: param2, type: "Last name")
            break
            
        case .number:
            validatePhone()
            break
            
        case .password:
        self.validatePassword(txtPassword: param1)
        self.validatePassword(txtPassword: param2)
        break
            
        }
        
    }

    private func validateUsername(name: String, type: String){
        if (name.isEmpty){
            self.brokenRules.append(SignUpBrokenRule(name:type,message:"\(type) must be provided."))
        }
        else
        {
            let usernameRegEX = "^(?=.{4,20}$)(?![0-9._])(?!.*[_.]{2})[a-zA-Z0-9._\\u0621-\\u064A ]+(?<![_.])$";
            let usernamePred = NSPredicate(format:"SELF MATCHES %@",usernameRegEX )
            if (!usernamePred.evaluate(with: name))
            {
                self.brokenRules.append(SignUpBrokenRule(name:"Username",
                                                         message:"\(type) must start of letter and be between 4 and 20 characters."))
            }
        }
    }
    
    private func validatePassword(txtPassword: String){
        if(txtPassword.isEmpty || txtPassword.count<6)
        {
            self.brokenRules.append(SignUpBrokenRule(name:"Password",message:"The password must be 6 characters long or more."))
        }
    }



    private func validatePhone()
    {
        if(self.param2.isEmpty)
        {
            self.brokenRules.append(SignUpBrokenRule(name:"phone",message: "Please enter your phone number."))
        }else {
            let phoneRegex = "^[1]\\d{9}$"
            let phonePred = NSPredicate(format:"SELF MATCHES %@",phoneRegex)
            if (!phonePred.evaluate(with: param2))
            {
                self.brokenRules.append(SignUpBrokenRule(name:"phone",message: "Phone number is badly formatted."))
            }
        }
    }
    
    func verifyPhone(phoneNumber: String,completion: @escaping(Bool?, Error?)-> Void) {

        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber , uiDelegate:nil ){

            (verificationId, error) in

            if error == nil{
                guard let verifyId = verificationId else {return}
                self.saveVerificationID(verifyId: verifyId)
                
                completion(true,nil)
            }else{
                completion(false,error)
                print("unable to get secret verification code from Firebase")
            }
        }
    }
    
    
    /* if error == nil{
               notificationCtr.post(name: NSNotification.Name(rawValue: broadcast), object: self)
               completion(true,nil)
           }else{
               completion(false,error)
               }
           }
       }

       deinit {
           notificationCtr.removeObserver(self)
       }*/
    
    func saveVerificationID(verifyId: String) {
        UserDefaults.standard.set(verifyId, forKey: "verificationId")
        UserDefaults.standard.synchronize()
    }
    
}

/*func addPost(_ param: [String:AnyObject],imageData: Data,imageName: String )
{
    self.view.showProgress(isDim: true)
    let multipartEncoding: (MultipartFormData) -> Void = { multipartFormData in


    


        
        multipartFormData.append(imageData, withName: "img", fileName: imageName, mimeType: "image/jpeg")


        


        
        for (key, value) in param {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
        }
    }


    
    let request = Router.AddPost(parameters: param)


    
    Alamofire.upload(
        multipartFormData: multipartEncoding,
        with: request,
        encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let request, _, _):
                request.responseJSON(completionHandler: { (dataResponse) in
                    print(dataResponse.data as Any)
                    if dataResponse.result.error != nil {


                   


                        
                    }
                    else
                    {
                        print(dataResponse)
                        print(dataResponse.request!)  // original URL request
                        print(dataResponse.request?.httpBody ?? "Undefined httpBody")
                        print(dataResponse.result.value ?? "Undefined resultValue")
                        print(dataResponse.response)


                        
                        self.view.hideProgress()
                        self.navigationController?.popViewController(animated: true)
                    }


                    
                })
            case .failure(_):
                self.view.hideProgress()
                print("Image Upload Failure")
                break


                
            }
    })
}*/
