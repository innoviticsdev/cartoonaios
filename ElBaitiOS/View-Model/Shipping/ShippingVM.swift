//
//  ShippingVM.swift
//  ElBaitiOS
//
//  Created by mac on 8/29/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class ShippingVM {
    
    
    var apiService: RestApiManager!
    var arrAddress: [AddressDetails] = [AddressDetails]()
    var message: String = ""
    
    var myCartData : MyCartData?
    var perorderModel : PerorderModel?
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var addCouponState: State = .empty {
        didSet {
            self.updateCouponStatus?()
        }
    }

    var removeCouponState: State = .empty {
        didSet {
            self.updateRemoveCouponStatus?()
        }
    }
    
    var prepareState: State = .empty {
        didSet {
            self.updatePrepareOrderStatus?()
        }
    }
    
    var RemoveState: State = .empty {
        didSet {
            self.removeAddressStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadViewClosure: (()->())?
    var showAlertClosure: (()->())?
    
    var updateLoadingStatus: (()->())?
    
    var updateCouponStatus: (()->())?
    
    var updateRemoveCouponStatus: (()->())?

    var updatePrepareOrderStatus: (()->())?
    
    var removeAddressStatus: (()->())?

    
    init(){
        
    }

    func getAddresses() {

        state = .loading
      
        apiService = RestApiManager(request: Router.getAdressesList)
        
        apiService.TestingEnabled = true
       // if(ReachabilityNetwork.getReachabilityInfo()){
         //   isInitRequest = true
                
        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in

            guard let self = self else { return }

            if success{
            do{
                 let addressesResult = try JSONDecoder().decode(AddressModel.self, from: JSONDATA.rawData())

                if(addressesResult.status.code == 200)
                   {
                    print("Reload")
                    
                    self.arrAddress.removeAll()
                    self.arrAddress = addressesResult.data
                    self.state = .populated
                   }

                    // if codeStatus != 200
                   else{
                    print("codeStatus != 200")
                    self.state = .error
                    self.alertMessage = addressesResult.status.message
                    return
                   }
             }catch{
                print(error)
                }
            }
            // if connection failed
            else {
                self.state = .error
                self.alertMessage = "connection failed"
            }
        }
      )

//}
}
    func removeAddress(addressID: String) {

            addCouponState = .loading
          
            apiService = RestApiManager(request: Router.removeAddress(parameters: ["id": addressID]))
            
            apiService.TestingEnabled = true
           // if(ReachabilityNetwork.getReachabilityInfo()){
             //   isInitRequest = true
                    
            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let couponResult = try JSONDecoder().decode(CouponModel.self, from: JSONDATA.rawData())

                    if(couponResult.status.code == 200)
                       {
                           print("Reload")
                        // logic success here..
                        print("populated message is  : \(couponResult.status.message)")
                        //self.arrAddress = addressesResult.data
                        self.RemoveState = .populated
                        
                       }

                        // if codeStatus != 200
                       else{
                        print("error message is  : \(couponResult.status.message)")
                        self.RemoveState = .error
                        self.message = couponResult.status.message
                        
                        //self.alertMessage = addressesResult.status.message
                        return
                       }
                 }catch{
                    print(error)
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.RemoveState = .error
                    self.alertMessage = "connection failed"
                }
            }
          )

    //}
    }
    func getCoupon(promoCode: String) {

            addCouponState = .loading
          
            apiService = RestApiManager(request: Router.getCoupon(parameters: ["code": promoCode]))
            
            apiService.TestingEnabled = true
           // if(ReachabilityNetwork.getReachabilityInfo()){
             //   isInitRequest = true
                    
            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let couponResult = try JSONDecoder().decode(CouponModel.self, from: JSONDATA.rawData())

                    if(couponResult.status.code == 200)
                       {
                           print("Reload")
                        // logic success here..
                        print("populated message is  : \(couponResult.status.message)")
                        //self.arrAddress = addressesResult.data
                        self.addCouponState = .populated
                        self.message = couponResult.status.message
                        
                        self.myCartData = couponResult.data
                       }

                        // if codeStatus != 200
                       else{
                        print("error message is  : \(couponResult.status.message)")
                        self.addCouponState = .error
                        self.message = couponResult.status.message
                        
                        //self.alertMessage = addressesResult.status.message
                        return
                       }
                 }catch{
                    print(error)
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.addCouponState = .error
                    self.alertMessage = "connection failed"
                }
            }
          )

    //}
    }
    
    func removeCoupon() {

        removeCouponState = .loading
          
        apiService = RestApiManager(request: Router.removeCoupon)
            
            apiService.TestingEnabled = true
           // if(ReachabilityNetwork.getReachabilityInfo()){
             //   isInitRequest = true
                    
            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let couponResult = try JSONDecoder().decode(CouponModel.self, from: JSONDATA.rawData())

                    if(couponResult.status.code == 200)
                       {
                           print("Reload")
                        // logic success here..
                        print("message is  : \(couponResult.status.message)")
                        //self.arrAddress = addressesResult.data
                        self.myCartData = couponResult.data
                        self.removeCouponState = .populated
                        self.message = couponResult.status.message
                        
                       }

                        // if codeStatus != 200
                       else{
                        print("message is  : \(couponResult.status.message)")
                        self.removeCouponState = .error
                        self.message = couponResult.status.message
                        
                        
                        //self.alertMessage = addressesResult.status.message
                        return
                       }
                 }catch{
                    print(error)
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.removeCouponState = .error
                    self.alertMessage = "connection failed"
                }
            }
          )

    //}
    }
    
    func prepareOrder(address: String, addressId: Int){

            prepareState = .loading
          
            apiService = RestApiManager(request: Router.prepareOrder(
                parameters:
                ["payment[method]": "cashondelivery",
                "billing[address1][0]": address,
                "billing[use_for_shipping]": "true",
                "billing[address_id]": "\(addressId)",
                "shipping[address1][0]": address]
            ))
            
            apiService.TestingEnabled = true
           // if(ReachabilityNetwork.getReachabilityInfo()){
             //   isInitRequest = true
                    
            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let preorderResult = try JSONDecoder().decode(PerorderModel.self, from: JSONDATA.rawData())

                    if(preorderResult.status.code == 200)
                       {
                           print("Reload")
                        // logic success here..
                        print("message is  : \(preorderResult.status.message)")

                        //*****************************************************************/
                        self.perorderModel = preorderResult
                        self.prepareState = .populated
                       }

                        // if codeStatus != 200
                       else{
                        print("message is  : \(preorderResult.status.message)")
                       // self.message = preorderResult.status.message
                        self.prepareState = .error
                        
                        //self.alertMessage = addressesResult.status.message
                        return
                       }
                 }catch{
                    print(error)
                    self.prepareState = .error
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.prepareState = .error
                    self.alertMessage = "connection failed"
                }
            }
          )

    //}
    }
}


