//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class WishlistVM {
    
    var apiService: RestApiManager!
    
    var arrWishlist: [WishlistData] = [WishlistData]()
    var moveParam = [String:String]()
    var removeParam = [String:String]()
    
    var currentPage = 1
    var lastPage = 1

    // callback for interfaces
    var state: PaginatedState = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var Movestate: State = .empty {
        didSet {
            self.moveCartLoadingStatus?()
        }
    }
    
  
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var moveCartLoadingStatus: (()->())?

    
    func moveToCartAPI() {

        apiService = RestApiManager(request: Router.moveToCart(parameters: moveParam))
        apiService.TestingEnabled = true
        
        Movestate = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let moveToResult = try JSONDecoder().decode(MoveToCartModel.self, from: JSONDATA.rawData())
                 if(moveToResult.status.code == 200)
                   {
                   
                    self.Movestate = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.Movestate = .error
                    self.alertMessage = moveToResult.status.message
                    return
                   }
             }
             catch {
                self.Movestate = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.Movestate = .error
            }
        }
        )
    }
    
    func addRemoveToWishlistAPI() {

        apiService = RestApiManager(request: Router.AddRemoveWishlist(parameters: removeParam))
        apiService.TestingEnabled = true
        
        Movestate = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let moveToResult = try JSONDecoder().decode(MoveToCartModel.self, from: JSONDATA.rawData())
                 if(moveToResult.status.code == 200)
                   {
                   
                    self.Movestate = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.Movestate = .error
                    self.alertMessage = moveToResult.status.message
                    return
                   }
             }
             catch {
                self.Movestate = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.Movestate = .error
            }
        }
        )
    }

   
    
    func initFetch(refreshType : RefreshType) {
        
   
        apiService = RestApiManager(request: Router.listWishlist)
        apiService.TestingEnabled = true

        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
            
            if(refreshType == .refresh)
                   {
                       self.arrWishlist.removeAll()
                       self.currentPage = 1
                   }
                   else
                   {
                       if(self.currentPage > (self.lastPage))
                       {
                        self.state = .noMore
                           return;
                       }
                   }
                
            if success{
            do
             {
                 let wishlistResult = try JSONDecoder().decode(WishlistModel.self, from: JSONDATA.rawData())
                 if(wishlistResult.status.code == 200)
                   {
                    if(wishlistResult.data != nil && wishlistResult.data.count > 0)
                     {
                        self.lastPage = (wishlistResult.meta.lastPage)
                         if(refreshType == .refresh)
                         {
                            self.arrWishlist = wishlistResult.data
                             self.currentPage += 1
                         }
                         else
                         {
                            if(self.currentPage <= (wishlistResult.meta.lastPage))
                             {
                                self.arrWishlist += wishlistResult.data
                                 self.currentPage += 1
                             }
                             else
                             {
                                 self.state = .noMore
                             }
                         }
                         self.state = .populated
                     }
                        else
                        {
                            self.state = .empty
                        }
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = wishlistResult.status.message
                    return
                   }
             }
             catch{
                //fatal error
                self.state = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.state = .error
            }
        }
        )
    }
}
