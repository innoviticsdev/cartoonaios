//
//  Operators.swift
//  ElBaitiOS
//
//  Created by Mohamed El Sawi on 9/1/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

public enum Operators {
    case increment
    case decrement
}
