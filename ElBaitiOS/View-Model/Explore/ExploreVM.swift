//
//  ExploreVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/18/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class ExploreVM {
    
    let apiService: RestApiManager

    var arrCategories: [Category] = [Category]()
    var arrDeals: [Deal] = [Deal]()
    var arrBestSellers: [BestSeller] = [BestSeller]()
    var arrElectronics: [BestSeller] = [BestSeller]()
    var arrFashion: [BestSeller] = [BestSeller]()
    var isInitRequest: Bool = false
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?

    init( apiService: RestApiManager = RestApiManager(request: Router.getExplore)) {
        self.apiService = apiService
        apiService.TestingEnabled = true
    }
    
    func initFetch() {

        state = .loading
        if(ReachabilityNetwork.getReachabilityInfo()){
            isInitRequest = true
        
        
        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                return
                
            }
                
            if success{
            do{
                 let exploreResult = try JSONDecoder().decode(ExploreModel.self, from: JSONDATA.rawData())
                
                 if(exploreResult.status.code == 200)
                   {
                       print("Reload")
                    self.arrCategories = exploreResult.data.categories
                    self.arrDeals = exploreResult.data.deals
                    self.arrBestSellers = exploreResult.data.bestSellers
                    self.arrElectronics = exploreResult.data.electronics
                    self.arrFashion = exploreResult.data.fashion
                    
                    print("Hello from VM --> count is \(self.arrCategories.count)")
                            
                      self.state = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    self.state = .error
                    self.alertMessage = exploreResult.status.message
                    return
                   }
             }catch{
                print(error)

                self.state = .error
                self.alertMessage = "connection failed"
                //fatalError("Failed to serialize JSON")

             }
            }
            // if connection failed
            else {
                self.state = .error
                self.alertMessage = "connection failed"
            }
        }
        )
        }else{
            isInitRequest = false
            self.state = .error
            self.alertMessage = "connection failed"
        }
}
}

