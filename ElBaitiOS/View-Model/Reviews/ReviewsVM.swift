//
//  ReviewsVM.swift
//  ElBaitiOS
//
//  Created by mac on 9/20/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class ReviewsVM {
    
    var arrReviews: [ReviewsData] = [ReviewsData]()
    var percentage : Percentage?
    var apiService: RestApiManager!
    var currentPage = 1
    var lastPage = 1
    
    // callback for interfaces
    var state: PaginatedState = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    init(){
        
    }

    
    func getReviews(productId: Int, refreshType : RefreshType) {

        state = .loading
          
        apiService = RestApiManager(request: Router.getReviews(parameters: ["product_id":productId,"page": currentPage]))
            
            apiService.TestingEnabled = true
        
           if(refreshType == .refresh)
           {
               self.arrReviews.removeAll()
               self.currentPage = 1
           }
           else
           {
               if(self.currentPage > (self.lastPage))
               {
                   state = .noMore
                   return;
               }
           }
                    
            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let reviewsModel = try JSONDecoder().decode(ReviewsModel.self, from: JSONDATA.rawData())

                    if(reviewsModel.status.code == 200)
                        {
                            if(reviewsModel.data != nil && reviewsModel.data.count > 0)
                          {
                             self.lastPage = (reviewsModel.meta.lastPage)
                              if(refreshType == .refresh)
                              {
                                self.percentage = reviewsModel.percentage.percentage
                                self.arrReviews = reviewsModel.data
                                  self.currentPage += 1
                              }
                              else
                              {
                                 if(self.currentPage <= (reviewsModel.meta.lastPage))
                                  {
                                    self.arrReviews += reviewsModel.data
                                      self.currentPage += 1
                                  }
                                  else
                                  {
                                      self.state = .noMore
                                  }
                              }
                              self.state = .populated
                          }
                             else
                             {
                                 self.state = .empty
                             }
                        }
//                       {
//                           print("Reload")
//                        // logic success here..
//                        print("populated message is  : \(reviewsModel.status.message)")
//                        self.state = .populated
//                        self.arrReviews = reviewsModel.data
//                       }

                        // if codeStatus != 200
                       else{
                        print("error message is  : \(reviewsModel.status.message)")
                        self.alertMessage = "\(reviewsModel.status.message)"
                        self.state = .error
                        
                        return
                       }
                 }catch{
                    print(error)
                    self.alertMessage = "\(error)"
                    self.state = .error
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.state = .error
                }
            }
          )

    //}
    }
    
}

