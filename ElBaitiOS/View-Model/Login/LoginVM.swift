//
//  LoginViewModel.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/23/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

enum CheckLogin{
    case login
    case register
}

struct LoginBrokenRule {
    var propertyName : String
    var message : String
}

class LoginViewModel{
    
// MARK: - Variables
    
var userEmail : String!
var userPassword : String!
var brokenRules: [LoginBrokenRule] = [LoginBrokenRule]()
var param: [String : String]!

var apiService: RestApiManager!


    
init() {
}
    
var isEmailValid: Bool {
    get{
        self.brokenRules.removeAll()
        self.validateEmail()
        return brokenRules.count == 0
    }
}

var isPasswordValid: Bool {
    get{
        self.brokenRules.removeAll()
        self.validatePassword()
        return brokenRules.count == 0
    }
}
 
    func validateEmail(){
        if(!(userEmail.isEmpty)){
            if(!(isValidEmail(email: userEmail))){
                self.brokenRules.append(LoginBrokenRule(propertyName: "User Email", message: "Please enter a valid Email Address"))
            }
        }else{
            self.brokenRules.append(LoginBrokenRule(propertyName: "User Email", message: "An email address must be provided."))
        }
    }
    func validatePassword(){
        if(!(userPassword.isEmpty)){
            if(userPassword.count < 6){
                self.brokenRules.append(LoginBrokenRule(propertyName: "User Password", message: "please enter a valid password"))
            }
        }else{
            self.brokenRules.append(LoginBrokenRule(propertyName: "User password", message: "Please enter your password."))
        }
    }

    private func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func authenticateEmail(completion: @escaping(CheckLogin?, String?)-> Void){
        
        apiService = RestApiManager(request: Router.checkEmail(parameters: param))
        apiService.TestingEnabled = true
        
        apiService.Request(completionHandler:{ (success,JSONDATA,status) -> Void in
            
            if success{
            do{
                 let chekEmailResult = try JSONDecoder().decode(CheckEmail.self, from: JSONDATA.rawData())
               
                let code = Int(chekEmailResult.status.code )
                let msg = chekEmailResult.status.message
                
                if(code >= 200 && code <= 299){
                        if msg == "Login" {
                            completion(.login,nil)
                        }else if msg == "Register"{
                            completion(.register,nil)
                        }
                }else{
                    completion(nil,msg)
                }
            }catch{
                fatalError("Failed to serialize JSON")
                }
            }
        })
    }
    
   
    
    func authenticatePassword(completion: @escaping(LoginModel?, String?)-> Void) {
        
     
        
        apiService = RestApiManager(request: Router.loginPassword(parameters: ["email": userEmail!,
             "password": userPassword!]))
        
        
        
        apiService.TestingEnabled = true
        
        apiService.Request(completionHandler:{ (success,JSONDATA,status) -> Void in
            if success{
                do{
                  let loginPasswordResult = try JSONDecoder().decode(LoginModel.self, from: JSONDATA.rawData())
                       
                    let code = Int(loginPasswordResult.status.code) ?? 0
                    let msg = loginPasswordResult.status.message
                    
                    
                        
                        
                        if(code >= 200 && code <= 299){
                            let token = loginPasswordResult.data!.token
                            print("Token is.. \(token)")
                            saveToken(token: token)
                            saveUserData(userObject: loginPasswordResult.data!.user)
                            
                            // lllllll
                            profileNotificationCtr.post(name: NSNotification.Name(rawValue: profileBroadcast), object: self)
                                                        completion(loginPasswordResult, nil)
                        }else{
                            completion(nil, msg)
                        }
                    }catch{
                        print(error)
                        completion(nil, error.localizedDescription)
                        }
            }
        }
        )

//        dataAccess.excutePasswordRequest(url: urlPassword, dic:
//                ["email": userEmail,
//                 "password": userPassword
//            ]){
//
//            loginModel , error in
//
//            if (loginModel?.code != nil){
//                completion(loginModel,nil)
//            }
//            if(error != nil){
//                 completion(nil,error)
//                }
//        }
    }
    
    deinit {
        profileNotificationCtr.removeObserver(self)
    }
}
