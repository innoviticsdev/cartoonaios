//
//  FiltersVM.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/24/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation
import UIKit

class FiltersVM {
    
    
    var filterParam = [String:String]()
    
    func addRemoveFilter(filterCode: String, filterValue: String){
        
        if filterParam[filterCode] != nil {
            for value in filterParam{
                if(value.key == filterCode){
                    if(value.value.contains(filterValue))
                    {
                        var arr = value.value.components(separatedBy: ",")
                        
                       arr.enumerated().forEach( { (index,item) in
                        if(item == filterValue){
                            arr.remove(at: index)
                        }
                        
                       } )
                        
                     let formattedArray = (arr.map{String($0)}).joined(separator: ",")
                     filterParam[filterCode] = formattedArray
                     print(filterParam)
                        
                    }
                    else{
                            filterParam[filterCode]!.append("," + filterValue)
                    }
                }
            }
        
        } else {
            filterParam[filterCode] = filterValue
        }
         
          print(filterParam.description)
    }
    
    
    
}

