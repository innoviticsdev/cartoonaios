//
//  SubmitOrderVM.swift
//  ElBaitiOS
//
//  Created by mac on 8/31/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class SubmitOrderVM {
    
    
    var apiService: RestApiManager!
   
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    init(){
        
    }

    
    func submitOrder() {

            state = .loading
          
        apiService = RestApiManager(request: Router.submitOrder)
            
            apiService.TestingEnabled = true
           // if(ReachabilityNetwork.getReachabilityInfo()){
             //   isInitRequest = true
                    
            apiService.Request(completionHandler: { [weak self]
                (success,JSONDATA,status) -> Void in

                guard let self = self else { return }

                if success{
                do{
                     let submitOrderModel = try JSONDecoder().decode(SubmitOrderModel.self, from: JSONDATA.rawData())

                    if(submitOrderModel.status.code == 200)
                       {
                           print("Reload")
                        // logic success here..
                        print("populated message is  : \(submitOrderModel.status.message)")
                        self.state = .populated
                        //self.alertMessage = "\(submitOrderModel.status.message)"
                       }

                        // if codeStatus != 200
                       else{
                        print("error message is  : \(submitOrderModel.status.message)")
                        self.alertMessage = "\(submitOrderModel.status.message)"
                        self.state = .error
                        
                        return
                       }
                 }catch{
                    print(error)
                    self.alertMessage = "\(error)"
                    self.state = .error
                    //fatalError("Failed to serialize JSON")
                 }
                }
                // if connection failed
                else {
                    self.state = .error
                }
            }
          )

    //}
    }
    
}
