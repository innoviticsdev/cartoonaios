//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class ProductDetailsVM {
       
    var apiService: RestApiManager!
    
    var ProductDetailsObject: ProductDetailsData!
    var relatedProductsArray: [RelatedItemData] = [RelatedItemData]()
    
    var productCartQty = 1
    
    var productId = ""


    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var addToCartState: State = .empty {
        didSet {
            self.updateAddCartStatus?()
        }
    }
    
    var similarItemsState: State = .empty {
        didSet {
            self.updateSimilarItemsStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateAddCartStatus: (()->())?
    var updateSimilarItemsStatus: (()->())?
    
    func changeProductQty(operators: Operators) ->Int
    {
        switch operators {
            case .increment:
            productCartQty += 1
            case .decrement:
            if(productCartQty > 1)
            {
                productCartQty -= 1
            }
        }
        return productCartQty
    }

    func initFetch() {
        let productDetailsParam = ["product_id":productId]
        apiService = RestApiManager(request: Router.getProductDetails(parameters: productDetailsParam))
            apiService.TestingEnabled = true

        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
            if success{
            do
             {
                 let categoryResult = try JSONDecoder().decode(ProductDetailsModel.self, from: JSONDATA.rawData())
                 if(categoryResult.status.code == 200)
                   {
                    self.ProductDetailsObject = categoryResult.data
                    self.state = .populated
                    self.fetchRelatedProducts()
                   }
                else{
                    //show error msg
                    self.state = .error
                    self.alertMessage = categoryResult.status.message
                    return
                   }
             }
             catch{
                self.state = .error
             }
            }
            // if connection failed
            else {
                self.state = .error
                // show connection failed
            }
        }
        )
    }
    
    func AddToCartAPI() {
        
        let params = ["product_id":self.productId,"quantity": self.productCartQty.description,"is_configurable":"false"]
        
        addToCartState = .loading
        
        apiService = RestApiManager(request: Router.AddToCart(parameters: params))
            apiService.TestingEnabled = true
        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
                 let addToCartResult = try JSONDecoder().decode(AddToCartModel.self, from: JSONDATA.rawData())
                 if(addToCartResult.status.code == 200)
                   {
                       print("Reload")
                   
                    
                      self.addToCartState = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.addToCartState = .error
                    self.alertMessage = addToCartResult.status.message
                    return
                   }
             }
             catch{
                print("Fatal Error")
                 self.addToCartState = .error
             }
            }
            // if connection failed
            else {
                // show connection failed
                 self.addToCartState = .error
            }
        }
        )
    }
    
    func fetchRelatedProducts() {
        similarItemsState = .loading
       
        let productDetailsParam = ["product_id":productId]
        apiService = RestApiManager(request: Router.getRelatedProducts(parameters: productDetailsParam))
        apiService.TestingEnabled = true
        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
            if success{
            do
             {
                 let relatedProductResult = try JSONDecoder().decode(RelatedItemModel.self, from: JSONDATA.rawData())
                 if(relatedProductResult.status.code == 200)
                   {
                       print("Reload")
                    
                    self.relatedProductsArray = relatedProductResult.data
                    self.similarItemsState = (self.relatedProductsArray.count == 0 ? .empty : .populated)
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    self.similarItemsState = .error
                    self.alertMessage = relatedProductResult.status.message
                    return
                   }
             }
             catch{
                self.similarItemsState = .error
             }
            }
            // if connection failed
            else {
                self.similarItemsState = .error
            }
        }
        )
    }
}
