
//
//  HelpVM.swift
//  ElBaitiOS
//
//  Created by Mohamed El Sawi on 8/30/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//


import Foundation

class CancelOrderVM {
    
    
    var apiService: RestApiManager!
    
 /* notificationCtr.post(name: NSNotification.Name(rawValue: broadcast), object: self)
            deinit {
                notificationCtr.removeObserver(self)
            }*/
   
    var cancelParam = [String:String] ()
    

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
 
    
    func initFetch() {

        apiService = RestApiManager(request: Router.cancelOrder(parameters: cancelParam))
        apiService.TestingEnabled = true
        
        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let helpResult = try JSONDecoder().decode(CancelOrderModel.self, from: JSONDATA.rawData())
                 if(helpResult.status.code == 200)
                   {
                    cancelNotificationCtr.post(name: NSNotification.Name(rawValue: cancelBroadcast), object: self)
                    self.state = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = helpResult.status.message
                    return
                   }
             }
             catch {
                self.state = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.state = .error
            }
        }
        )
    }
    
    deinit {
        cancelNotificationCtr.removeObserver(self)
               }
    
}

