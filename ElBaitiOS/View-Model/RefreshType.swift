//
//  RefreshType.swift
//  ElBaitiOS
//
//  Created by Maged Aziz on 9/2/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

public enum RefreshType {
    case refresh
    case loadmore
}
