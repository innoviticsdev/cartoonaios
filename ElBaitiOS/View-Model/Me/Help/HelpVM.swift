//
//  HelpVM.swift
//  ElBaitiOS
//
//  Created by Mohamed El Sawi on 8/30/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//


import Foundation

class HelpVM {
    
    
    var apiService: RestApiManager!
    
 
    var myOrdersData:[MyOrdersData] = [MyOrdersData]()
    var helpParam = [String:String] ()
    

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    func validateTextFields() {
        if((helpParam["subject"]) == ""){
            self.state = .error
            self.alertMessage = "Please enter subject!"
        }else if((helpParam["details"]) == ""){
            self.state = .error
            self.alertMessage = "Please enter details!"
        }else{
            initFetch()
        }
    }
 
    
    func initFetch() {

        apiService = RestApiManager(request: Router.submitHelp(parameters: helpParam))
        apiService.TestingEnabled = true
        
        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let helpResult = try JSONDecoder().decode(HelpModel.self, from: JSONDATA.rawData())
                 if(helpResult.status.code == 200)
                   {
                    self.state = .populated
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = helpResult.status.message
                    return
                   }
             }
             catch {
                self.state = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.state = .error
            }
        }
        )
    }
}

