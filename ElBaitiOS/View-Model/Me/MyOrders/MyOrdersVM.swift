//
//  CategoriesVM.swift
//  ElBaitiOS
//
//  Created by Ahmed Ramzy on 8/17/20.
//  Copyright © 2020 Ahmed Ramzy. All rights reserved.
//

import Foundation

class MyOrdersVM {
    
    
    var apiService: RestApiManager!
    
 
    var myOrdersData:[MyOrdersData] = [MyOrdersData]()
    

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
 
    
    func initFetch() {

        apiService = RestApiManager(request: Router.MyOrders)
        apiService.TestingEnabled = true
        
        state = .loading

        apiService.Request(completionHandler: { [weak self]
            (success,JSONDATA,status) -> Void in
            
            guard let self = self else {
                            return
                        }
                
            if success{
            do
             {
              
                 let myOrdersResult = try JSONDecoder().decode(MyOrdersModel.self, from: JSONDATA.rawData())
                 if(myOrdersResult.status.code == 200)
                   {

                    if(myOrdersResult.data != nil && myOrdersResult.data!.count > 0)
                    {
                        self.myOrdersData = myOrdersResult.data!
                        self.state = .populated
                    }
                    else
                    {
                        self.state = .empty
                    }
                   }
            
                    // if codeStatus != 200
                   else{
                    //show error msg
                    
                    self.state = .error
                    self.alertMessage = myOrdersResult.status.message
                    return
                   }
             }
             catch {
                self.state = .error
                print(error)
             }
            }
            // if connection failed
            else {
                // show connection failed
                self.state = .error
            }
        }
        )
    }
}

