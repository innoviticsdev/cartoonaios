// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let checkEmail = try? newJSONDecoder().decode(CheckEmail.self, from: jsonData)

import Foundation

// MARK: - CheckEmail
struct CheckEmail: Codable {
    let status: StatusCheckEmail
}

// MARK: - Status
struct StatusCheckEmail: Codable {
    let code: Int
    let message: String
}
