
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let addressDetailsModel = try? newJSONDecoder().decode(AddressDetailsModel.self, from: jsonData)

import Foundation

// MARK: - AddressDetailsModel
struct AddressDetailsModel: Codable {
    let status: Status

}

