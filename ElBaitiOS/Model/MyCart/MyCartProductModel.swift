// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myCartProductModel = try? newJSONDecoder().decode(MyCartProductModel.self, from: jsonData)

import Foundation

// MARK: - MyCartProductModel
struct MyCartProductModel: Codable {
    let data: MyCartData?
    let status: Status
}

// MARK: - DataClass
struct MyCartData: Codable {
    let id: Int
    let couponCode, grandTotal, formatedGrandTotal, subTotal: String
    let formatedSubTotal, formatedDiscount: String
    let items: [Item]
    let selectedShippingRate: SelectedShippingRate?
    let shippingAddress: ShippingAddress?
    let freeShipping: FreeShipping

    enum CodingKeys: String, CodingKey {
        case id
        case couponCode = "coupon_code"
        case grandTotal = "grand_total"
        case formatedGrandTotal = "formated_grand_total"
        case subTotal = "sub_total"
        case formatedSubTotal = "formated_sub_total"
        case formatedDiscount = "formated_discount"
        case items
        case selectedShippingRate = "selected_shipping_rate"
        case shippingAddress = "shipping_address"
        case freeShipping
    }
}

// MARK: - FreeShipping
struct FreeShipping: Codable {
    let status: Int
    let minimumAmount, formatedMinimumAmount, remainingAmount, formatedRemainingAmount: String
    let message: String

    enum CodingKeys: String, CodingKey {
        case status
        case minimumAmount = "minimum_amount"
        case formatedMinimumAmount = "formated_minimum_amount"
        case remainingAmount = "remaining_amount"
        case formatedRemainingAmount = "formated_remaining_amount"
        case message
    }
}

// MARK: - Item
struct Item: Codable {
    let id: Int
    let quantity, name, currency, price: String
    let formatedPrice, formatedTotal: String
    let additional: Additional
    let product: Product

    enum CodingKeys: String, CodingKey {
        case id, quantity, name, currency, price
        case formatedPrice = "formated_price"
        case formatedTotal = "formated_total"
        case additional, product
    }
}

// MARK: - SelectedShippingRate
struct SelectedShippingRate: Codable {
    let id: Int
    let carrier, carrierTitle, method, methodTitle: String
    let methodDescription, price, formatedPrice, basePrice: String
    let formatedBasePrice, createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, carrier
        case carrierTitle = "carrier_title"
        case method
        case methodTitle = "method_title"
        case methodDescription = "method_description"
        case price
        case formatedPrice = "formated_price"
        case basePrice = "base_price"
        case formatedBasePrice = "formated_base_price"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - ShippingAddress
struct ShippingAddress: Codable {
    let id: Int?
    let firstName, lastName, locationName, email: String?
    let address1: [String]
    let country, countryName, state, city: String?
    let postcode: String?
    let phone: String?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case locationName = "location_name"
        case email, address1, country
        case countryName = "country_name"
        case state, city, postcode, phone
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Additional
struct Additional: Codable {
    let productID: String
    let quantity: Quantity
    let isConfigurable: String
    let selectedConfigurableOption, superAttribute: String?


    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case quantity
        case isConfigurable = "is_configurable"
        case selectedConfigurableOption = "selected_configurable_option"
        case superAttribute = "super_attribute"
 
    }
}




enum Quantity: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Quantity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

// MARK: - Product
struct Product: Codable {
    let id: Int
    let name, finalPrice: String
    let baseImage: BaseImage
    let reviews: ProductReviews?

    enum CodingKeys: String, CodingKey {
        case id, name
        case finalPrice = "final_price"
        case baseImage = "base_image"
        case reviews
    }
}

// MARK: - ProductReviews
struct ProductReviews: Codable {
    let total, totalRating, averageRating: Int
    let percentage: Percentage

    enum CodingKeys: String, CodingKey {
        case total
        case totalRating = "total_rating"
        case averageRating = "average_rating"
        case percentage
    }
}




