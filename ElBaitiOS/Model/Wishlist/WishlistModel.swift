// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let wishlistModel = try? newJSONDecoder().decode(WishlistModel.self, from: jsonData)

import Foundation

// MARK: - WishlistModel
struct WishlistModel: Codable {
    let data: [WishlistData]
    let links: Links
    let meta: Meta
    let status: Status
}

// MARK: - Datum
struct WishlistData: Codable {
    let id: Int
    let product: WishlistProduct
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, product
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Product
struct WishlistProduct: Codable {
    let id: Int
    let name, finalPrice, originalPrice: String
    let baseImage: BaseImage
    let inStock: Bool
    let reviews: Reviews
    let tags: [Tag]
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case finalPrice = "final_price"
        case originalPrice = "original_price"
        case baseImage = "base_image"
        case inStock = "in_stock"
        case reviews, tags
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}




// MARK: - Reviews








