// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myOrdersModel = try? newJSONDecoder().decode(MyOrdersModel.self, from: jsonData)

import Foundation

// MARK: - MyOrdersModel
struct MyOrdersModel: Codable {
    let data: [MyOrdersData]?
    let links: Links?
    let meta: Meta?
    let status: Status
}

// MARK: - Datum
struct MyOrdersData: Codable {
    let id: Int
    let statusLocation, statusLabel, grandTotal, formatedGrandTotal,subTotal, formatedSubTotal: String
    let canCancel: Bool
    let items: [MyOrderItem]
    let shipments: [JSONAny]
    let createdAt: String
    let discountAmount, formatedDiscountAmount: String
    let shippingAddress: ShippingAddress
    let isReviewed, canReview: Bool

    enum CodingKeys: String, CodingKey {
        case id
        case statusLocation = "status_location"
        case statusLabel = "status_label"
        case grandTotal = "grand_total"
        case formatedGrandTotal = "formated_grand_total"
        case items, shipments
        case createdAt = "created_at"
         case subTotal = "sub_total"
        case formatedSubTotal = "formated_sub_total"
        case discountAmount = "discount_amount"
        case formatedDiscountAmount = "formated_discount_amount"
        case shippingAddress = "shipping_address"
        case canCancel
        case isReviewed = "is_reviewed"
        case canReview
    }
}

// MARK: - Item
struct MyOrderItem: Codable {
    let id: Int
    let name: String
    let product: Product?
    let qtyOrdered: String
    let isReviewed: Bool
    let reviews: ItemReviews?

    enum CodingKeys: String, CodingKey {
        case isReviewed = "is_reviewed"
        case reviews
        case id, name, product
        case qtyOrdered = "qty_ordered"
    }
}

// MARK: - ItemReviews
struct ItemReviews: Codable {
    let rating, comment: String
}



