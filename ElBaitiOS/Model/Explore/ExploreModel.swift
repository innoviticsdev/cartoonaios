// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let exploreModel = try? newJSONDecoder().decode(ExploreModel.self, from: jsonData)

import Foundation

// MARK: - ExploreModel
struct ExploreModel: Codable {
    let status: Status
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let categories: [Category]
    let deals: [Deal]
    let bestSellers, electronics, fashion: [BestSeller]

    enum CodingKeys: String, CodingKey {
        case categories, deals
        case bestSellers = "best_sellers"
        case electronics, fashion
    }
}

// MARK: - BestSeller
struct BestSeller: Codable {
    let id: Int
    let name, finalPrice, originalPrice: String?
    let baseImage: BaseImage
    let inStock: Bool?
    let reviews: Reviews
    let tags: [Tag]
    let createdAt, updatedAt: String
    let isSaved: Bool

    enum CodingKeys: String, CodingKey {
        case id, name
        case finalPrice = "final_price"
        case originalPrice = "original_price"
        case baseImage = "base_image"
        case inStock = "in_stock"
        case reviews, tags
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isSaved = "is_saved"
    }
}

enum Color: String, Codable {
    case add8E6 = "#ADD8E6"
    case ffff66 = "#FFFF66"
}

enum Text: String, Codable {
    case freeShipping = "Free Shipping"
    case the18Off = "18% Off"
}

// MARK: - Category
struct Category: Codable {
    let id: Int
    let name: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case imageURL = "image_url"
    }
}

// MARK: - Deal
struct Deal: Codable {
    let percentage: String
    let image: String
    let name: String
}
