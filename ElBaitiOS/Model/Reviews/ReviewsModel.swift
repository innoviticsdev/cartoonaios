// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let reviewsModel = try? newJSONDecoder().decode(ReviewsModel.self, from: jsonData)

import Foundation

// MARK: - ReviewsModel
struct ReviewsModel: Codable {
    let data: [ReviewsData]
    let links: Links
    let meta: Meta
    let status: Status
    let percentage: SubmitReviewModelPercentage
}

// MARK: - SubmitReviewModelPercentage
struct SubmitReviewModelPercentage: Codable {
    let total, totalRating, averageRating: Int
    let percentage: Percentage

    enum CodingKeys: String, CodingKey {
        case total
        case totalRating = "total_rating"
        case averageRating = "average_rating"
        case percentage
    }
}

//// MARK: - PercentagePercentage
//struct PercentagePercentage: Codable {
//    let fiveStar, fourStar, threeStar, twoStar: Star
//    let oneStar: Star
//
//    enum CodingKeys: String, CodingKey {
//        case fiveStar = "five_star"
//        case fourStar = "four_star"
//        case threeStar = "three_star"
//        case twoStar = "two_star"
//        case oneStar = "one_star"
//    }
//}

// MARK: - Datum
struct ReviewsData: Codable {
    let id: Int
    let rating, comment, name: String
    let image: String
    let createdAt: String

    enum CodingKeys: String, CodingKey {
        case id, rating, comment, name, image
        case createdAt = "created_at"
    }
}

// MARK: - Links
//struct Links: Codable {
//    let first, last: String
//    let prev, next: JSONNull?
//}

// MARK: - Meta
//struct Meta: Codable {
//    let currentPage, from, lastPage: Int
//    let path: String
//    let perPage, to, total: Int
//
//    enum CodingKeys: String, CodingKey {
//        case currentPage = "current_page"
//        case from
//        case lastPage = "last_page"
//        case path
//        case perPage = "per_page"
//        case to, total
//    }
//}

// MARK: - Status
//struct Status: Codable {
//    let code: Int
//    let message: String
//}

// MARK: - Encode/decode helpers

//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
