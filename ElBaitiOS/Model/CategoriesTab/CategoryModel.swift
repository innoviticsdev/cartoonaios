// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let categoryModel = try? newJSONDecoder().decode(CategoryModel.self, from: jsonData)

import Foundation

// MARK: - CategoryModel
struct CategoryModel: Codable {
    let data: [CategoryData]?
    let links: Links
    let meta: Meta
    let status: Status
}

// MARK: - Datum
struct CategoryData: Codable {
    let id: Int
    let code: String?
    let name, slug, displayMode, datumDescription: String
    let metaTitle, metaDescription, metaKeywords, status: String
    let imageURL: String
    let createdAt, updatedAt: String
    let attributes: [CategoryAttribute]

    enum CodingKeys: String, CodingKey {
        case id, code, name, slug
        case displayMode = "display_mode"
        case datumDescription = "description"
        case metaTitle = "meta_title"
        case metaDescription = "meta_description"
        case metaKeywords = "meta_keywords"
        case status
        case imageURL = "image_url"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case attributes
    }
}

struct CategoryAttribute: Codable {
    let id: Int
    let code: String
    let type: String
    let name: String
    let swatchType: String?
    let options: [CategoryOption]
    let createdAt: String
    let updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, code, type, name
        case swatchType = "swatch_type"
        case options
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Option
struct CategoryOption: Codable {
    let id: Int
    let adminName, label: String
    let swatchValue: String?

    enum CodingKeys: String, CodingKey {
        case id
        case adminName = "admin_name"
        case label
        case swatchValue = "swatch_value"
    }
}

// MARK: - Links
struct Links: Codable {
    let first, last: String?
    let prev: String?
    let next: String?
}

// MARK: - Meta
struct Meta: Codable {
    let currentPage, lastPage: Int
    let from : Int?
    let to : Int?
    let path: String
    let perPage, total: Int

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case from
        case lastPage = "last_page"
        case path
        case perPage = "per_page"
        case to, total
    }
}

// MARK: - Status
struct Status: Codable {
    let code: Int
    let message: String
}

// MARK: - Encode/decode helpers

@objcMembers class JSONNull: NSObject, Codable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    override public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
