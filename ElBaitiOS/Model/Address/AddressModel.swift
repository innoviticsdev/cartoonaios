// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let addressModel = try? newJSONDecoder().decode(AddressModel.self, from: jsonData)

import Foundation

// MARK: - AddressModel
struct AddressModel: Codable {
    let status: StatusAddressModel
    let data: [AddressDetails]
}

// MARK: - Datum
struct AddressDetails: Codable {
    let id: Int
    let address1: [String]?
    let country, countryName, city, locationName: String
    let buildingNo, floorNo, flatNo, details: String
    var isChecked: Bool?

    enum CodingKeys: String, CodingKey {
        case id, address1, country
        case countryName = "country_name"
        case city
        case locationName = "location_name"
        case buildingNo = "building_no"
        case floorNo = "floor_no"
        case flatNo = "flat_no"
        case details
    }
}

// MARK: - Status
struct StatusAddressModel: Codable {
    let code: Int
    let message: String
}
