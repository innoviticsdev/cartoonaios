// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let registerModel = try? newJSONDecoder().decode(RegisterModel.self, from: jsonData)

import Foundation

// MARK: - RegisterModel
struct RegisterModel: Codable {
    let status: StatusRegisterModel
    let data: DataClassRegisterModel?
}

// MARK: - DataClass
struct DataClassRegisterModel: Codable {
    let user: UserRegisterModel
    let token: String
}

// MARK: - User
struct UserRegisterModel: Codable {
    let id: Int
    let email, firstName, lastName, name: String
    let gender, dateOfBirth: JSONNull?
    let phone, image: String
    let status: JSONNullRegisterModel?
    let group: GroupRegisterModel
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case name, gender
        case dateOfBirth = "date_of_birth"
        case phone, image, status, group
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Group
struct GroupRegisterModel: Codable {
    let id: Int
    let name: String
    let createdAt, updatedAt: JSONNullRegisterModel?

    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Status
struct StatusRegisterModel: Codable {
    let code: Int
    let message: String
}

// MARK: - Encode/decode helpers

class JSONNullRegisterModel: Codable, Hashable {

    public static func == (lhs: JSONNullRegisterModel, rhs: JSONNullRegisterModel) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
