// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let helpModel = try? newJSONDecoder().decode(HelpModel.self, from: jsonData)

import Foundation

// MARK: - HelpModel
struct HelpModel: Codable {
    let status: Status
}
 
