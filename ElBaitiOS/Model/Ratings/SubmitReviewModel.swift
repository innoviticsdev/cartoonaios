// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let submitReviewModel = try? newJSONDecoder().decode(SubmitReviewModel.self, from: jsonData)

import Foundation

// MARK: - SubmitReviewModel
struct SubmitReviewModel: Codable {
    let status: SubmitReviewStatus
}

// MARK: - Status
struct SubmitReviewStatus: Codable {
    let code: Int
    let message: String
}
