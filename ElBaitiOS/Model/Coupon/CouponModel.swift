// To parse the JSON, add this file to your project and do:
//
//   let couponModel = try? newJSONDecoder().decode(CouponModel.self, from: jsonData)

import Foundation

// MARK: - CouponModel
struct CouponModel: Codable {
    let status: StatusCouponModel
    let data: MyCartData?
}

//// MARK: - DataClass
//struct DataClassCouponModel: Codable {
//    let id: Int
//    let couponCode, grandTotal, formatedGrandTotal, subTotal: String
//    let formatedSubTotal, formatedDiscount: String
//    let items: [ItemCouponModel]
//    let selectedShippingRate: SelectedShippingRateCouponModel
//    let shippingAddress: ShippingAddressCouponModel
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case couponCode = "coupon_code"
//        case grandTotal = "grand_total"
//        case formatedGrandTotal = "formated_grand_total"
//        case subTotal = "sub_total"
//        case formatedSubTotal = "formated_sub_total"
//        case formatedDiscount = "formated_discount"
//        case items
//        case selectedShippingRate = "selected_shipping_rate"
//        case shippingAddress = "shipping_address"
//    }
//}
//
//// MARK: - Item
//struct ItemCouponModel: Codable {
//    let id: Int
//    let quantity, name, currency, price: String
//    let formatedPrice, formatedTotal: String
//    let additional: AdditionalCouponModel
//    let product: ProductCouponModel
//
//    enum CodingKeys: String, CodingKey {
//        case id, quantity, name, currency, price
//        case formatedPrice = "formated_price"
//        case formatedTotal = "formated_total"
//        case additional, product
//    }
//}
//
//// MARK: - Additional
//struct AdditionalCouponModel: Codable {
//    let productID: String
//    let quantity: QuantityCouponModel
//    let isConfigurable: String
//    let selectedConfigurableOption, superAttribute: String?
//    let attributes: AttributesCouponModel?
//
//    enum CodingKeys: String, CodingKey {
//        case productID = "product_id"
//        case quantity
//        case isConfigurable = "is_configurable"
//        case selectedConfigurableOption = "selected_configurable_option"
//        case superAttribute = "super_attribute"
//        case attributes
//    }
//}
//
//// MARK: - Attributes
//struct AttributesCouponModel: Codable {
//    let color, storageSize: ColorCouponModel
//
//    enum CodingKeys: String, CodingKey {
//        case color
//        case storageSize = "storage_size"
//    }
//}
//
//// MARK: - Color
//struct ColorCouponModel: Codable {
//    let attributeName: String
//    let optionID: Int
//    let optionLabel: String
//
//    enum CodingKeys: String, CodingKey {
//        case attributeName = "attribute_name"
//        case optionID = "option_id"
//        case optionLabel = "option_label"
//    }
//}
//
//enum QuantityCouponModel: Codable {
//    case integer(Int)
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Int.self) {
//            self = .integer(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        throw DecodingError.typeMismatch(Quantity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Quantity"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .integer(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}
//
//// MARK: - Product
//struct ProductCouponModel: Codable {
//    let id: Int
//    let name, finalPrice: String
//    let baseImage: BaseImageCouponModel
//
//    enum CodingKeys: String, CodingKey {
//        case id, name
//        case finalPrice = "final_price"
//        case baseImage = "base_image"
//    }
//}
//
//// MARK: - BaseImage
//struct BaseImageCouponModel: Codable {
//    let smallImageURL, mediumImageURL, largeImageURL, originalImageURL: String
//
//    enum CodingKeys: String, CodingKey {
//        case smallImageURL = "small_image_url"
//        case mediumImageURL = "medium_image_url"
//        case largeImageURL = "large_image_url"
//        case originalImageURL = "original_image_url"
//    }
//}
//
//// MARK: - SelectedShippingRate
//struct SelectedShippingRateCouponModel: Codable {
//    let id: Int
//    let carrier, carrierTitle, method, methodTitle: String
//    let methodDescription, price, formatedPrice, basePrice: String
//    let formatedBasePrice, createdAt, updatedAt: String
//
//    enum CodingKeys: String, CodingKey {
//        case id, carrier
//        case carrierTitle = "carrier_title"
//        case method
//        case methodTitle = "method_title"
//        case methodDescription = "method_description"
//        case price
//        case formatedPrice = "formated_price"
//        case basePrice = "base_price"
//        case formatedBasePrice = "formated_base_price"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//    }
//}
//
//// MARK: - ShippingAddress
//struct ShippingAddressCouponModel: Codable {
//    let id: Int
//    let firstName, lastName, locationName, email: String
//    let address1: [String]
//    let country, countryName, state, city: String
//    let postcode: String
//    let phone: String?
//    let createdAt, updatedAt: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case firstName = "first_name"
//        case lastName = "last_name"
//        case locationName = "location_name"
//        case email, address1, country
//        case countryName = "country_name"
//        case state, city, postcode, phone
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//    }
//}

// MARK: - Status
struct StatusCouponModel: Codable {
    let code: Int
    let message: String
}

//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
