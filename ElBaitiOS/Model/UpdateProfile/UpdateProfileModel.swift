// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let updateProfileModel = try? newJSONDecoder().decode(UpdateProfileModel.self, from: jsonData)

import Foundation

// MARK: - UpdateProfileModel
struct UpdateProfileModel: Codable {
    let status: StatusUpdateProfile
    let data: DataClassUpdateProfile?
}

 //MARK: - DataClass
struct DataClassUpdateProfile: Codable {
    let user: User
    let token: String
}

 //MARK: - User
struct UserUpdateProfile: Codable {
    let id: Int
    let email, firstName, lastName, name: String
    let gender, dateOfBirth: JSONNullUpdateProfile?
    let phone, image, status: String
    let group: GroupUpdateProfile
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case name, gender
        case dateOfBirth = "date_of_birth"
        case phone, image, status, group
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

 //MARK: - Group
struct GroupUpdateProfile: Codable {
    let id: Int
    let name: String
    let createdAt, updatedAt: JSONNullUpdateProfile?

    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

 //MARK: - Status
struct StatusUpdateProfile: Codable {
    let code: Int
    let message: String
}

 //MARK: - Encode/decode helpers

class JSONNullUpdateProfile: Codable, Hashable {

    public static func == (lhs: JSONNullUpdateProfile, rhs: JSONNullUpdateProfile) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNullUpdateProfile.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
