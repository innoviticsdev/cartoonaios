
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let relatedProductsModel = try? newJSONDecoder().decode(RelatedProductsModel.self, from: jsonData)

import Foundation

// MARK: - RelatedProductsModel
struct RelatedItemModel: Codable {
    let data: [RelatedItemData]
    let status: Status
}

// MARK: - Datum
struct RelatedItemData: Codable {
    let id: Int
    let name, finalPrice, originalPrice: String
    let baseImage: BaseImage
    let inStock: Bool
    let reviews: Reviews
    let tags: [Tag]
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case finalPrice = "final_price"
        case originalPrice = "original_price"
        case baseImage = "base_image"
        case inStock = "in_stock"
        case reviews, tags
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}










