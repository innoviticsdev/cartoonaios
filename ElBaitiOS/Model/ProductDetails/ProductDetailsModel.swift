
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let productDetailsModel = try? newJSONDecoder().decode(ProductDetailsModel.self, from: jsonData)

import Foundation

// MARK: - ProductDetailsModel
struct ProductDetailsModel: Codable {
    let data: ProductDetailsData?
    let status: Status
}

// MARK: - DataElement
struct ProductDetailsData: Codable {
    let id: Int
    let type, name, finalPrice, originalPrice: String
    let shortDescription, dataDescription: String?
    let images: [ImageElement]
    let baseImage: Image
    let variants: [ProductDetailsData]
    let inStock: Bool?
    let filters: Filters?
    let reviews: Reviews
    let createdAt, updatedAt: String
    let cartCount: Int
    let isSaved: Bool

    enum CodingKeys: String, CodingKey {
        case id, type, name
        case finalPrice = "final_price"
        case originalPrice = "original_price"
        case shortDescription = "short_description"
        case dataDescription = "description"
        case images
        case baseImage = "base_image"
        case variants
        case inStock = "in_stock"
        case filters, reviews
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case cartCount = "cart_count"
        case isSaved = "is_saved"
    }
}

// MARK: - Image
struct Image: Codable {
    let smallImageURL, mediumImageURL, largeImageURL, originalImageURL: String

    enum CodingKeys: String, CodingKey {
        case smallImageURL = "small_image_url"
        case mediumImageURL = "medium_image_url"
        case largeImageURL = "large_image_url"
        case originalImageURL = "original_image_url"
    }
}

// MARK: - Filters
struct Filters: Codable {
    let attributes: [Attribute]
    let index: [[String]]
    let regularPrice: Price
    let variantPrices: [VariantPrice]
    let variantImages: [[Image]]
    let chooseText: String

    enum CodingKeys: String, CodingKey {
        case attributes, index
        case regularPrice = "regular_price"
        case variantPrices = "variant_prices"
        case variantImages = "variant_images"
        case chooseText
    }
}

// MARK: - Attribute
struct Attribute: Codable {
    let id: Int
    let code, label: String
    let swatchType: String?
    let options: [Option]

    enum CodingKeys: String, CodingKey {
        case id, code, label
        case swatchType = "swatch_type"
        case options
    }
}

// MARK: - Option
struct Option: Codable {
    let id: Int
    let label: String
    let swatchValue: String?
    let products: [Int]

    enum CodingKeys: String, CodingKey {
        case id, label
        case swatchValue = "swatch_value"
        case products
    }
}

// MARK: - Price
struct Price: Codable {
    let formatedPrice, price: String

    enum CodingKeys: String, CodingKey {
        case formatedPrice = "formated_price"
        case price
    }
}

// MARK: - VariantPrice
struct VariantPrice: Codable {
    let regularPrice, finalPrice: Price

    enum CodingKeys: String, CodingKey {
        case regularPrice = "regular_price"
        case finalPrice = "final_price"
    }
}

// MARK: - ImageElement
struct ImageElement: Codable {
    let id: Int
    let path: String
    let url, originalImageURL, smallImageURL, mediumImageURL: String
    let largeImageURL: String

    enum CodingKeys: String, CodingKey {
        case id, path, url
        case originalImageURL = "original_image_url"
        case smallImageURL = "small_image_url"
        case mediumImageURL = "medium_image_url"
        case largeImageURL = "large_image_url"
    }
}







