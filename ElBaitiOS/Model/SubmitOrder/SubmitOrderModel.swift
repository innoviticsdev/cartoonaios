// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let submitOrderModel = try? newJSONDecoder().decode(SubmitOrderModel.self, from: jsonData)

import Foundation

// MARK: - SubmitOrderModel
struct SubmitOrderModel: Codable {
    let status: StatusSubmitOrderModel
    let data: DataClassSubmitOrderModel
}

// MARK: - DataClass
struct DataClassSubmitOrderModel: Codable {
    let id: Int
    let statusLocation, statusLabel: String
    let canCancel: Bool
    let grandTotal, formatedGrandTotal, subTotal, formatedSubTotal: String
    let discountAmount, formatedDiscountAmount: String
    let shippingAddress: ShippingAddressSubmitOrderModel
    let items: [ItemSubmitOrderModel]
    let shipments: [JSONAnySubmitOrderModel]
    let createdAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case statusLocation = "status_location"
        case statusLabel = "status_label"
        case canCancel
        case grandTotal = "grand_total"
        case formatedGrandTotal = "formated_grand_total"
        case subTotal = "sub_total"
        case formatedSubTotal = "formated_sub_total"
        case discountAmount = "discount_amount"
        case formatedDiscountAmount = "formated_discount_amount"
        case shippingAddress = "shipping_address"
        case items, shipments
        case createdAt = "created_at"
    }
}

// MARK: - Item
struct ItemSubmitOrderModel: Codable {
    let id: Int
    let name: String
    let product: ProductSubmitOrderModel
    let qtyOrdered: String

    enum CodingKeys: String, CodingKey {
        case id, name, product
        case qtyOrdered = "qty_ordered"
    }
}

// MARK: - Product
struct ProductSubmitOrderModel: Codable {
    let id: Int
    let name, finalPrice, originalPrice: String
    let baseImage: BaseImageSubmitOrderModel
    let inStock: Bool
    let reviews: ReviewsSubmitOrderModel
    let tags: [TagSubmitOrderModel]
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case finalPrice = "final_price"
        case originalPrice = "original_price"
        case baseImage = "base_image"
        case inStock = "in_stock"
        case reviews, tags
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - BaseImage
struct BaseImageSubmitOrderModel: Codable {
    let smallImageURL, mediumImageURL, largeImageURL, originalImageURL: String

    enum CodingKeys: String, CodingKey {
        case smallImageURL = "small_image_url"
        case mediumImageURL = "medium_image_url"
        case largeImageURL = "large_image_url"
        case originalImageURL = "original_image_url"
    }
}

// MARK: - Reviews
struct ReviewsSubmitOrderModel: Codable {
    let total, totalRating, averageRating: Int
    let percentage: Percentage

    enum CodingKeys: String, CodingKey {
        case total
        case totalRating = "total_rating"
        case averageRating = "average_rating"
        case percentage
    }
}

// MARK: - Tag
struct TagSubmitOrderModel: Codable {
    let color, text: String
}

// MARK: - ShippingAddress
struct ShippingAddressSubmitOrderModel: Codable {
    let id: Int
    let email, firstName, lastName, locationName: String
    let address1: [String]
    let country, countryName, state, city: String
    let postcode: String
    let phone: JSONNullSubmitOrderModel?
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case locationName = "location_name"
        case address1, country
        case countryName = "country_name"
        case state, city, postcode, phone
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Status
struct StatusSubmitOrderModel: Codable {
    let code: Int
    let message: String
}

// MARK: - Encode/decode helpers

class JSONNullSubmitOrderModel: Codable, Hashable {

    public static func == (lhs: JSONNullSubmitOrderModel, rhs: JSONNullSubmitOrderModel) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKeySubmitOrderModel: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAnySubmitOrderModel: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
