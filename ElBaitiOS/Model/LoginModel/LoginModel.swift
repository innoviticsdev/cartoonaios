// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginModel = try? newJSONDecoder().decode(LoginModel.self, from: jsonData)

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let status: StatusLoginModel
    let data: DataClassLoginModel?
}

// MARK: - DataClass
struct DataClassLoginModel: Codable {
    let user: User
    let token: String
}

// MARK: - User
struct User: Codable {
    let id: Int
    let email, firstName, lastName, name: String
    let gender, dateOfBirth: String?
    let phone, status, image: String
    let group: Group
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case name, gender , image
        case dateOfBirth = "date_of_birth"
        case phone, status, group
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    // MARK: NSCoding Protocol
    
}


// MARK: - Group
struct Group: Codable {
    let id: Int
    let name: String
    let createdAt, updatedAt: JSONNull?

    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Status
struct StatusLoginModel: Codable {
    let code : Int
    let message: String
}

// MARK: - Encode/decode helpers

//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
