// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let perorderModel = try? newJSONDecoder().decode(PerorderModel.self, from: jsonData)

import Foundation

// MARK: - PerorderModel
struct PerorderModel: Codable {
    let status: StatusPerorderModel
    let data: DataClassPerorderModel?
}

// MARK: - DataClass
struct DataClassPerorderModel: Codable {
    let cart: CartPerorderModel
}

// MARK: - Cart
struct CartPerorderModel: Codable {
    let id: Int
    let couponCode, grandTotal, formatedGrandTotal, subTotal: String
    let formatedSubTotal, formatedDiscount: String
    let items: [ItemPerorderModel]
    let selectedShippingRate: SelectedShippingRatePerorderModel
    let shippingAddress: ShippingAddressPerorderModel
    let freeShipping: FreeShipping?

    enum CodingKeys: String, CodingKey {
        case id
        case couponCode = "coupon_code"
        case grandTotal = "grand_total"
        case formatedGrandTotal = "formated_grand_total"
        case subTotal = "sub_total"
        case formatedSubTotal = "formated_sub_total"
        case formatedDiscount = "formated_discount"
        case items
        case selectedShippingRate = "selected_shipping_rate"
        case shippingAddress = "shipping_address"
        case freeShipping

    }
}

// MARK: - Item
struct ItemPerorderModel: Codable {
    let id: Int
    let quantity, name, currency, price: String
    let formatedPrice, formatedTotal: String
    let additional: AdditionalPerorderModel
    let product: ProductPerorderModel

    enum CodingKeys: String, CodingKey {
        case id, quantity, name, currency, price
        case formatedPrice = "formated_price"
        case formatedTotal = "formated_total"
        case additional, product
    }
}

// MARK: - Additional
struct AdditionalPerorderModel: Codable {
    let productID, isConfigurable: String
    let quantity: String
    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case quantity
        case isConfigurable = "is_configurable"
    }
}

// MARK: - Product
struct ProductPerorderModel: Codable {
    let id: Int
    let name, finalPrice: String
    let baseImage: BaseImagePerorderModel

    enum CodingKeys: String, CodingKey {
        case id, name
        case finalPrice = "final_price"
        case baseImage = "base_image"
    }
}

// MARK: - BaseImage
struct BaseImagePerorderModel: Codable {
    let smallImageURL, mediumImageURL, largeImageURL, originalImageURL: String

    enum CodingKeys: String, CodingKey {
        case smallImageURL = "small_image_url"
        case mediumImageURL = "medium_image_url"
        case largeImageURL = "large_image_url"
        case originalImageURL = "original_image_url"
    }
}

// MARK: - SelectedShippingRate
struct SelectedShippingRatePerorderModel: Codable {
    let id: Int
    let carrier, carrierTitle, method, methodTitle: String
    let methodDescription, price, formatedPrice, basePrice: String
    let formatedBasePrice, createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id, carrier
        case carrierTitle = "carrier_title"
        case method
        case methodTitle = "method_title"
        case methodDescription = "method_description"
        case price
        case formatedPrice = "formated_price"
        case basePrice = "base_price"
        case formatedBasePrice = "formated_base_price"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - ShippingAddress
struct ShippingAddressPerorderModel: Codable {
    let id: Int
    let firstName, lastName, locationName, email: String
    let address1: [String]
    let country, countryName, state, city: String
    let postcode, phone, createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case locationName = "location_name"
        case email, address1, country
        case countryName = "country_name"
        case state, city, postcode, phone
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

// MARK: - Status
struct StatusPerorderModel: Codable {
    let code: Int
    let message: String
}
